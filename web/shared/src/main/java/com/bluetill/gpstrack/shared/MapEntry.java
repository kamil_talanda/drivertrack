package com.bluetill.gpstrack.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 04.07.13
 * Time: 16:01

 */
public class MapEntry implements IsSerializable {
    private int entryId;
    private String entryName;
    private EntryStyle entryStyle;
    private ArrayList<HashMap<String,LineArray>>  pointList;


    public MapEntry(int entryId, String entryName, EntryStyle entryStyle, ArrayList<HashMap<String,LineArray>> pointList) {
        this.entryId = entryId;
        this.entryName = entryName;
        this.entryStyle = entryStyle;
        this.pointList = pointList;
    }

    public MapEntry() {
        this.entryId = 0;
        this.entryName = null;
        this.entryStyle = new EntryStyle();
        this.pointList = new ArrayList<HashMap<String,LineArray>>();
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public EntryStyle getEntryStyle() {
        return entryStyle;
    }

    public void setEntryStyle(EntryStyle entryStyle) {
        this.entryStyle = entryStyle;
    }

    public ArrayList<HashMap<String,LineArray>> getPointList() {
        return pointList;
    }

    public void setPointList(ArrayList<HashMap<String,LineArray>> pointList) {
        this.pointList = pointList;
    }
}
