package com.bluetill.gpstrack.shared.dto;

import com.bluetill.gpstrack.shared.EntryStyle;
import com.bluetill.gpstrack.shared.LineArray;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 04.07.13
 * Time: 15:56

 */
public class DriversPathDTO extends BaseDTO {

    public static int driverNo = 0;

    private Long id;
    private String name;
    private Long driverId;
    private Long time;
    private String path;

    private String deviceId;
    private EntryStyle style;
    private ArrayList<LineArray> pointList;

    public DriversPathDTO(){
    }
    public DriversPathDTO(Long id, String name, String deviceId, EntryStyle style) {
        this.style = style;
        this.name = name;
        this.id = id;
        this.deviceId = deviceId;
    }

    public DriversPathDTO(Long id, String name, String deviceId, EntryStyle style, ArrayList<LineArray> pointList) {
        this.id = id;
        this.name = name;
        this.deviceId = deviceId;
        this.style = style;
        this.pointList = pointList;
    }

    public static int getDriverNo() {
        return driverNo;
    }

    public static void setDriverNo(int driverNo) {
        DriversPathDTO.driverNo = driverNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public EntryStyle getStyle() {
        return style;
    }

    public void setStyle(EntryStyle style) {
        this.style = style;
    }

    public ArrayList<LineArray> getPointList() {
        return pointList;
    }

    public void setPointList(ArrayList<LineArray> pointList) {
        this.pointList = pointList;
    }
}