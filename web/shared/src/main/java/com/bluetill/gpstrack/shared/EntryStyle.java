package com.bluetill.gpstrack.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 04.07.13
 * Time: 15:57

 */
public class EntryStyle implements IsSerializable {
    public String color;

    public EntryStyle(String color) {
        this.color = color;
    }

    public EntryStyle() {
    }

    public void setColor(String color) {
        this.color = color;
    }

}