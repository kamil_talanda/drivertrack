package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 11:12

 */
public class TenantsDTO extends BaseDTO {
    private Long id;
    private String name;
    private String username;

    public TenantsDTO() {
    }

    public TenantsDTO(Long id, String name, String username) {
        this.id = id;
        this.name = name;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
