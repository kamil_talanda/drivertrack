package com.bluetill.gpstrack.shared;

import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.util.ArrayList;

public class DriverData {

    public static final String SCHEDULE_CHANGE_NOTIFICATION = "Your schedule has been updated, reload to check changes.";

    private static ListGridRecord[] records;

    public static ListGridRecord[] getInstance(ArrayList<DriversPathDTO> mainDriversList) {
        if (records == null) {
            records = getRecords(mainDriversList);
        }
        return records;
    }

    public static ListGridRecord createRecord(String id, String name, String deviceId) {
        ListGridRecord record = new ListGridRecord();
        record.setAttribute("idField", id);
        record.setAttribute("nameField", name);
        record.setAttribute("deviceIdField", deviceId);
        return record;
    }

    public static ListGridRecord createRecord(String id, String bm_id, String name, String deviceId) {
        ListGridRecord record = new ListGridRecord();
        record.setAttribute("idField", id);
        record.setAttribute("bmIdField", bm_id);
        record.setAttribute("nameField", name);
        record.setAttribute("deviceIdField", deviceId);
        return record;
    }

    public static ListGridRecord[] getRecords(ArrayList<DriversPathDTO> mainDriversList){

        ArrayList<ListGridRecord> items = new ArrayList<ListGridRecord>();

        for(DriversPathDTO driversPathDTO : mainDriversList){
             items.add(createRecord(Long.toString(driversPathDTO.getId()), driversPathDTO.getName(), driversPathDTO.getDeviceId()));
        }

        return items.toArray(new ListGridRecord[items.size()]);
    }
}