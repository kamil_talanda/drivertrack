package com.bluetill.gpstrack.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 05.07.13
 * Time: 11:43

 */
public class Directions implements IsSerializable {
    private double longitude;
    private double latitude;
    private String description;

    public Directions(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Directions(double longitude, double latitude, String description) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.description = description;
    }

    public Directions() {
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
