package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:20

 */
public class Item_ListDTO extends BaseDTO {

    private Long id;
    private Long holder;
    private Long start_time;

    public Item_ListDTO() {
    }

    public Item_ListDTO(Long id, Long holder, Long start_time) {
        this.id = id;
        this.holder = holder;
        this.start_time = start_time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHolder() {
        return holder;
    }

    public void setHolder(Long holder) {
        this.holder = holder;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }
}
