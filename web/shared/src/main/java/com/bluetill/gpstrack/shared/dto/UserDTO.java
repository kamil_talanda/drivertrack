package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:20

 */
public class UserDTO extends BaseDTO {

    private String username;
    private String password;
    private boolean enabled;
    private String full_name;

    public UserDTO() {
    }

    public UserDTO(String username, String password, boolean enabled, String full_name) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.full_name = full_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;

        UserDTO userDTO = (UserDTO) o;

        if (enabled != userDTO.enabled) return false;
        if (full_name != null ? !full_name.equals(userDTO.full_name) : userDTO.full_name != null) return false;
        if (password != null ? !password.equals(userDTO.password) : userDTO.password != null) return false;
        if (username != null ? !username.equals(userDTO.username) : userDTO.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (full_name != null ? full_name.hashCode() : 0);
        return result;
    }
}
