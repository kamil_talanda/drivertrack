package com.bluetill.gpstrack.shared;

import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/05/14
 * Time: 08:40

 */
public class UserData {

    private static String ADMIN_NAME = "admin";
    private static ListGridRecord[] records;

    public static ListGridRecord[] getInstance(ArrayList<UserDTO> userList) {
        if (records == null) {
            records = getRecords(userList);
        }
        return records;
    }

    public static ListGridRecord createRecord(String name, String fullName, boolean enabled) {
        ListGridRecord record = new ListGridRecord();
        record.setAttribute("nameField", name);
        record.setAttribute("fullNameField", fullName);
        record.setAttribute("enableField", enabled);
        return record;
    }

    public static ListGridRecord[] getRecords(ArrayList<UserDTO> userList) {
        ArrayList<ListGridRecord> items = new ArrayList<ListGridRecord>();
        for (UserDTO userDTO : userList) {
            if (!userDTO.getUsername().equals(ADMIN_NAME)) {
                items.add(createRecord(userDTO.getUsername(), userDTO.getFull_name(), userDTO.isEnabled()));
            }
        }

        return items.toArray(new ListGridRecord[items.size()]);
    }
}
