package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 11:12

 */
public class CarriersDTO extends BaseDTO{
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarriersDTO() {
    }

    public CarriersDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
