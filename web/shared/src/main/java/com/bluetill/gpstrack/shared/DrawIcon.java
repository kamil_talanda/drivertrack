package com.bluetill.gpstrack.shared;

import org.apache.xerces.impl.dv.util.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 05.07.13
 * Time: 12:57

 */

public class DrawIcon {
    public String base64bytes;

    public DrawIcon(String iconColor) {

        BufferedImage bi = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        Graphics2D ig2 = bi.createGraphics();
        ig2.setColor(Color.decode(iconColor));
        ig2.fillRect(0, 0, 100, 100);


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(bi, "PNG", out);
        } catch (IOException e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        byte[] bytes = out.toByteArray();

        this.base64bytes = Base64.encode(bytes);
    }
}