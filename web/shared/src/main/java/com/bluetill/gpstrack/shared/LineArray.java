package com.bluetill.gpstrack.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 08.08.13
 * Time: 13:13

 */
public class LineArray implements IsSerializable {
    private Long time;
    private ArrayList<Directions> pointList;

    public LineArray(Long time, ArrayList<Directions> pointList) {
        this.time = time;
        this.pointList = pointList;
    }

    public LineArray() {
    }

    public Long getTime() {
        return time;
    }
    public void setTime(Long time) {
        this.time = time;
    }

    public ArrayList<Directions> getPointList() {
        return pointList;
    }
    public void setPointList(ArrayList<Directions> pointList) {
        this.pointList = pointList;
    }
}
