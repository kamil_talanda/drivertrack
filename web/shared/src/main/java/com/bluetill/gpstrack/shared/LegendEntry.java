package com.bluetill.gpstrack.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Legend Entry.
 */
public class LegendEntry implements IsSerializable {

    public int entryId;
    public String entryName;
    public String iconBase64;

    public LegendEntry(){
    }

    public LegendEntry(int id, String name){
        this.entryId = id;
        this.entryName = name;
    }

    public LegendEntry(int id, String name, String iconBase64){
        this.entryId = id;
        this.entryName = name;
        this.iconBase64 = iconBase64;
    }
}