package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:20

 */
public class SettingsDTO extends BaseDTO {

    public static final String SETTINGS_COMPANY_SYMBOL = "company_symbol";
    public static final String SETTINGS_EMAIL_SIGNATURE = "email_signature";
    public static final String SETTINGS_EMAIL_FROM = "email_from";
    public static final String SETTINGS_OFFICE_ADDRESS = "office_address";
    public static final String SETTINGS_DELIVERY_AVERAGE_DURATION = "delivery_average_duration";

    private Long id;
    private String name;
    private String value;

    public SettingsDTO() {
    }

    public SettingsDTO(Long id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
