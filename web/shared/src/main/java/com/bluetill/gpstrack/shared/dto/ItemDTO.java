package com.bluetill.gpstrack.shared.dto;

public class ItemDTO extends BaseDTO {

    private Long id;
    private String barcode;
    private String carrier;
    private String tenant;
    private Long qty;
    private Long holderId;
    private String name;
    private String destination;
    private Long addTime;
    private Long approveTime;
    private String approvePoint;
    private String signatureImage;
    private Long status;
    private String description;
    private String holderName;
    private Long deliveryTimePlanned;
    private Long itemList;
    private Long itemListOrder;
    private Long bmNo;


    public ItemDTO() {
    }

    public ItemDTO(Long id, String barcode, String carrier, String tenant, Long qty, Long holderId, String name, String destination, Long addTime, Long approveTime, String approvePoint, String signatureImage, Long status, String description, String holderName, Long deliveryTimePlanned, Long itemList, Long itemListOrder, Long bmNo) {
        this.id = id;
        this.barcode = barcode;
        this.carrier = carrier;
        this.tenant = tenant;
        this.qty = qty;
        this.holderId = holderId;
        this.name = name;
        this.destination = destination;
        this.addTime = addTime;
        this.approveTime = approveTime;
        this.approvePoint = approvePoint;
        this.signatureImage = signatureImage;
        this.status = status;
        this.description = description;
        this.holderName = holderName;
        this.deliveryTimePlanned = deliveryTimePlanned;
        this.itemList = itemList;
        this.itemListOrder = itemListOrder;
        this.bmNo = bmNo;
    }

    public ItemDTO(ItemDTO itemDTO) {
        this(
                itemDTO.getId(),
                itemDTO.getBarcode(),
                itemDTO.getCarrier(),
                itemDTO.getTenant(),
                itemDTO.getQty(),
                itemDTO.getHolderId(),
                itemDTO.getName(),
                itemDTO.getDestination(),
                itemDTO.getAddTime(),
                itemDTO.getApproveTime(),
                itemDTO.getApprovePoint(),
                itemDTO.getSignatureImage(),
                itemDTO.getStatus(),
                itemDTO.getDescription(),
                itemDTO.getHolderName(),
                itemDTO.getDeliveryTimePlanned(),
                itemDTO.getItemList(),
                itemDTO.getItemListOrder(),
                itemDTO.getBmNo()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Long getHolderId() {
        return holderId;
    }

    public void setHolderId(Long holderId) {
        this.holderId = holderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Long getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Long approveTime) {
        this.approveTime = approveTime;
    }

    public Long getDeliveryTimePlanned() {
        return deliveryTimePlanned;
    }

    public void setDeliveryTimePlanned(Long deliveryTimePlanned) {
        this.deliveryTimePlanned = deliveryTimePlanned;
    }

    public String getApprovePoint() {
        return approvePoint;
    }

    public void setApprovePoint(String approvePoint) {
        this.approvePoint = approvePoint;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public Long getItemList() {
        return itemList;
    }

    public void setItemList(Long itemList) {
        this.itemList = itemList;
    }

    public Long getItemListOrder() {
        return itemListOrder;
    }

    public void setItemListOrder(Long itemListOrder) {
        this.itemListOrder = itemListOrder;
    }

    public Long getBmNo() {
        return bmNo;
    }

    public void setBmNo(Long bmNo) {
        this.bmNo = bmNo;
    }
}
