package com.bluetill.gpstrack.shared.dto;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/12/13
 * Time: 16:43

 */
public class DriverDTO extends BaseDTO {

    private Long id;
    private Long bm_id;
    private String name;
    private String deviceId;

    public DriverDTO() {
    }

    public DriverDTO(Long id, Long bm_id, String name, String deviceId) {
        this.id = id;
        this.bm_id = bm_id;
        this.name = name;
        this.deviceId = deviceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBm_id() {
        return bm_id;
    }

    public void setBm_id(Long bm_id) {
        this.bm_id = bm_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
