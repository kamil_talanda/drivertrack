package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 21/01/14
 * Time: 07:04

 */
public interface ItemsEntityVars extends BaseVars{

    String ITEMS_TABLE = "items";

    String ITEM_BARCODE = "barcode";
    String ITEM_CARRIER = "carrier";
    String ITEM_TENANT = "tenant";
    String ITEM_QTY = "qty";
    String ITEM_HOLDER_ID = "holder_id";
    String ITEM_NAME = "name";
    String ITEM_DESTINATION = "destination";
    String ITEM_ADD_TIME = "add_time";
    String ITEM_APPROVE_TIME = "approve_time";
    String ITEM_APPROVE_POINT = "approve_point";
    String ITEM_SIGNATURE_IMAGE = "signature_image";
    String ITEM_STATUS = "status";
    String ITEM_DESCRIPTION = "description";
    String ITEM_DELIVERY_TIME_PLANNED = "delivery_time_planned";
    String ITEM_TASK_LIST = "item_list";
    String ITEM_TASK_LIST_ORDER = "item_list_order";
    String ITEM_BM_NO = "bm_no";

    String STATUS_DONE = "Delivered";
    String STATUS_IN_PROGRESS = "In Transit";
}
