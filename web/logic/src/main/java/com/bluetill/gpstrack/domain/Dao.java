package com.bluetill.gpstrack.domain;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Criterion;

import java.util.List;
import java.util.Map;

import com.bluetill.gpstrack.domain.exceptions.DaoException;

/**
 * Generic data access interface.
 * @param <E> an entity class
 * @param <K> a key class
 */
public interface Dao<K, E> {

    /**
     * Persist operation.
     * @param entity entity to persist
     * @throws DaoException - exception thrown when an error in DAO occures
     */
    void persist(E entity) throws DaoException;

    /**
     * Remove operation.
     * @param entity entity to remove
     * @throws DaoException - exception thrown when an error in DAO occures
     */
    void remove(E entity) throws DaoException;

    /**
     * Find operation.
     * @param objectId key to search
     * @return found entity
     * @throws DaoException - exception thrown when an error in DAO occures
     */
    E findById(K objectId) throws DaoException;

    /**
     * Find operation, with fetchMode.
     * @param objectId key to search
     * @param fields field names with fetchMode
     * @return found entity
     * @throws DaoException - exception thrown when an error in DAO occures
     */
    E findById(K objectId, Map<String, FetchMode> fields) throws DaoException;

    /**
     * Find operation with fetchMode and restrictions.
     * @param fields field names with fetchMode
     * @param restrictions restrictions
     * @return found entities
     * @throws DaoException - exception thrown when an error in DAO occures
     */
    List<E> findByRestrictions(final Map<String, FetchMode> fields, final List<Criterion> restrictions) throws DaoException;
}

