package com.bluetill.gpstrack.domain;

import javax.persistence.*;

import static com.bluetill.gpstrack.SettingsVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 02/12/13
 * Time: 15:27

 */
@Table(name = SETTINGS_TABLE, schema = "public")
@Entity
public class SettingsEntity {
    private Long id;
    private String name;
    private String value;
    private String username;

    public SettingsEntity() {
    }

    public SettingsEntity(Long id, String name, String value, String username) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.username = username;
    }

    @Column(name = ID)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = SETTINGS_NAME_COLUMN)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = SETTINGS_VALUE_COLUMN)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
