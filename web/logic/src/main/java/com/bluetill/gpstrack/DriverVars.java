package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface DriverVars extends BaseVars{

    String DRIVER_TABLE = "drivers";

    String unassigned = "No Driver";
    String DRIVER_NAME = "name";
    String BM_ID = "bm_id";
}
