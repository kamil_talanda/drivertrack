package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.ItemsEntityDao;
import com.bluetill.gpstrack.domain.SettingsEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.ItemEntityUtils;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 08:27

 */

@Component
public class Item_ListServiceLogic {

    @Autowired
    private ItemsEntityDao itemsEntityDao;
    @Autowired
    private SettingsEntityDao settingsDao;

    @Transactional
    public ItemDTO changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, boolean changeAllTime) {
        return changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, BaseEntityUtils.getUsername(), changeAllTime);
    }

    @Transactional
    public ItemDTO changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, String username, boolean changeAllTime) {
        HashMap<Long, ArrayList<ItemDTO>> itemListMap = createItemList(itemDTOs);
        if (changeAllTime) changeOrderList(itemListMap, oldItemListKey, newItemListKey, currentItem);
        reorderItemList(itemListMap, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder);
        calculateRoute(itemDTOs, username);
        return currentItem;
    }

    @Transactional
    public void calculateRoute(List<ItemDTO> itemDTOs) {
        calculateRoute(itemDTOs, BaseEntityUtils.getUsername());

    }

    @Transactional
    public void calculateRoute(List<ItemDTO> itemDTOs, String username) {
        HashMap<Long, ArrayList<ItemDTO>> itemListMap = createItemList(itemDTOs);
        cleanOrder(itemListMap);
        calculateRoute(itemListMap, username);
        resolveApprovePoints(itemListMap, username);
        updateDatabase(itemListMap, username);
    }

    private void calculateRoute(HashMap<Long, ArrayList<ItemDTO>> itemListMap, String username) {
        GeocoderServiceLogic geocoderServiceLogic = new GeocoderServiceLogic();
        SettingsServiceLogic settingsServiceLogic = new SettingsServiceLogic(settingsDao);
        String officeAddress = settingsServiceLogic.getByName(SettingsDTO.SETTINGS_OFFICE_ADDRESS, username).getValue();
        Long deliveryAverageDuration = Long.parseLong(settingsServiceLogic.getByName(SettingsDTO.SETTINGS_DELIVERY_AVERAGE_DURATION, username).getValue()) * 60 * 1000;

        for (Long key : itemListMap.keySet()) {
            if (key != null) {
                TreeMap<Long, ItemDTO> mapTransit = new TreeMap<Long, ItemDTO>();
                TreeMap<Long, ItemDTO> mapDelivered = new TreeMap<Long, ItemDTO>();
                for (ItemDTO itemDTO : itemListMap.get(key)) {
                    if (itemDTO.getStatus() == 0) mapTransit.put(itemDTO.getItemListOrder(), itemDTO);
                    else mapDelivered.put(itemDTO.getItemListOrder(), itemDTO);
                }
                ItemDTO previousItemDTO = null;
                for (ItemDTO itemDTO : mapDelivered.values()) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date(itemDTO.getApproveTime()));
                    if (previousItemDTO == null || previousItemDTO.getApproveTime() < itemDTO.getApproveTime())
                        previousItemDTO = itemDTO;
                }
                for (ItemDTO itemDTO : mapTransit.values()) {

                    if (previousItemDTO == null && itemDTO.getStatus() == 1) {
                        previousItemDTO = itemDTO;
                    } else {
                        if (previousItemDTO == null) {
                            Long duration = geocoderServiceLogic.getDrivingTimeInSeconds(officeAddress, itemDTO.getDestination());
                            if (duration != null) itemDTO.setApproveTime(key + duration * 1000);
                            else itemDTO.setApproveTime(null);
                        } else {
                            Long duration = geocoderServiceLogic.getDrivingTimeInSeconds(previousItemDTO.getDestination(), itemDTO.getDestination());
                            if (duration != null && previousItemDTO.getApproveTime() != null)
                                if (previousItemDTO.getStatus() == 0)
                                    itemDTO.setApproveTime(previousItemDTO.getApproveTime() + deliveryAverageDuration + duration * 1000);
                                else itemDTO.setApproveTime(previousItemDTO.getApproveTime() + duration * 1000);
                            else previousItemDTO.setApproveTime(null);
                        }
                        previousItemDTO = itemDTO;
                    }
                }
            }
        }
    }

    public void resolveApprovePoints(HashMap<Long, ArrayList<ItemDTO>> itemListMap, String username) {
        for (Long key : itemListMap.keySet()) {
            for (ItemDTO dto : itemListMap.get(key)) {
                if (dto.getDestination() != null) {
                    try {
                        Geocoder geocoder = new Geocoder();
                        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(dto.getDestination()).setLanguage("en").getGeocoderRequest();
                        GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);

                        List<GeocoderResult> resultList = geocoderResponse.getResults();
                        if (resultList.size() > 0) {
                            GeocoderGeometry data = resultList.get(0).getGeometry();
                            BigDecimal Latitude = data.getLocation().getLat();
                            BigDecimal Longitude = data.getLocation().getLng();
                            dto.setApprovePoint("POINT(" + Longitude + " " + Latitude + ")");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private HashMap createItemList(List<ItemDTO> itemDTOs) {
        HashMap<Long, ArrayList<ItemDTO>> result = new HashMap<Long, ArrayList<ItemDTO>>();

        for (ItemDTO itemDTO : itemDTOs) {
            if (!result.containsKey(itemDTO.getItemList())) {
                result.put(itemDTO.getItemList(), new ArrayList<ItemDTO>());
            }

            result.get(itemDTO.getItemList()).add(itemDTO);
        }
        return result;
    }

    private void cleanOrder(HashMap<Long, ArrayList<ItemDTO>> itemListMap) {
        for (Long key : itemListMap.keySet()) {
            TreeMap<Long, ItemDTO> map = new TreeMap<Long, ItemDTO>();
            for (ItemDTO itemDTO : itemListMap.get(key)) {
                long order = itemDTO.getItemListOrder();
                while (map.containsKey(order)) {
                    order += 1;
                    itemDTO.setItemListOrder(order);
                }
                map.put(order, itemDTO);
            }
            long i = 1;
            for (ItemDTO itemDTO : map.values()) {
                itemDTO.setItemListOrder(i++);
            }
        }
    }

    private void reorderItemList(HashMap<Long, ArrayList<ItemDTO>> itemListMap, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder) {

        if (newListOrder < 1) newListOrder = 1;

        boolean newItemListKeyExists = false;
        currentItem.setItemListOrder(newListOrder);
        currentItem.setItemList(newItemListKey);

        for (Long key : itemListMap.keySet()) {
            if (key != null) {
                ArrayList<ItemDTO> currentItemList = itemListMap.get(key);
                if (key == oldItemListKey) {
                    itemListMap.get(oldItemListKey).remove(currentItem);
                }
                if (key == newItemListKey && currentItemList != null) {
                    changeOrder(currentItemList, oldListOrder, newListOrder);
                }
                if (key == newItemListKey) {
                    currentItemList.add(currentItem);
                    newItemListKeyExists = true;
                }
            }
        }
        if (!newItemListKeyExists) {
            ArrayList<ItemDTO> itemDTOArrayList = new ArrayList<ItemDTO>();
            itemDTOArrayList.add(currentItem);
            itemListMap.put(newItemListKey, itemDTOArrayList);
        }
    }

    private void updateDatabase(HashMap<Long, ArrayList<ItemDTO>> itemListMap, String username) {
        for (ArrayList<ItemDTO> itemDTOArrayList : itemListMap.values()) {
            for (ItemDTO itemDTO : itemDTOArrayList) {
                itemsEntityDao.set(ItemEntityUtils.DtoToEntity(itemDTO, username));
            }
        }
    }

    private void changeOrderList(HashMap<Long, ArrayList<ItemDTO>> itemListMap, long oldListOrder, long newListOrder, ItemDTO currentItem) {
        for (Long key : itemListMap.keySet()) {
            List<ItemDTO> itemDTOArrayList = itemListMap.get(key);
            if (key.equals(oldListOrder))
                itemDTOArrayList.add(currentItem);
            for (ItemDTO itemDTO : itemDTOArrayList) {
                if (itemDTO.getItemList() == oldListOrder && itemDTO.getStatus() == 0) {

                    Calendar calendarItemList = Calendar.getInstance();
                    calendarItemList.setTimeInMillis(newListOrder);

                    Calendar calendarCurrentItem = Calendar.getInstance();
                    calendarCurrentItem.setTimeInMillis(itemDTO.getApproveTime());
                    calendarCurrentItem.set(Calendar.YEAR, calendarItemList.get(Calendar.YEAR));
                    calendarCurrentItem.set(Calendar.DAY_OF_YEAR, calendarItemList.get(Calendar.DAY_OF_YEAR));

                    itemDTO.setItemList(calendarCurrentItem.getTimeInMillis());
                    itemDTO.setItemList(newListOrder);
                }

            }
        }
        itemListMap.put(newListOrder, itemListMap.get(oldListOrder));
        itemListMap.remove(oldListOrder);
    }

    private void changeOrder(ArrayList<ItemDTO> itemDTOs, long oldListOrder, long newListOrder) {
        for (ItemDTO itemDTO : itemDTOs) {
            long listOrder = itemDTO.getItemListOrder() != null ? itemDTO.getItemListOrder() : -1l;
            if ((newListOrder < oldListOrder && listOrder >= newListOrder && listOrder < oldListOrder)) {
                listOrder += 1;
                itemDTO.setItemListOrder(listOrder);

            } else if (newListOrder > oldListOrder && listOrder > oldListOrder && listOrder <= newListOrder) {
                listOrder -= 1;
                itemDTO.setItemListOrder(listOrder);
            }
        }
    }

}