package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.TenantsVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TenantsEntityDao extends BaseDao<Integer, TenantsEntity> implements ITenantsEntityDao, TenantsVars {

    @Override
    public ArrayList<TenantsEntity> getAll(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return (ArrayList<TenantsEntity>) criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    @Override
    public TenantsEntity set(TenantsEntity tenantsEntity) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, tenantsEntity.getUsername()));
            criteria.add(Restrictions.eq(TENANT_NAME, tenantsEntity.getName()));
            if (criteria.list().size() > 0) return null;
            persist(tenantsEntity);
            for (TenantsEntity entity : (List<TenantsEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteByName(String name, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(TENANT_NAME, name));
            List<TenantsEntity> tenantsEntities = (List<TenantsEntity>) criteria.list();
            for (TenantsEntity entity : tenantsEntities) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }
}
