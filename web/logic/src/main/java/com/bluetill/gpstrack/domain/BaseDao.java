package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.domain.exceptions.DaoException;
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generic Dao basic implementation.
 */
public class BaseDao<K extends Serializable, E> implements Dao<K, E> {

    protected Class<E> persistentClass;
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public BaseDao() {
        super();
        persistentClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Autowired
    @Qualifier("bluetillSessionFactory")
    public void setSessionFactoryBluetill(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
    @SuppressWarnings("unchecked")
    public List<E> findAll() throws DaoException {
        try {
            return getCriteria().list();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    protected Criteria getCriteria() throws DaoException {
        try {
            return this.sessionFactory.getCurrentSession().createCriteria(persistentClass);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void persist(final E entity) throws DaoException {
        try {
            this.sessionFactory.getCurrentSession().saveOrUpdate(entity);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void remove(final E entity) throws DaoException {
        try {
            this.sessionFactory.getCurrentSession().delete(entity);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public E findById(final K objectId) throws DaoException {
        try {
            return (E) this.sessionFactory.getCurrentSession().get(persistentClass, objectId);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public E findById(final K objectId, final Map<String, FetchMode> fields) throws DaoException {
        try {
            E object = null;
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.idEq(objectId));
            if (fields != null) {
                final Set<String> keys = fields.keySet();
                for (String key : keys) {
                    criteria.setFetchMode(key, fields.get(key));
                }
            }
            object = (E) criteria.uniqueResult();

            return object;
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public final List<E> findByRestrictions(final Map<String, FetchMode> fields,
                                            final List<Criterion> restrictions) throws DaoException {
        try {
            final Criteria criteria = getCriteria();
            if (fields != null) {
                final Set<String> keys = fields.keySet();
                for (String key : keys) {
                    criteria.setFetchMode(key, fields.get(key));
                }
            }
            if (restrictions != null) {
                for (Criterion key : restrictions) {
                    criteria.add(key);
                }
            }
            return criteria.list();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    public final List<E> findByPointIntersect(String property, Geometry point) throws DaoException {

        try {
            final Criteria criteria = getCriteria();
            criteria.add(SpatialRestrictions.intersects(property, point));

            return criteria.list();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
