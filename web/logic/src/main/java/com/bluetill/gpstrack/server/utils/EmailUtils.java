package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.shared.dto.ItemDTO;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/02/14
 * Time: 09:28

 */
public class EmailUtils implements EmailVars {

    private static final String SHOW_TIME_FORMAT = "HH:mm dd/MM/yyyy";

    public static boolean sendEmail(ItemDTO item, String toAddress, String emailFrom, String emailSignature) {


        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", EMAIL_HOST);
        Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailFrom));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(toAddress));
            message.setSubject("Order number: " + item.getBarcode());
            message.setContent(generateEmailBody(item, emailSignature), EMAIL_TYPE);
            Transport.send(message);
            return true;
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
        return false;
    }

    public static String generateEmailBody(ItemDTO item, String emailSignature) {
        String result = "<body>Hi,</br></br>";
        result += "Barcode: " + item.getBarcode() + "</br>";
        if (item.getName() != null) result += "Receiver: " + item.getName() + "</br>";
        if (item.getQty() != null) result += "QTY: " + item.getQty() + "</br>";
        if (item.getTenant() != null) result += "Tenant: " + item.getTenant() + "</br>";
        if (item.getCarrier() != null) result += "Carrier: " + item.getCarrier() + "</br>";
        if (item.getDestination() != null) result += "Destination: " + item.getDestination() + "</br>";
        //TODO holder id to holder name
        if (item.getHolderId() != -1) result += "Driver: " + item.getHolderId() + "</br>";

        switch ((int) (long) item.getStatus()) {
            case 0:
                result += "Status: " + STATUS_IN_PROGRESS + "</br>";
                break;
            case 1:
                result += "Status: " + STATUS_DONE + "</br>";
                break;
            default:
                break;
        }

        TimeZone tz = TimeZone.getTimeZone("Australia/Sydney");
        long currentTimeOffset = tz.getOffset(new Date().getTime());

        if (item.getAddTime() != null)
            result += "Add Time: " + parseTime(new Date(item.getAddTime() + currentTimeOffset)) + "</br>";
        if (item.getApproveTime() != null)
            result += "Approve Time: " + parseTime(new Date(item.getApproveTime() + currentTimeOffset)) + "</br>";
        if (item.getSignatureImage() != null)
            result += "Signature:</br>" + "<img src='data:image/png;base64, " + item.getSignatureImage() + "'>" + "</br>";
        result += "</br>" + emailSignature + "</body>";

        return result;
    }

    private static String parseTime(Date date) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(SHOW_TIME_FORMAT);
        if (date != null) return dateTimeFormat.format(date);
        else return null;
    }
}
