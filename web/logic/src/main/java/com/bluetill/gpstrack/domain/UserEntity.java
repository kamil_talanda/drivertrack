package com.bluetill.gpstrack.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.bluetill.gpstrack.UserVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:14

 */
@Table(name = USER_TABLE, schema = "public")
@Entity
public class UserEntity {

    private String username;
    private String password;
    private boolean enabled;
    private String full_name;

    public UserEntity() {
    }

    public UserEntity(String username, String password, boolean enabled, String full_name) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.full_name = full_name;
    }

    @Column(name = USERNAME)
    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = USER_PASSWORD)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = USER_ENABLED)
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = USER_FULL_NAME)
    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserEntity)) return false;

        UserEntity that = (UserEntity) o;

        if (enabled != that.enabled) return false;
        if (full_name != null ? !full_name.equals(that.full_name) : that.full_name != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (full_name != null ? full_name.hashCode() : 0);
        return result;
    }
}
