package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.ItemsEntityVars;

import java.util.Date;
import java.util.List;

public interface IItemsEntityDao extends ItemsEntityVars {

    List<ItemsEntity> getAll(String username);

    List<ItemsEntity> getAll(Date startFilterDate, Date endFilterDate, String username);

    ItemsEntity get(Long itemId, String username);

    List<ItemsEntity> getByBarcode(String barcode, String username);

    List<ItemsEntity> getByDriverId(Long holderId, String username);

    ItemsEntity set(ItemsEntity entity);

    boolean deleteById(Long id, String username);

    List<ItemsEntity> getUncommittedItems(String username);

    String getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight);

    int getItemsNumber();

    int getMaxItemId();

    List<ItemsEntity> getInProgressByDriverId(Long holderId, String username);

    List<ItemsEntity> getByBarcode(String barcode);

    ItemsEntity setFromUploadFIle(ItemsEntity entity);

    boolean deleteByList(Long list, Long holderId, String username);

    Integer getNumberOfOpenBefore(ItemsEntity entity, String username);

    List<ItemsEntity> getByDriverIdByTime(Long holderId, Long startTime, Long endTime, String username);
}