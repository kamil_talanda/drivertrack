package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.DriverEntity;
import com.bluetill.gpstrack.domain.DriverEntityDao;
import com.bluetill.gpstrack.domain.DriversPathEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.shared.DriverData;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.bluetill.gpstrack.shared.EntryStyle;
import com.bluetill.gpstrack.shared.LineArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 08:27

 */

@Component
public class DriverPathServiceLogic {

    private static String[] listOfColors = {
            "#FF8000", "#00CD48", "#0083FF", "#000000", "#A5CF14", "#E250FE", "#FF3800", "#775DFF", "#00D1D6"
    };
    private static HashMap<Long, String> msgList = new HashMap<Long, String>();
    @Autowired
    private DriverEntityDao driverEntityDao;
    @Autowired
    private DriversPathEntityDao driversPathEntityDao;

    @Transactional
    public static void setMessage(Long driverId, String msg) {

        boolean exists = false;
        if (msg.equals(DriverData.SCHEDULE_CHANGE_NOTIFICATION) && msgList != null && msgList.get(driverId) != null) {
            for (String message : msgList.get(driverId).split("\n")) {
                if (message.equals(DriverData.SCHEDULE_CHANGE_NOTIFICATION)) exists = true;
            }
        }
        if (!exists) {
            if (msgList.get(driverId) != null) {
                msg = msgList.get(driverId) + "\n" + msg;
            }
            msgList.put(driverId, msg);
        }
    }

    public static String getMessage(Long driverId) {
        Long resultKey = null;
        for (Long key : msgList.keySet()) {
            if (key.equals(driverId)) resultKey = key;
        }
        return msgList.remove(resultKey);
    }

    @Transactional
    public String syncDriver(Long driverId) {
        return getMessage(driverId);
    }

    public DriversPathDTO nextDriver(ArrayList<LineArray> pointList, Long driverId, String username) {
        DriverEntity entity = driverEntityDao.get(driverId, username);
        EntryStyle dStyle = new EntryStyle(listOfColors[(int) (long) driverId % listOfColors.length]);
        return new DriversPathDTO(driverId, entity.getName(), entity.getAndroid_id(), dStyle, pointList);
    }

    @Transactional
    public List<DriversPathDTO> getAll() {
        return getAll(BaseEntityUtils.getUsername());
    }

    @Transactional
    public List<DriversPathDTO> getAll(String username) {
        ArrayList<DriversPathDTO> dtoArrayList = new ArrayList();

        for (DriverEntity entity : driverEntityDao.getAll(username)) {
            dtoArrayList.add(this.nextDriver(null, entity.getId(), username));
        }
        return dtoArrayList;
    }

    @Transactional
    public DriversPathDTO getDriverTrack(Long driverId, Long day) {
        return getDriverTrack(driverId, day, BaseEntityUtils.getUsername());
    }

    @Transactional
    public DriversPathDTO getDriverTrack(Long driverId, Long day, String username) {

        ArrayList<LineArray> pointList = driversPathEntityDao.getPointList(driverId, day, username);
        return this.nextDriver(pointList, driverId, username);
    }

    @Transactional
    public boolean saveLine(Long driverId, String line, String username) {
        return driversPathEntityDao.saveLine(driverId, line, username);
    }

    @Transactional
    public String getLastDriverPosition(long driverId) {
        return driversPathEntityDao.getDriverLastPosition(driverId);
    }
}