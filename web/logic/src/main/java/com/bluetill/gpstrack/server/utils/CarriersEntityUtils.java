package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.CarriersEntity;
import com.bluetill.gpstrack.shared.dto.CarriersDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class CarriersEntityUtils {

    public static CarriersDTO EntityToDto(CarriersEntity entity) {
        return (entity != null) ? new CarriersDTO(entity.getId(), entity.getName()) : null;
    }

    public static CarriersEntity DtoToEntity(CarriersDTO dto, String username) {
        return (dto != null) ? new CarriersEntity(dto.getId(), dto.getName(), username) : null;
    }
}
