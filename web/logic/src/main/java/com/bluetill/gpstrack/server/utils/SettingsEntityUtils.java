package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.SettingsEntity;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class SettingsEntityUtils {

    public static SettingsDTO EntityToDto(SettingsEntity entity) {
        return (entity != null) ? new SettingsDTO(entity.getId(), entity.getName(), entity.getValue()) : null;
    }

    public static SettingsEntity DtoToEntity(SettingsDTO dto, String username){
        return (dto != null) ? new SettingsEntity(dto.getId(), dto.getName(), dto.getValue(), username) : null;
    }
}
