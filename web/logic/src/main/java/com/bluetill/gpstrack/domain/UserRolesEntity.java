package com.bluetill.gpstrack.domain;

import javax.persistence.*;

import static com.bluetill.gpstrack.UserRolesVars.*;
/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 16/05/14
 * Time: 09:00

 */
@Table(name = USER_ROLES_TABLE, schema = "public")
@Entity
public class UserRolesEntity {

    private Long user_role_id;
    private String username;
    private String role;

    @Column(name = USER_ROLES_ID)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getUser_role_id() {
        return user_role_id;
    }

    public void setUser_role_id(Long user_role_id) {
        this.user_role_id = user_role_id;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = USER_ROLES_ROLE)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserRolesEntity() {
    }

    public UserRolesEntity(String username, String role) {
        this.username = username;
        this.role = role;
    }
}
