package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface BaseVars{

    String USERNAME = "username";

    String PASSWORD = "password";
    String ID = "id";
    String ANDROID_ID = "android_id";
}
