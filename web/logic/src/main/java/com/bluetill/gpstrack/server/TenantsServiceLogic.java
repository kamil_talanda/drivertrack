package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.TenantsEntity;
import com.bluetill.gpstrack.domain.TenantsEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.TenantsEntityUtils;
import com.bluetill.gpstrack.shared.dto.TenantsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 08:27

 */

@Component
public class TenantsServiceLogic {

    @Autowired
    private TenantsEntityDao tenantsEntityDao;

    @Transactional
    public ArrayList<TenantsDTO> getAll() {
        return getAll(BaseEntityUtils.getUsername());
    }

    @Transactional
    public ArrayList<TenantsDTO> getAll(String username) {
        ArrayList<TenantsDTO> dtoArrayList = new ArrayList();
        for (TenantsEntity entity : tenantsEntityDao.getAll(username)) {
            dtoArrayList.add(TenantsEntityUtils.EntityToDto(entity));
        }
        return dtoArrayList;
    }

    @Transactional
    public TenantsDTO set(TenantsDTO dto) {
        return set(dto, BaseEntityUtils.getUsername());
    }


    @Transactional
    public TenantsDTO set(TenantsDTO dto, String username) {
        return TenantsEntityUtils.EntityToDto(tenantsEntityDao.set(TenantsEntityUtils.DtoToEntity(dto, username)));
    }

    @Transactional
    public boolean deleteByName(String name) {
        return tenantsEntityDao.deleteByName(name, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean deleteByName(String name, String username) {
        return tenantsEntityDao.deleteByName(name, username);
    }
}