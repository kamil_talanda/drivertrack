package com.bluetill.gpstrack.server;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:37

 */
@Component
public class GeocoderServiceLogic {

    @Transactional
    public String[] geocodeAddress(String address) {
        if (address != null) {
            try {
                Geocoder geocoder = new Geocoder();
                GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("en").getGeocoderRequest();
                GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);

                List<GeocoderResult> resultList = geocoderResponse.getResults();
                if (resultList.size() > 0) {
                    GeocoderGeometry data = resultList.get(0).getGeometry();
                    BigDecimal Latitude = data.getLocation().getLat();
                    BigDecimal Longitude = data.getLocation().getLng();
                    return new String[]{"POINT(" + Longitude + " " + Latitude + ")", resultList.get(0).getFormattedAddress()};
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return null;
    }

    @Transactional
    public Long getDrivingTimeInSeconds(String origin, String destination) {
        if (origin != null && destination != null) {
            GeoApiContext context = new GeoApiContext().setQueryRateLimit(3)
                    .setApiKey("AIzaSyC27EQT0EGYYscczbALcQG28b-DX3c_4DY");

            try {
                DistanceMatrix matrix = DistanceMatrixApi.newRequest(context)
                        .origins(origin)
                        .destinations(destination)
                        .await();

                if (matrix != null && matrix.rows.length > 0 && matrix.rows[0].elements.length > 0)
                    return matrix.rows[0].elements[0].duration.inSeconds;

            } catch (Exception e) {

            }
        }
        return null;
    }

}
