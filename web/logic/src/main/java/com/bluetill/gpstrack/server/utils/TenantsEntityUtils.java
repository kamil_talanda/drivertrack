package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.TenantsEntity;
import com.bluetill.gpstrack.shared.dto.TenantsDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class TenantsEntityUtils {

    public static TenantsDTO EntityToDto(TenantsEntity entity) {
        return (entity != null) ? new TenantsDTO(entity.getId(), entity.getName(), entity.getUsername()) : null;
    }

    public static TenantsEntity DtoToEntity(TenantsDTO dto, String username){
        return (dto != null) ? new TenantsEntity(dto.getId(), dto.getName(), username) : null;
    }
}
