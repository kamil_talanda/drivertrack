package com.bluetill.gpstrack.domain;

import java.util.ArrayList;

public interface IItem_ListEntityDao {

    ArrayList<Item_ListEntity> getAll(String username);

    Item_ListEntity get(Long id, String username);

    Item_ListEntity set(Item_ListEntity setEntity);

    boolean delete(Long id, String username);
}
