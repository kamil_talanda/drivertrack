package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface SettingsVars extends BaseVars{

    String SETTINGS_TABLE = "settings";

    String SETTINGS_NAME_COLUMN = "name";
    String SETTINGS_VALUE_COLUMN = "value";
}
