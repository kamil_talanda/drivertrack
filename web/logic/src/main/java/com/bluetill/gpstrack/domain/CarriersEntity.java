package com.bluetill.gpstrack.domain;

import javax.persistence.*;

import static com.bluetill.gpstrack.CarriersVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 11.07.13
 * Time: 10:13

 */
@Table(name = CARRIERS_TABLE, schema = "public")
@Entity
public class CarriersEntity {
    private Long id;
    private String name;
    private String username;

    @Column(name = ID)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = CARRIERS_NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CarriersEntity() {
    }

    public CarriersEntity(Long id, String name, String username) {
        this.id = id;
        this.name = name;
        this.username = username;
    }

}
