package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.domain.exceptions.DaoException;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:20

 */
public interface ISettingsEntityDao {

    SettingsEntity getByName(String name, String username) throws DaoException;

    SettingsEntity set(SettingsEntity entity) throws DaoException;
}
