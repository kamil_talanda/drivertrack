package com.bluetill.gpstrack.domain;

import javax.persistence.*;

import static com.bluetill.gpstrack.DriverVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 11.07.13
 * Time: 10:13

 */
@Table(name = DRIVER_TABLE, schema = "public")
@Entity
public class DriverEntity{
    private Long id;
    private Long bm_id;
    private String name;
    private String android_id;
    private String username;

    @Column(name = ID)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = BM_ID)
    public Long getBm_id() {
        return bm_id;
    }

    public void setBm_id(Long bm_id) {
        this.bm_id = bm_id;
    }

    @Column(name = DRIVER_NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = ANDROID_ID)
    public String getAndroid_id() {
        return android_id;
    }

    public void setAndroid_id(String android_id) {
        this.android_id = android_id;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DriverEntity() {
    }

    public DriverEntity(Long id, Long bm_id, String name, String android_id, String username) {
        this.id = id;
        this.bm_id = bm_id;
        this.name = name;
        this.android_id = android_id;
        this.username = username;
    }
}
