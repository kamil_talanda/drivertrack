package com.bluetill.gpstrack.domain;

import java.util.ArrayList;

public interface IDriversEntityDao {

    ArrayList<DriverEntity> getAll(String username);

    DriverEntity get(Long id, String username);

    DriverEntity getByName(String driverName, String username);

    DriverEntity set(DriverEntity driverEntity);

    boolean delete(DriverEntity driverEntity, String username);

    boolean deleteByName(String driverName, String username);

    boolean checkAndroidId(DriverEntity driverEntity, String username);

    boolean isUserEmpty(DriverEntity driverEntity, String username);

    boolean checkAndroidIdByName(DriverEntity driverEntity, String username);

    DriverEntity getByBmId(Long bm_id, String username);

    Long getMaxBmId(String username);
}
