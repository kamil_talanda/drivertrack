package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface UserRolesVars extends BaseVars{

    String USER_ROLES_TABLE = "user_roles";

    String USER_ROLES_ID = "user_role_id";
    String USER_ROLES_ROLE = "role";

    String ROLE_USER = "ROLE_USER";

}
