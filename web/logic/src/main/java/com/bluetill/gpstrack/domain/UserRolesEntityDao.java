package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.UserRolesVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:25

 */
@Repository
public class UserRolesEntityDao extends BaseDao<String, UserRolesEntity> implements IUserRolesEntityDao, UserRolesVars {

    @Override
    public boolean setUserRole_user(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(USER_ROLES_ROLE, ROLE_USER));
            if(criteria.list().size() > 0) return false;
            UserRolesEntity userRolesEntity = new UserRolesEntity(username, ROLE_USER);
            persist(userRolesEntity);
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }
}
