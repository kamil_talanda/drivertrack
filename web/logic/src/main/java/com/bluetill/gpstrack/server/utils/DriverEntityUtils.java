package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.DriverEntity;
import com.bluetill.gpstrack.shared.dto.DriverDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class DriverEntityUtils {

    public static DriverDTO EntityToDto(DriverEntity entity) {
        return (entity != null) ? new DriverDTO(entity.getId(), entity.getBm_id(), entity.getName(), entity.getAndroid_id()) : null;
    }

    public static DriverEntity DtoToEntity(DriverDTO dto, String username){
        return (dto != null) ? new DriverEntity(dto.getId(), dto.getBm_id(), dto.getName(), dto.getDeviceId(), username) : null;
    }
}
