package com.bluetill.gpstrack.domain;

import java.util.ArrayList;

public interface ITenantsEntityDao {


    ArrayList<TenantsEntity> getAll(String username);

    TenantsEntity set(TenantsEntity tenantsEntity);


    boolean deleteByName(String name, String username);
}
