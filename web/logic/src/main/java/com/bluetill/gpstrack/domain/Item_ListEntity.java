package com.bluetill.gpstrack.domain;

import javax.persistence.*;

import static com.bluetill.gpstrack.Item_ListVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 11.07.13
 * Time: 10:13

 */
@Table(name = ITEM_LIST_TABLE, schema = "public")
@Entity
public class Item_ListEntity {
    private Long id;
    private Long holder;
    private Long start_time;
    private String username;

    public Item_ListEntity() {
    }

    public Item_ListEntity(Long id, Long holder, Long start_time, String username) {
        this.id = id;
        this.holder = holder;
        this.start_time = start_time;
        this.username = username;
    }

    @Column(name = ID)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = ITEM_LIST_HOLDER)
    public Long getHolder() {
        return holder;
    }

    @Column(name = ITEM_LIST_START_TIME)
    public void setHolder(Long holder) {
        this.holder = holder;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
