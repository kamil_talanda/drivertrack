package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.ItemsEntity;
import com.bluetill.gpstrack.ItemsEntityVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 27/01/14
 * Time: 07:03

 */
public class ItemEntityUtils implements ItemsEntityVars {

    public static ItemDTO EntityToDto(ItemsEntity entity, String holderName) {
        return (entity != null) ? new ItemDTO(
                entity.getId(),
                entity.getBarcode(),
                entity.getCarrier(),
                entity.getTenant(),
                entity.getQty(),
                entity.getHolder_id(),
                entity.getName(),
                entity.getDestination(),
                entity.getAdd_time(),
                entity.getApprove_time(),
                entity.getApprove_pointWKT(),
                entity.getSignature_image(),
                entity.getStatus(),
                entity.getDescription(),
                holderName,
                entity.getDelivery_time_planned(),
                entity.getItem_list(),
                entity.getItem_list_order(),
                entity.getBm_no()
        ) : null;
    }

    public static ItemsEntity DtoToEntity(ItemDTO dto, String username){
        return (dto != null) ? new ItemsEntity(
                dto.getId(),
                dto.getBarcode(),
                dto.getCarrier(),
                dto.getTenant(),
                dto.getQty(),
                dto.getHolderId(),
                dto.getName(),
                dto.getDestination(),
                dto.getAddTime(),
                dto.getApproveTime(),
                dto.getApprovePoint(),
                dto.getSignatureImage(),
                dto.getStatus(),
                dto.getDescription(),
                dto.getDeliveryTimePlanned(),
                dto.getItemList(),
                dto.getItemListOrder(),
                dto.getBmNo(),
                username
        ) : null;
    }
}