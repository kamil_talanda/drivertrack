package com.bluetill.gpstrack.domain;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

import javax.persistence.*;

import static com.bluetill.gpstrack.DriversPathVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 16:16

 */
@Table(name = DRIVERS_PATH_TABLE, schema = "public")
@Entity
public class DriversPathEntity {

    private Long id;
    private Long driver_id;
    private Long time;
    private Geometry path;
    private String username;

    @Column(name = ID)
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = DRIVERS_PATH_DRIVER_ID)
    public Long getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(Long driver_id) {
        this.driver_id = driver_id;
    }

    @Column(name = DRIVERS_PATH_TIME)
    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Column(name = DRIVERS_PATH_PATH)
    public Geometry getPath() {
        return path;
    }

    public void setPath(Geometry path) {
        this.path = path;
    }

    @Transient
    public String getPathWKT() {
        if (this.path == null) {
            return null;
        }
        WKTWriter writer = new WKTWriter();
        return writer.write(this.path);
    }

    public void setPathWKT(String geometryWKT) throws ParseException {
        if (geometryWKT == null) {
            return;
        }
        WKTReader reader = new WKTReader(new GeometryFactory(new PrecisionModel()));
        this.path = reader.read(geometryWKT);
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
