package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface DriversPathVars extends BaseVars{

    String DRIVERS_PATH_TABLE = "drivers_path";

    long DAY_MILLISECONDS = 1000 * 60 * 60 * 24;

    String DRIVERS_PATH_DRIVER_ID = "driver_id";
    String DRIVERS_PATH_TIME = "time";
    String DRIVERS_PATH_PATH = "path";
}
