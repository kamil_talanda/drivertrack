package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface Item_ListVars extends BaseVars{

    String ITEM_LIST_TABLE = "item_list";

    String ITEM_LIST_HOLDER = "name";

    String ITEM_LIST_START_TIME = "start_time";

}
