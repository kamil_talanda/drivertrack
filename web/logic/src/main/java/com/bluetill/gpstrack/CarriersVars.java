package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface CarriersVars extends BaseVars{

    String NO_CARRIER = "No Carrier";

    String CARRIERS_TABLE = "carriers";

    String CARRIERS_NAME = "name";

}
