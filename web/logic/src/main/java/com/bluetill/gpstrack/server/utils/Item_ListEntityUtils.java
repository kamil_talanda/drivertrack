package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.Item_ListEntity;
import com.bluetill.gpstrack.shared.dto.Item_ListDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class Item_ListEntityUtils {

    public static Item_ListDTO EntityToDto(Item_ListEntity entity) {
        return (entity != null) ? new Item_ListDTO(entity.getId(),entity.getHolder(), entity.getStart_time()) : null;
    }

    public static Item_ListEntity DtoToEntity(Item_ListDTO dto, String username) {
        return (dto != null) ? new Item_ListEntity(dto.getId(), dto.getHolder(), dto.getStart_time(), username) : null;
    }
}
