package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface TenantsVars extends BaseVars{

    String NO_TENANT = "No Tenant";

    String TENANTS_TABLE = "tenants";

    String TENANT_NAME = "name";

}
