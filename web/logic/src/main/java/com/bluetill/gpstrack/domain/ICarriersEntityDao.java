package com.bluetill.gpstrack.domain;

import java.util.ArrayList;

public interface ICarriersEntityDao {


    ArrayList<CarriersEntity> getAll(String username);

    CarriersEntity set(CarriersEntity carriersEntity);

    boolean deleteByName(String name, String username);

    CarriersEntity get(Long id, String username);
}
