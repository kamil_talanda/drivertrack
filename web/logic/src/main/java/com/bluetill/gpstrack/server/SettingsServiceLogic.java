package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.SettingsVars;
import com.bluetill.gpstrack.domain.SettingsEntityDao;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.SettingsEntityUtils;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:23
 */
@Component
public class SettingsServiceLogic implements SettingsVars {

    @Autowired
    private SettingsEntityDao settingsDao;


    public SettingsServiceLogic() {
    }

    public SettingsServiceLogic(SettingsEntityDao settingsDao) {
        this.settingsDao = settingsDao;
    }


    @Transactional(readOnly = true)
    public SettingsDTO getByName(String name) {
        return getByName(name, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public SettingsDTO getByName(String name, String username) {
        try {
            return SettingsEntityUtils.EntityToDto(settingsDao.getByName(name, username));
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public SettingsDTO set(SettingsDTO dto) {
        return set(dto, BaseEntityUtils.getUsername());
    }

    @Transactional
    public SettingsDTO set(SettingsDTO dto, String username) {
        try {
            return SettingsEntityUtils.EntityToDto(settingsDao.set(SettingsEntityUtils.DtoToEntity(dto, username)));
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

}
