package com.bluetill.gpstrack.domain.exceptions;

/**
 * Dao exception
 */
@SuppressWarnings("serial")
public class DaoException extends Exception {

    public DaoException() { }

    public DaoException(String message) { super(message); }

    public DaoException(Exception exception) { super(exception); }
}

