package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.ItemsEntityVars;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/02/14
 * Time: 09:29

 */
public interface EmailVars extends ItemsEntityVars {
    String EMAIL_FROM = "admin@bluetill.com";
    String EMAIL_HOST = "localhost";
    String EMAIL_TYPE = "text/html";
}
