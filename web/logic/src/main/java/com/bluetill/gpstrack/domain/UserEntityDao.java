package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.UserVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:25

 */
@Repository
public class UserEntityDao extends BaseDao<String, UserEntity> implements IUserEntityDao, UserVars {

    @Override
    public List<UserEntity> getAll() {
        final Criteria criteria;
        try {
            criteria = getCriteria();
            return (ArrayList<UserEntity>) criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserEntity getByName(String username) {
        final Criteria criteria;
        try {
            criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return (UserEntity) criteria.uniqueResult();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserEntity set(UserEntity userEntity) {
        try {
            persist(userEntity);
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, userEntity.getUsername()));
            for (UserEntity entity : (List<UserEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteByName(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            List<UserEntity> userEntities = (List<UserEntity>) criteria.list();
            for (UserEntity entity : userEntities) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }
}
