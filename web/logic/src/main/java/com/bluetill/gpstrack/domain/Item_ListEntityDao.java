package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.Item_ListVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class Item_ListEntityDao extends BaseDao<Integer, Item_ListEntity> implements IItem_ListEntityDao, Item_ListVars {

    @Override
    public ArrayList<Item_ListEntity> getAll(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return (ArrayList<Item_ListEntity>) criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Item_ListEntity get(Long id, String username) {
        try {
            final Criteria criteria;
            criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, id));
            for (Item_ListEntity entity : (List<Item_ListEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Item_ListEntity set(Item_ListEntity setEntity) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, setEntity.getUsername()));
            criteria.add(Restrictions.eq(ITEM_LIST_HOLDER, setEntity.getHolder()));
            criteria.add(Restrictions.eq(ITEM_LIST_START_TIME, setEntity.getStart_time()));
            if (criteria.list().size() > 0) return null;
            persist(setEntity);
            for (Item_ListEntity entity : (List<Item_ListEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(Long id, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, id));
            for (Item_ListEntity entity : (List<Item_ListEntity>) criteria.list()) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }
}
