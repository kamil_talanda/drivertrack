package com.bluetill.gpstrack;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 06:56

 */
public interface UserVars extends BaseVars{

    String USER_TABLE = "users";

    String USER_PASSWORD = "password";
    String USER_ENABLED = "enabled";
    String USER_FULL_NAME = "full_name";

}
