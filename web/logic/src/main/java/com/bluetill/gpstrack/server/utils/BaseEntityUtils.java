package com.bluetill.gpstrack.server.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:10

 */
public class BaseEntityUtils {
    public static String getUsername() {
        String username = "";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) username = auth.getName();
        return username;
    }
}
