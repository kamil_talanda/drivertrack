package com.bluetill.gpstrack.domain;


import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:18

 */
public interface IUserEntityDao {

    List<UserEntity> getAll();

    UserEntity getByName(String username);

    UserEntity set(UserEntity userEntity);

    boolean deleteByName(String username);
}
