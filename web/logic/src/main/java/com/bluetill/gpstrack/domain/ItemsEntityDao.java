package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.ItemsEntityVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.postgresql.util.Base64;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("itemsDao")
public class ItemsEntityDao extends BaseDao<Integer, ItemsEntity> implements IItemsEntityDao, ItemsEntityVars {
    private static final Long UNCOMMITTED_HOLDER_ID = -1l;

    @Override
    public List<ItemsEntity> getAll(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getAll(Date startFilterDate, Date endFilterDate, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.between(ITEM_ADD_TIME, startFilterDate.getTime(), endFilterDate.getTime()));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ItemsEntity get(Long itemId, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, itemId));
            List<ItemsEntity> list = criteria.list();
            if (list.size() > 0) {
                return list.get(0);
            }
            return null;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getByBarcode(String barcode) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(ITEM_BARCODE, barcode));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ItemsEntity setFromUploadFIle(ItemsEntity entity) {
        try {
            persist(entity);
            return entity;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getByBarcode(String barcode, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ITEM_BARCODE, barcode));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getNumberOfOpenBefore(ItemsEntity entity, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(ITEM_TASK_LIST, entity.getItem_list()));
            criteria.add(Restrictions.eq(ITEM_STATUS, 0l));
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, entity.getHolder_id()));
            criteria.add(Restrictions.le(ITEM_TASK_LIST_ORDER, entity.getItem_list_order()));
            List<ItemsEntity> list = (ArrayList<ItemsEntity>) criteria.list();
            return list.size() - 1;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getByDriverId(Long holderId, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, holderId));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getByDriverIdByTime(Long holderId, Long startTime, Long endTime, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, holderId));
            criteria.add(Restrictions.between(ITEM_APPROVE_TIME, startTime, endTime));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ItemsEntity> getInProgressByDriverId(Long holderId, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, holderId));
            criteria.add(Restrictions.eq(ITEM_STATUS, 0l));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ItemsEntity set(ItemsEntity entity) {
        try {
            persist(entity);
            return entity;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteById(Long id, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, id));
            List<ItemsEntity> entityList = (List<ItemsEntity>) criteria.list();
            for (ItemsEntity entity : entityList) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteByList(Long list, Long holderId, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ITEM_TASK_LIST, list));
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, holderId));
            criteria.add(Restrictions.eq(ITEM_STATUS, 0l));
            List<ItemsEntity> entityList = (List<ItemsEntity>) criteria.list();
            for (ItemsEntity entity : entityList) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<ItemsEntity> getUncommittedItems(String username) {
        try {
            final Criteria criteria;
            criteria = getCriteria();
            criteria.add(Restrictions.eq(ITEM_HOLDER_ID, UNCOMMITTED_HOLDER_ID));
            criteria.add(Restrictions.eq(USERNAME, username));
            return criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight) {
        Writer writer = new Code128Writer();
        try {
            BitMatrix bitMatrix = writer.encode(itemId, BarcodeFormat.CODE_128, barcodeWidth, barcodeHeight);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "png", out);
            return Base64.encodeBytes(out.toByteArray());
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getItemsNumber() {

        try {
            final Criteria criteria;
            criteria = getCriteria();
            return criteria.list().size();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int getMaxItemId() {
        String queryString = "SELECT max(" + ID + ") from " + ITEMS_TABLE;
        List result = getSessionFactory().getCurrentSession().createSQLQuery(queryString).list();
        if (result != null && result.size() > 0 && result.get(0) != null) return (Integer) result.get(0);
        else return 0;
    }
}