package com.bluetill.gpstrack.server.utils;

import com.bluetill.gpstrack.domain.UserEntity;
import com.bluetill.gpstrack.shared.dto.UserDTO;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/11/14
 * Time: 07:05

 */
public class UserEntityUtils {

    public static UserDTO EntityToDto(UserEntity entity) {
        return (entity != null) ? new UserDTO(entity.getUsername(), entity.getPassword(), entity.isEnabled(), entity.getFull_name()) : null;
    }

    public static UserEntity DtoToEntity(UserDTO dto){
        return (dto != null) ? new UserEntity(dto.getUsername(), dto.getPassword(), dto.isEnabled(), dto.getFull_name()) : null;
    }
}
