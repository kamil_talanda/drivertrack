package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.DriverEntity;
import com.bluetill.gpstrack.domain.DriverEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.DriverEntityUtils;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 08:27
 */

@Component
public class DriverServiceLogic {


    @Autowired
    private DriverEntityDao driverEntityDao;

    @Transactional
    public ArrayList<DriverDTO> getAll() {
        return getAll(BaseEntityUtils.getUsername());
    }

    @Transactional
    public ArrayList<DriverDTO> getAll(String username) {
        ArrayList<DriverDTO> dtoArrayList = new ArrayList();
        for (DriverEntity entity : driverEntityDao.getAll(username)) {
            dtoArrayList.add(DriverEntityUtils.EntityToDto(entity));
        }
        return dtoArrayList;
    }

    @Transactional(readOnly = true)
    public DriverDTO get(Long id) {
        return get(id, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public DriverDTO get(Long id, String username) {
        return DriverEntityUtils.EntityToDto(driverEntityDao.get(id, username));
    }

    @Transactional(readOnly = true)
    public DriverDTO getByName(String name) {
        return getByName(name, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public DriverDTO getByName(String name, String username) {
        return DriverEntityUtils.EntityToDto(driverEntityDao.getByName(name, username));
    }

    @Transactional
    public DriverDTO getByBmId(Long bm_id) {
        return getByBmId(bm_id, BaseEntityUtils.getUsername());
    }

    @Transactional
    public DriverDTO getByBmId(Long bm_id, String username) {
        return DriverEntityUtils.EntityToDto(driverEntityDao.getByBmId(bm_id, username));
    }

    @Transactional
    public boolean checkAndroidId(String driverName, String androidId, String username) {
        DriverEntity driverEntity = new DriverEntity();
        driverEntity.setName(driverName);
        driverEntity.setAndroid_id(androidId);
        return driverEntityDao.checkAndroidIdByName(driverEntity, username);
    }

    @Transactional
    public boolean checkAndroidId(Long driverId, String androidId, String username) {
        DriverEntity driverEntity = new DriverEntity();
        driverEntity.setId(driverId);
        driverEntity.setAndroid_id(androidId);
        return driverEntityDao.checkAndroidId(driverEntity, username);
    }

    @Transactional
    public boolean isUserEmpty(String name) {
        return isUserEmpty(name, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean isUserEmpty(String name, String username) {
        DriverEntity driverEntity = new DriverEntity();
        driverEntity.setName(name);
        return driverEntityDao.isUserEmpty(driverEntity, username);
    }

    @Transactional
    public DriverDTO set(DriverDTO dto) {
        return set(dto, BaseEntityUtils.getUsername());
    }

    @Transactional
    public DriverDTO set(DriverDTO driverDTO, String username) {
        if (driverDTO.getBm_id() == null) {
            driverDTO.setBm_id(driverEntityDao.getMaxBmId(username) + 1);
        }
        return DriverEntityUtils.EntityToDto(driverEntityDao.set(DriverEntityUtils.DtoToEntity(driverDTO, username)));
    }

    @Transactional
    public boolean deleteByName(String name) {
        return driverEntityDao.deleteByName(name, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean deleteByName(String name, String username) {
        return driverEntityDao.deleteByName(name, username);
    }

    @Transactional
    public boolean delete(DriverDTO dto, String username) {
        return driverEntityDao.delete(DriverEntityUtils.DtoToEntity(dto, username), username);
    }


}