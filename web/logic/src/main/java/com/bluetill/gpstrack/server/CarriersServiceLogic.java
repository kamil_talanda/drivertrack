package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.CarriersEntity;
import com.bluetill.gpstrack.domain.CarriersEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.CarriersEntityUtils;
import com.bluetill.gpstrack.shared.dto.CarriersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 08:27

 */

@Component
public class CarriersServiceLogic {

    @Autowired
    private CarriersEntityDao carriersEntityDao;

    @Transactional
    public ArrayList<CarriersDTO> getAll() {
        return getAll(BaseEntityUtils.getUsername());
    }

    @Transactional
    public ArrayList<CarriersDTO> getAll(String username) {
        ArrayList<CarriersDTO> dtoArrayList = new ArrayList();
        for (CarriersEntity entity : carriersEntityDao.getAll(username)) {
            dtoArrayList.add(CarriersEntityUtils.EntityToDto(entity));
        }
        return dtoArrayList;
    }

    @Transactional
    public CarriersDTO set(CarriersDTO dto) {
        return set(dto, BaseEntityUtils.getUsername());
    }

    @Transactional
    public CarriersDTO set(CarriersDTO dto, String username) {
        return CarriersEntityUtils.EntityToDto(carriersEntityDao.set(CarriersEntityUtils.DtoToEntity(dto, username)));
    }

    @Transactional
    public boolean deleteByName(String name) {
        return deleteByName(name, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean deleteByName(String name, String username) {
        return carriersEntityDao.deleteByName(name, username);
    }
}