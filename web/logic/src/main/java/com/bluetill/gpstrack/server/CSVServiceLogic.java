package com.bluetill.gpstrack.server;

import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/02/14
 * Time: 07:14

 */

@Component
public class CSVServiceLogic extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String data = request.getParameter("data");
        data = data.replace("_START_NEW_LINE_ITEM_", "\n");

        response.setHeader("Content-Type", "text/csv");
        response.setHeader("Content-Length", String.valueOf(data.length()));
        response.setHeader("Content-disposition", "attachment;filename=\"itemList.csv\"");

        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
        bos.write(data.getBytes());

        bos.flush();
        bos.close();
    }
}
