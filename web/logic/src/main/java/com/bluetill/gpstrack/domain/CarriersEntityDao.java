package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.CarriersVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CarriersEntityDao extends BaseDao<Integer, CarriersEntity> implements ICarriersEntityDao, CarriersVars {

    @Override
    public ArrayList<CarriersEntity> getAll(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return (ArrayList<CarriersEntity>) criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public CarriersEntity get(Long id, String username) {
        try {
            final Criteria criteria;
            criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, id));
            for (CarriersEntity entity : (List<CarriersEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public CarriersEntity set(CarriersEntity carriersEntity) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, carriersEntity.getUsername()));
            criteria.add(Restrictions.eq(CARRIERS_NAME, carriersEntity.getName()));
            if (criteria.list().size() > 0) return null;
            persist(carriersEntity);
            for (CarriersEntity entity : (List<CarriersEntity>) criteria.list()) {
                return entity;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteByName(String name, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(CARRIERS_NAME, name));
            List<CarriersEntity> carrierEntities = (List<CarriersEntity>) criteria.list();
            for (CarriersEntity entity : carrierEntities) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }
}
