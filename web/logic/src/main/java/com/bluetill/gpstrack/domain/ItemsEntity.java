package com.bluetill.gpstrack.domain;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import javax.persistence.*;


import static com.bluetill.gpstrack.ItemsEntityVars.*;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 02/12/13
 * Time: 15:27

 */
@Table(name = ITEMS_TABLE, schema = "public")
@Entity
public class ItemsEntity {
    private Long id;
    private String barcode;
    private String carrier;
    private String tenant;
    private Long qty;
    private Long holder_id;
    private String name;
    private String destination;
    private Long add_time;
    private Long approve_time;
    private Geometry approve_point;
    private String signature_image;
    private Long status;
    private String description;
    private Long delivery_time_planned;
    private Long item_list;
    private Long item_list_order;
    private Long bm_no;
    private String username;


    public ItemsEntity() {
    }

    public ItemsEntity(Long id, String barcode, String carrier, String tenant, Long qty, Long holder_id, String name, String destination, Long add_time, Long approve_time, String approve_pointWKT, String signature_image, Long status, String description, Long delivery_time_planned, Long item_list, Long item_list_order, Long bm_no, String username) {
        this.id = id;
        this.barcode = barcode;
        this.carrier = carrier;
        this.tenant = tenant;
        this.qty = qty;
        this.holder_id = holder_id;
        this.name = name;
        this.destination = destination;
        this.add_time = add_time;
        this.approve_time = approve_time;
        this.setApprove_pointWKT(approve_pointWKT);
        this.signature_image = signature_image;
        this.status = status;
        this.description = description;
        this.delivery_time_planned = delivery_time_planned;
        this.item_list = item_list;
        this.item_list_order = item_list_order;
        this.bm_no = bm_no;
        this.username = username;
    }

    @Column(name = ID)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = ITEM_BARCODE)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Column(name = ITEM_CARRIER)
    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    @Column(name = ITEM_TENANT)
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    @Column(name = ITEM_QTY)
    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    @Column(name = ITEM_HOLDER_ID)
    public Long getHolder_id() {
        return holder_id;
    }

    public void setHolder_id(Long holder_id) {
        this.holder_id = holder_id;
    }

    @Column(name = ITEM_NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = ITEM_DESTINATION)
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Column(name = ITEM_ADD_TIME)
    public Long getAdd_time() {
        return add_time;
    }

    public void setAdd_time(Long add_time) {
        this.add_time = add_time;
    }

    @Column(name = ITEM_APPROVE_TIME)
    public Long getApprove_time() {
        return approve_time;
    }

    public void setApprove_time(Long approve_time) {
        this.approve_time = approve_time;
    }

    @Column(name = ITEM_DELIVERY_TIME_PLANNED)
    public Long getDelivery_time_planned() {
        return delivery_time_planned;
    }

    public void setDelivery_time_planned(Long delivery_time_planned) {
        this.delivery_time_planned = delivery_time_planned;
    }

    @LazyCollection(LazyCollectionOption.FALSE)
    @Column(columnDefinition = "geometry", name = ITEM_APPROVE_POINT)
    @Type(type = "org.hibernate.spatial.GeometryType")
    public Geometry getApprove_point() {
        return approve_point;
    }

    public void setApprove_point(Geometry approve_point) {
        this.approve_point = approve_point;
    }

    /**
     * Gets entity geometry as WKT.
     *
     * @return WKT representation for geometry
     */
    @Transient
    public String getApprove_pointWKT() {
        if (this.approve_point == null) {
            return null;
        }
        WKTWriter writer = new WKTWriter();
        return writer.write(this.approve_point);
    }

    /**
     * Sets geometry using WKT representation.
     *
     * @param geometryWKT WKT representation for geometry
     * @throws com.vividsolutions.jts.io.ParseException
     *
     */
    public void setApprove_pointWKT(String geometryWKT) {
        if (geometryWKT == null) {
            return;
        }
        WKTReader reader = new WKTReader(new GeometryFactory(new PrecisionModel()));
        try {
            this.approve_point = reader.read(geometryWKT);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Column(name = ITEM_SIGNATURE_IMAGE)
    public String getSignature_image() {
        return signature_image;
    }

    public void setSignature_image(String signature_image) {
        this.signature_image = signature_image;
    }

    @Column(name = ITEM_STATUS)
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Column(name = ITEM_DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = ITEM_TASK_LIST)
    public Long getItem_list() {
        return item_list;
    }

    public void setItem_list(Long item_list) {
        this.item_list = item_list;
    }

    @Column(name = ITEM_TASK_LIST_ORDER)
    public Long getItem_list_order() {
        return item_list_order;
    }

    public void setItem_list_order(Long item_list_order) {
        this.item_list_order = item_list_order;
    }

    @Column(name = ITEM_BM_NO)
    public Long getBm_no() {
        return bm_no;
    }

    public void setBm_no(Long bm_no) {
        this.bm_no = bm_no;
    }

    @Column(name = USERNAME)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
