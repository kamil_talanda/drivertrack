package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.DriverVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DriverEntityDao extends BaseDao<Integer, DriverEntity> implements IDriversEntityDao, DriverVars {

    @Override
    public ArrayList<DriverEntity> getAll(String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            return (ArrayList<DriverEntity>) criteria.list();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DriverEntity get(Long id, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, id));
            return (DriverEntity) criteria.uniqueResult();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DriverEntity getByName(String driverName, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(DRIVER_NAME, driverName));
            return (DriverEntity) criteria.uniqueResult();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DriverEntity getByBmId(Long bm_id, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(BM_ID, bm_id));
            for (DriverEntity result : (ArrayList<DriverEntity>) criteria.list()) {
                return result;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public DriverEntity set(DriverEntity driverEntity) {
        try {
            persist(driverEntity);
            return driverEntity;

        } catch (DaoException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(DriverEntity driverEntity, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(DRIVER_NAME, driverEntity.getName()));
            List<DriverEntity> driverEntities = (List<DriverEntity>) criteria.list();
            for (DriverEntity entity : driverEntities) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteByName(String driverName, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(DRIVER_NAME, driverName));
            List<DriverEntity> driverEntities = (List<DriverEntity>) criteria.list();
            for (DriverEntity entity : driverEntities) {
                remove(entity);
            }
            return true;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkAndroidIdByName(DriverEntity driverEntity, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(DRIVER_NAME, driverEntity.getName()));
            criteria.add(Restrictions.eq(ANDROID_ID, driverEntity.getAndroid_id()));
            List<DriverEntity> d = (List<DriverEntity>) criteria.list();
            if (d.size() > 0) {
                return true;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkAndroidId(DriverEntity driverEntity, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(ID, driverEntity.getId()));
            criteria.add(Restrictions.eq(ANDROID_ID, driverEntity.getAndroid_id()));
            List<DriverEntity> d = (List<DriverEntity>) criteria.list();
            if (d.size() > 0) {
                return true;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isUserEmpty(DriverEntity driverEntity, String username) {
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            criteria.add(Restrictions.eq(DRIVER_NAME, driverEntity.getName()));
            List<DriverEntity> driverEntities = (List<DriverEntity>) criteria.list();
            for (DriverEntity entity : driverEntities) {
                if (entity.getAndroid_id() == null) return true;
                else return false;
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Long getMaxBmId(String username){
        try {
            final Criteria criteria = getCriteria();
            criteria.add(Restrictions.eq(USERNAME, username));
            Long result = 0l;
            for (DriverEntity entity : (List<DriverEntity>) criteria.list()) {
                if (result <= entity.getBm_id()) result = entity.getBm_id();
            }
            return result;
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return 0l;
    }

}
