package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.shared.LineArray;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 09/01/14
 * Time: 13:33

 */
public interface IDriversPathsEntityDao {

    String getDriverLastPosition(long driverId);

    boolean saveLine(Long driverId, String line, String username);

    ArrayList<LineArray> getPointList(Long driverId, Long day, String username);
}
