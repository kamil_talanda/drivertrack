package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.DriversPathVars;
import com.bluetill.gpstrack.shared.Directions;
import com.bluetill.gpstrack.shared.LineArray;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 16:23
 */

@Repository
public class DriversPathEntityDao extends BaseDao<Integer, DriversPathEntity> implements IDriversPathsEntityDao, DriversPathVars {

    @Override
    public ArrayList<LineArray> getPointList(Long driverId, Long day, String username) {

        Session session = getSessionFactory().getCurrentSession();
        String sql = null;

        Long today = (day / DAY_MILLISECONDS) * DAY_MILLISECONDS;
        Long tomorrow = today + DAY_MILLISECONDS;

        sql = "select driver_id, st_astext(path), time from drivers_path where username ='" + username + "' and driver_id=" + driverId + " and time > " + today + " and time < " + tomorrow + " order by time";

        SQLQuery query = session.createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        ArrayList<Path> driversPathList = new ArrayList<>();

        for (Object curDriverPathTmp : query.list()) {
            HashMap curDriverPath = (HashMap) curDriverPathTmp;
            Long time = ((BigInteger) curDriverPath.get("time")).longValue();
            Matcher matcher = (Pattern.compile("\\((.*?)\\)")).matcher((String) curDriverPath.get("st_astext"));
            while (matcher.find()) {
                ArrayList<Directions> directions = new ArrayList<Directions>();
                for (String curPosition : matcher.group(1).split(",")) {
                    String[] r = curPosition.split(" ");
                    directions.add(new Directions(Double.parseDouble(r[0]), Double.parseDouble(r[1])));
                }
                driversPathList.add(new Path(driverId, directions, time));
            }
        }

        ArrayList<LineArray> pointList = new ArrayList<>();
        for (Path path : driversPathList) {
            pointList.add(new LineArray(path.getTime(), path.getPath()));
        }
        return pointList;
    }

    @Override
    public boolean saveLine(Long driverId, String line, String username) {
        try {
            Session session = getSessionFactory().getCurrentSession();
            String sql = new String();

            Long time = new Date().getTime();

            sql = "select * from drivers where id=" + driverId + ";";
            SQLQuery query = session.createSQLQuery(sql);
            if (query.list().size() != 0) {
                sql = "insert into drivers_path (driver_id, time, path, username) values (" + driverId + ", " + time + ", '" + line + "', '" + username + "' );";
                query = session.createSQLQuery(sql);
                query.executeUpdate();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getDriverLastPosition(long driverId) {
        Session session = getSessionFactory().getCurrentSession();
        String sql = "select driver_id, st_astext(path), time from drivers_path where driver_id=" + driverId + " order by time";

        SQLQuery query = session.createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

        List itemList = query.list();
        if (itemList.size() > 0) {
            HashMap curDriverPath = (HashMap) itemList.get(itemList.size() - 1);
            Timestamp time = (Timestamp) curDriverPath.get("time");
            String wktDriverLastPosition = (String) curDriverPath.get("st_astext");
            return wktDriverLastPosition;
        }
        return null;
    }

    public class Path {
        public Long driverId;
        public ArrayList<Directions> path;
        public Long time;

        public Path(Long driverId, ArrayList<Directions> path, Long time) {
            this.time = time;
            this.driverId = driverId;
            this.path = path;
        }

        public ArrayList<Directions> getPath() {
            return path;
        }

        public void setPath(ArrayList<Directions> path) {
            this.path = path;
        }

        public Long getDriverId() {
            return driverId;
        }

        public void setDriverId(Long driverId) {
            this.driverId = driverId;
        }

        public Long getTime() {
            return time;
        }

        public void setTime(Long time) {
            this.time = time;
        }
    }
}