package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.UserEntity;
import com.bluetill.gpstrack.domain.UserEntityDao;
import com.bluetill.gpstrack.domain.UserRolesEntityDao;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.UserEntityUtils;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:37

 */
@Component
public class UserServiceLogic {

    @Autowired
    private UserEntityDao userEntityDao;
    @Autowired
    private UserRolesEntityDao userRolesEntityDao;

    @Transactional
    public ArrayList<UserDTO> getAll() {
        ArrayList<UserDTO> dtoArrayList = new ArrayList();
        for (UserEntity entity : userEntityDao.getAll()) {
            dtoArrayList.add(UserEntityUtils.EntityToDto(entity));
        }
        return dtoArrayList;
    }

    @Transactional
    public UserDTO getByName(String username) {
        return UserEntityUtils.EntityToDto(userEntityDao.getByName(username));
    }

    @Transactional
    public UserDTO getLoggedUserDetails() {
        return getByName(BaseEntityUtils.getUsername());
    }

    @Transactional
    public UserDTO set(UserDTO userDTO) {
        UserDTO result = UserEntityUtils.EntityToDto(userEntityDao.set(UserEntityUtils.DtoToEntity(userDTO)));
        userRolesEntityDao.setUserRole_user(userDTO.getUsername());
        return result;
    }

    @Transactional
    public boolean deleteByName(String username) {
        return userEntityDao.deleteByName(username);
    }

    @Transactional
    public boolean checkPassword(String username, String password) {
        if (userEntityDao.getByName(username) != null && password.equals(userEntityDao.getByName(username).getPassword()))
            return true;
        else return false;
    }
}
