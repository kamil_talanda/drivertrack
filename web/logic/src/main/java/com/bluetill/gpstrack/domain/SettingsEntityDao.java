package com.bluetill.gpstrack.domain;

import com.bluetill.gpstrack.SettingsVars;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:22
 */
@Repository("settingsDao")
public class SettingsEntityDao extends BaseDao<Integer, SettingsEntity> implements ISettingsEntityDao, SettingsVars {

    @Override
    public SettingsEntity set(SettingsEntity entity) throws DaoException {
        final Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(USERNAME, entity.getUsername()));
        criteria.add(Restrictions.eq(SETTINGS_NAME_COLUMN, entity.getName()));
        for(SettingsEntity settingsEntity :(List<SettingsEntity>)criteria.list()){
            settingsEntity.setValue(entity.getValue());
            persist(settingsEntity);
            return settingsEntity;
        }
        persist(entity);
        return entity;
    }

    @Override
    public SettingsEntity getByName(String name, String username) throws DaoException {
        final Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(USERNAME, username));
        criteria.add(Restrictions.eq(SETTINGS_NAME_COLUMN, name));
        for(SettingsEntity settingsEntity :(List<SettingsEntity>)criteria.list()){
            return settingsEntity;
        }
        return null;
    }
}
