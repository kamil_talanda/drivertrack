package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.domain.*;
import com.bluetill.gpstrack.domain.exceptions.DaoException;
import com.bluetill.gpstrack.server.utils.BaseEntityUtils;
import com.bluetill.gpstrack.server.utils.EmailUtils;
import com.bluetill.gpstrack.server.utils.ItemEntityUtils;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
public class ItemsServiceLogic {

    private static final Long STATUS_DONE = (long) 1;
    @Autowired
    private IItemsEntityDao itemsDao;
    @Autowired
    private IDriversEntityDao driversDao;
    @Autowired
    private ISettingsEntityDao settingsDao;

    @Transactional
    public ItemDTO get(Long itemId) {
        return get(itemId, BaseEntityUtils.getUsername());
    }

    @Transactional
    public ItemDTO get(Long itemId, String username) {
        ItemsEntity entity = itemsDao.get(itemId, username);
        return ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username));
    }

    @Transactional
    public List<ItemDTO> getByBarcode(String barcode) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getByBarcode(barcode)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), "")));
        }
        return dtoArrayList;
    }

    @Transactional
    public ItemDTO setFromUploadFIle(ItemDTO dto) {
        return ItemEntityUtils.EntityToDto(itemsDao.setFromUploadFIle(ItemEntityUtils.DtoToEntity(dto, BaseEntityUtils.getUsername())), dto.getHolderName());
    }

    @Transactional
    public List<ItemDTO> getByBarcode(String barcode, String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getByBarcode(barcode, username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional
    public List<ItemDTO> getByDriverId(Long driverId) {
        return getByDriverId(driverId, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getByDriverId(Long driverId, String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getByDriverId(driverId, username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getByDriverIdByTime(Long driverId, Long startTime, Long endTime, String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getByDriverIdByTime(driverId, startTime, endTime, username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional
    public List<ItemDTO> getByDriverIdToday(Long driverId, String username) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Australia/Sydney"));
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Long todayStart = calendar.getTime().getTime();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
        Long todayEnd = calendar.getTime().getTime();
        return getByDriverIdByTime(driverId, todayStart, todayEnd, username);

    }

    @Transactional
    public List<ItemDTO> getInProgressByDriverId(Long driverId) {
        return getInProgressByDriverId(driverId, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getInProgressByDriverId(Long driverId, String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getInProgressByDriverId(driverId, username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getUncommittedItems() {
        return getUncommittedItems(BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getUncommittedItems(String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getUncommittedItems(username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getAll() {
        return getAll(BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getAll(String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getAll(username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getAll(Date startFilterDate, Date endFilterDate) {
        return getAll(startFilterDate, endFilterDate, BaseEntityUtils.getUsername());
    }

    @Transactional(readOnly = true)
    public List<ItemDTO> getAll(Date startFilterDate, Date endFilterDate, String username) {
        ArrayList<ItemDTO> dtoArrayList = new ArrayList();
        for (ItemsEntity entity : itemsDao.getAll(startFilterDate, endFilterDate, username)) {
            dtoArrayList.add(ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username)));
        }
        return dtoArrayList;
    }

    @Transactional
    public ItemDTO set(ItemDTO dto) {
        return set(dto, BaseEntityUtils.getUsername());
    }

    @Transactional
    public ItemDTO set(ItemDTO dto, String username) {
        ItemsEntity entity = itemsDao.set(ItemEntityUtils.DtoToEntity(dto, username));
        return ItemEntityUtils.EntityToDto(entity, getHolderName(entity.getHolder_id(), username));
    }

    @Transactional
    public boolean deleteById(Long id) {
        return deleteById(id, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean deleteById(Long id, String username) {
        return itemsDao.deleteById(id, username);
    }

    @Transactional
    public boolean deleteByList(Long list, Long holderId) {
        return deleteByList(list, holderId, BaseEntityUtils.getUsername());
    }

    @Transactional
    public boolean deleteByList(Long list, Long holderId, String username) {
        return itemsDao.deleteByList(list, holderId, username);
    }

    @Transactional
    public String getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight) {
        return itemsDao.getBarcodeImage(itemId, barcodeWidth, barcodeHeight);
    }

    @Transactional
    public Integer getNumberOfOpenBefore(ItemDTO dto) {
        return itemsDao.getNumberOfOpenBefore(ItemEntityUtils.DtoToEntity(dto, BaseEntityUtils.getUsername()), BaseEntityUtils.getUsername());
    }

    @Transactional
    public void approveByBarcode(String barcode, Long driverId, String signatureImage, String approvePoint) {
        approveByBarcode(barcode, driverId, signatureImage, approvePoint, BaseEntityUtils.getUsername());
    }

    @Transactional
    public void approveByBarcode(String barcode, Long driverId, String signatureImage, String approvePoint, String username) {
        for (ItemsEntity itemEntity : itemsDao.getByBarcode(barcode, username)) {
            itemEntity.setHolder_id(driverId);
            itemEntity.setSignature_image(signatureImage);
            itemEntity.setStatus(STATUS_DONE);
            itemEntity.setApprove_time(new Date().getTime());
            itemEntity.setApprove_pointWKT(approvePoint);
            itemsDao.set(itemEntity);
        }
    }

    @Transactional
    public void approve(Long itemId, Long driverId, String signatureImage, String approvePoint) {
        approve(itemId, driverId, signatureImage, approvePoint, BaseEntityUtils.getUsername());
    }

    @Transactional
    public void approve(Long itemId, Long driverId, String signatureImage, String approvePoint, String username) {
        final ItemsEntity itemEntity = itemsDao.get(itemId, username);
        itemEntity.setHolder_id(driverId);
        itemEntity.setSignature_image(signatureImage);
        itemEntity.setStatus(STATUS_DONE);
        itemEntity.setApprove_time(new Date().getTime());
        itemEntity.setApprove_pointWKT(approvePoint);

        List<ItemDTO> todayList = getByDriverIdToday(driverId, username);
        if (todayList != null && todayList.size() > 0) {
            itemEntity.setItem_list(todayList.get(0).getItemList());
        }
        itemsDao.set(itemEntity);
    }

    @Transactional
    public void setHolderByBarcode(String barcode, Long driverId) {
        setHolderByBarcode(barcode, driverId, BaseEntityUtils.getUsername());
    }

    @Transactional
    public void setHolderByBarcode(String barcode, Long driverId, String username) {
        for (ItemsEntity itemEntity : itemsDao.getByBarcode(barcode, username)) {
            itemEntity.setHolder_id(driverId);
            itemsDao.set(itemEntity);
        }
    }

    @Transactional
    public int getItemsNumber() {
        return itemsDao.getItemsNumber();
    }

    @Transactional
    public int getMaxItemId() {
        return itemsDao.getMaxItemId();
    }

    @Transactional
    public String getApprovePointDirections(String wktGeometry) {
        return wktGeometry;
    }

    @Transactional
    public boolean sendEmail(ItemDTO item, String toAddress) {
        try {
            return (new EmailUtils()).sendEmail(item, toAddress, settingsDao.getByName(SettingsDTO.SETTINGS_EMAIL_FROM, BaseEntityUtils.getUsername()).getValue(), settingsDao.getByName(SettingsDTO.SETTINGS_EMAIL_SIGNATURE, BaseEntityUtils.getUsername()).getValue());
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Transactional
    public boolean sendEmail(ItemDTO item, String toAddress, String username) {

        try {
            return (new EmailUtils()).sendEmail(item, toAddress, settingsDao.getByName(SettingsDTO.SETTINGS_EMAIL_FROM, username).getValue(), settingsDao.getByName(SettingsDTO.SETTINGS_EMAIL_SIGNATURE, username).getValue());
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getHolderName(Long holderId, String username) {
        String holderName = "No Driver";
        DriverEntity driverEntity = driversDao.get(holderId, username);
        if (driverEntity != null) holderName = driverEntity.getName();
        return holderName;
    }
}

