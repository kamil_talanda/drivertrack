<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta name="gwt:property" content="locale=en_EN">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>driverTrack</title>
    <script type="text/javascript" src="scripts/proj4js-combined.js"></script>
    <script type="text/javascript" src="scripts/openlayers/OpenLayers.js"></script>
    <script src="http://maps.google.com/maps/api/js?v=3&amp;sensor=false"></script>
    <script type="text/javascript" src="Main/Main.nocache.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        Proj4js.defs['EPSG:2180'] = '+proj=tmerc +lat_0=0 +lon_0=19 +k=0.9993 +x_0=500000 +y_0=-5300000 +ellps=GRS80 +units=m +no_defs';
        Proj4js.defs['EPSG:4326'] = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
        Proj4js.defs['EPSG:900913'] = '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs';
        //]]>
    </script>
</head>
<body>
    <iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1'
            style="position:absolute;width:0;height:0;border:0"></iframe>
    <!-- IFrame below is needed to make AJAX file uploads -->
    <iframe id="uploadFrame" name="_uploadFrame" style="position:absolute;width:0;height:0;border:0"></iframe>
    <noscript>
        <div style="width: 22em; position: absolute; left: 50%; margin-left: -11em; color: red; background-color: white; border: 1px solid red; padding: 4px; font-family: sans-serif">
            Your web browser must have JavaScript enabled
            in order for this application to display correctly.
        </div>
    </noscript>

    <%
        String prefix = "Main/sc/modules/";
        String[] modules = {
                "ISC_Core.js",
                "ISC_Foundation.js",
                "ISC_Containers.js",
                "ISC_Grids.js",
                "ISC_Forms.js",
                "ISC_Calendar.js",
                "ISC_DataBinding.js"
        };

        for (int i = 0; i < modules.length; ++i) {
            double percent = ((double) i) / modules.length;
            out.println("<script type=\"text/javascript\">"
                    + "document.getElementById('loadingText').innerHTML = "
                    + "'Załadowano: " + (int) (percent * 100) + "%'"
                    + "</script>"
            );
            out.println("<script type=\"text/javascript\" src=\""
                    + prefix
                    + modules[i]
                    + "\" defer></script>"
            );
        }


        String skin = "drivertrackTheme";

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; ++i) {
                Cookie cookie = cookies[i];
                if ("skin".equals(cookie.getName())) {
                    skin = cookie.getValue();
                }
            }
        }

        out.println("<script type=\"text/javascript\" src=\"style/" + skin + "/load_skin.js\"></script>");
    %>
</body>
</html>