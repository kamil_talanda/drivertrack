<html xmlns:c="http://www.w3.org/1999/XSL/Transform">
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<title>Track Your delivery</title>
<script>
    // online production
    //        var WS_URL = "http://drivertrackapp.com/driverTrack/rest/getItemLocation";
    // online test
    var WS_URL = "http://54.66.223.17:8080/rest/position_getItemLocation";
    // offline test
//        var WS_URL = "http://127.0.0.1:8888/rest/position_getItemLocation";


    var ANDROID_VERSION = "v1.4";
    var ANDROID_APP_LOCATION = "https://play.google.com/store/apps/details?id=com.bluetill.drivertrack.android";
    var ITEM_ID_WS = "barcode";
    var ADMIN_EMAIL = "ktalanda@bluetill.com";

    var NO_DATA = "No data to show";
    var INPUT_NAME_PLACEHOLDER = "Enter name";
    var INPUT_NAME_TITLE = "Username:";
    var INPUT_PASSWORD_PLACEHOLDER = "Enter password";
    var INPUT_PASSWORD_TITLE = "Password:";
    var INPUT_ITEM_ID_PLACEHOLDER = "Item ID";
    var ITEM_LOCATION_BUTTON_NAME = "Track Your Delivery";
    var FOOTER_TEXT = "driverTrack v1.4 by Bluetill corp. 2014";


    var NOTE_BOTTOM_TEXT = 'Contact: +61 411 389 336 or <a href="mailto:' + ADMIN_EMAIL + '">' + ADMIN_EMAIL + '</a>.';


    $(document).ready(function () {

        document.getElementById("itemItemId").placeholder = INPUT_ITEM_ID_PLACEHOLDER;
        document.getElementById("getLocationItemButton").innerHTML = ITEM_LOCATION_BUTTON_NAME;
        document.getElementById("footer").innerHTML = FOOTER_TEXT;

        $("#getLocationItemButton").click(function () {

            var itemId = document.getElementById("itemItemId").value;


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: WS_URL,
                data: "{" + ITEM_ID_WS + ": '" + itemId + "'}",
                complete: function (data, status) {
                    try {
                        var response = JSON.parse(data.responseText);
                        var parsedData = "";
                        parsedData += "<div class='dialog_div'><div class='dialog_title'>Barcode: </div><div class='dialog_content'>" + response.item.barcode + "</div></div>";
                        if (typeof  response.item.name !== 'undefined') parsedData += "<div class='dialog_div'><div class='dialog_title'>Name: </div><div class='dialog_content'>" + response.item.name + "</div></div>";
                        if (typeof  response.item.destination !== 'undefined') parsedData += "<div class='dialog_div'><div class='dialog_title'>Address: </div><div class='dialog_content'>" + response.item.destination + "</div></div>";
                        if (typeof  response.item.approveTime !== 'undefined') {
                            var date = new Date(response.item.approveTime);
                            var hour = date.getHours();
                            if (hour < 10) hour = "0" + hour;
                            var minute = date.getMinutes();
                            if (minute < 10) minute = "0" + minute;
                            var day = date.getDate();
                            if (day < 10) day = "0" + day;
                            var month = date.getMonth();
                            if (month < 10) month = "0" + month;
                            var year = date.getFullYear();

                            var deliveryTime = hour + ":" + minute + " - " + day + "/" + month + "/" + year;
                            parsedData += "<div class='dialog_div'><div class='dialog_title'>Estimated delivery time: </div><div class='dialog_content'>" + deliveryTime + "</div></div>";
                            parsedData += "<div class='dialog_div'><div class='dialog_title'>Deliveries before: </div><div class='dialog_content'>" + response.before + "</div></div>";
                        }


                        $("#dialog").html(parsedData);

                        $("#dialog").dialog({
                            title: "Item details",
                            width: 600,
                            height: 300,
                            modal: true,
                            buttons: {
                                "Show Current Position On Map": function () {
                                    try {
                                        var locationArray = response.position.split("(");
                                        if (locationArray.length > 1) var locationArray = locationArray[1].split(")");
                                        if (locationArray.length > 1) var locationArray = locationArray[0].split(",");
                                        if (locationArray.length > 1) var locationArray = locationArray[0].split(" ");

                                        if (locationArray.length == 2) {
                                            window.open("https://maps.google.com/maps?q=loc:" + locationArray[1] + ", " + locationArray[0]);
                                        } else {
                                            alert("No position available");
                                        }
                                    } catch (e) {
                                        alert("No position available");
                                    }
                                },
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    } catch (e) {
                        alert(data.responseText);
                    }


                }
            });

        });
    });
</script>
<style>
    * {
        margin: 0;
        padding: 0;
        font-family: Arial, Verdana, sans-serif;
    }

    input {
        width: 120px;
        height: 25px;
    }

    label {
        color: #1C61AE;
    }

    .button {
        padding: 5px 10px;
        display: inline;
        background: #777;
        border: none;
        color: #fff;
        cursor: pointer;
        font-weight: bold;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        text-shadow: 1px 1px #666;
    }

    .button.darkblue {
        background-color: #1C61AE;
    }

    .button.darkblue:hover {
        background-color: #0FA0C6;
        background-position: 0 -48px;
    }

    #drivertrack_logo {
        text-align: center;
        margin-top: 30px;
    }

    #error_msg {
        text-align: center;
        background-color: #9c192c;
        font-weight: bold;
        font-size: 20px;
        color: white;
        padding: 5px;
    }

    #footer {
        background-color: #154D74;
        border-top: 1px solid #000 !important;
        background-position: center bottom;
        background-repeat: repeat-x;
        height: 20px;
        margin: 0;
        text-align: center;
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 1000;
        font-size: 10px;
        color: #FFFFFF;
        padding-top: 4px;
    }

    .ui-dialog {
        z-index: 2000 !important;
    }

    #dialog {
        display: none;
        width: 100%;
        height: 100%;
    }

    .dialog_div, .details_div {
        width: 100%;
        display: table;
    }

    .dialog_title, .details_title {
        width: 45%;
        display: table-cell;
        font-size: 15px;
        text-align: right;
        font-size: 12px;
        font-weight: bold;
    }
    .dialog_title{
        width: 30%;
    }

    .dialog_content, .details_content {
        margin-left: 30px;;
    }

</style>
</head>
<body style="background: #F7FDFF;">

<div id="dialog" title="Delivery Info">
</div>
<div id="passwordLoginOption" class="form" style="padding-top: 10px;">

    <div id="drivertrack_logo">
        <img src="images/drivertrack_logo.png"/>
    </div>

    <div id="error_msg">
        Wrong item id
    </div>


    <div style="text-align: center; ">
        <input id="itemItemId"/>
        <button id="getLocationItemButton" class="button darkblue"></button>
    </div>
    <br>

    <div>
        <div class='details_div'>
            <div class='details_title'>Address:</div>
            <div class='details_content'>879 Old Northern Road</div>
        </div>
        <div class='details_div'>
            <div class='details_title'></div>
            <div class='details_content'>DURAL NSW 2158</div>
        </div>
        <div class='details_div'>
            <div class='details_title'></div>
            <div class='details_content'>AUSTRALIA</div>
        </div>
        <br>
        <div class='details_div'>
            <div class='details_title'>Telephone:</div>
            <div class='details_content'>02 9651 1010</div>
        </div>
    </div>


</div>
<script>
    document.getElementById("error_msg").style.visibility = "hidden";
    if (document.URL.indexOf("error") > -1) {
        document.getElementById("error_msg").style.visibility = "visible";
    }
</script>
<div id="footer">
</div>
</body>
</html>