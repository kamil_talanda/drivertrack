<html xmlns:c="http://www.w3.org/1999/XSL/Transform">
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>

<title>Login Page</title>
<script>
    // online production
    //        var WS_URL = "http://drivertrackapp.com/driverTrack/rest/getItemLocation";
    // online test
            var WS_URL = "http://54.66.204.45:8080/drivertrack/rest/position_getItemLocation";
    // offline test
//    var WS_URL = "http://127.0.0.1:8888/rest/position_getItemLocation";


    var ANDROID_VERSION = "v1.4";
    var ANDROID_APP_LOCATION = "https://play.google.com/store/apps/details?id=com.bluetill.drivertrack.android";
    var ITEM_ID_WS = "barcode";
    var ADMIN_EMAIL = "ktalanda@bluetill.com";

    var NO_DATA = "No data to show";
    var INPUT_NAME_PLACEHOLDER = "Enter name";
    var INPUT_NAME_TITLE = "Username:";
    var INPUT_PASSWORD_PLACEHOLDER = "Enter password";
    var INPUT_PASSWORD_TITLE = "Password:";
    var INPUT_ITEM_ID_PLACEHOLDER = "Item ID";
    var ITEM_LOCATION_BUTTON_NAME = "Track Your Delivery";
    var FOOTER_TEXT = "driverTrack v1.4 by Bluetill corp. 2014";


    var NOTE_BOTTOM_TEXT = 'Contact: +61 411 389 336 or <a href="mailto:' + ADMIN_EMAIL + '">' + ADMIN_EMAIL + '</a>.';


    $(document).ready(function () {
        document.getElementById("nameInput").placeholder = INPUT_NAME_PLACEHOLDER;
        document.getElementById("nameTitle").innerHTML = INPUT_NAME_TITLE;
        document.getElementById("passwordInput").placeholder = INPUT_PASSWORD_PLACEHOLDER;
        document.getElementById("passwordTitle").innerHTML = INPUT_PASSWORD_TITLE;
        document.getElementById("itemItemId").placeholder = INPUT_ITEM_ID_PLACEHOLDER;
        document.getElementById("getLocationItemButton").innerHTML = ITEM_LOCATION_BUTTON_NAME;
        document.getElementById("contact").innerHTML = NOTE_BOTTOM_TEXT;
        document.getElementById("footer").innerHTML = FOOTER_TEXT;


        $("#getLocationItemButton").click(function () {

            var itemId = document.getElementById("itemItemId").value;


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: WS_URL,
                data: "{" + ITEM_ID_WS + ": '" + itemId + "'}",
                complete: function (data, status) {
                    try {
                        var response = JSON.parse(data.responseText);
                        var parsedData = "";
                        parsedData += "<div class='dialog_div'><div class='dialog_title'>Barcode: </div><div class='dialog_content'>" + response.item.barcode + "</div></div>";
                        if (typeof  response.item.name !== 'undefined') parsedData += "<div class='dialog_div'><div class='dialog_title'>Name: </div><div class='dialog_content'>" + response.item.name + "</div></div>";
                        if (typeof  response.item.destination !== 'undefined') parsedData += "<div class='dialog_div'><div class='dialog_title'>Destination: </div><div class='dialog_content'>" + response.item.destination + "</div></div>";
                        if (typeof  response.item.approveTime !== 'undefined'){
                            var date = new Date(response.item.approveTime);
                            var hour = date.getHours();
                            if(hour < 10) hour = "0" + hour;
                            var minute = date.getMinutes();
                            if(minute < 10) minute = "0" + minute;
                            var day = date.getDate();
                            if(day < 10) day = "0" + day;
                            var month = date.getMonth();
                            if(month < 10) month = "0" + month;
                            var year = date.getFullYear();

                            var deliveryTime = hour + ":" + minute + " - " + day + "/" + month + "/" + year;
                            parsedData += "<div class='dialog_div'><div class='dialog_title'>Estimated delivery time: </div><div class='dialog_content'>" + deliveryTime + "</div></div>";
                            parsedData += "<div class='dialog_div'><div class='dialog_title'>Deliveries before: </div><div class='dialog_content'>" + response.before + "</div></div>";
                        }

                        $("#dialog").html(parsedData);

                        $("#dialog").dialog({
                            title: "Item details",
                            width: 600,
                            height: 300,
                            modal: true,
                            buttons: {
                                "Show Current Position On Map": function () {
                                    try {
                                        var locationArray = response.position.split("(");
                                        if (locationArray.length > 1) var locationArray = locationArray[1].split(")");
                                        if (locationArray.length > 1) var locationArray = locationArray[0].split(",");
                                        if (locationArray.length > 1) var locationArray = locationArray[0].split(" ");

                                        if (locationArray.length == 2) {
                                            window.open("https://maps.google.com/maps?q=loc:" + locationArray[1] + ", " + locationArray[0]);
                                        } else {
                                            alert("No position available");
                                        }
                                    } catch (e) {
                                        alert("No position available");
                                    }
                                },
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    } catch (e) {
                        alert("Wrong item id");
                    }


                }
            });

        });
    });
</script>
<style>
    * {
        margin: 0;
        padding: 0;
        font-family: Arial, Verdana, sans-serif;
    }

    input {
        width: 120px;
        height: 25px;
    }

    label {
        color: #1C61AE;
    }

    .button {
        padding: 5px 10px;
        display: inline;
        background: #777;
        border: none;
        color: #fff;
        cursor: pointer;
        font-weight: bold;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        text-shadow: 1px 1px #666;
    }

    .button.darkblue {
        background-color: #1C61AE;
    }

    .button.darkblue:hover {
        background-color: #0FA0C6;
        background-position: 0 -48px;
    }

    #drivertrack_logo {
        text-align: center;
        margin-top: 30px;
    }

    #error_msg {
        text-align: center;
        background-color: #9c192c;
        font-weight: bold;
        font-size: 20px;
        color: white;
        padding: 5px;
    }

    #bluetill_logo {
        background-color: #ffffff;
        width: 100%;
        margin-bottom: 5px;
    }

    #login {
        position: absolute;
        top: 28%;
        width: 100%;
        z-index: 1003;
    }

    #login2 {
        width: 350px;
        margin: 0 auto;
        border: 1px solid #1C61AE;
        text-align: center;

        background: #BEDCE2;
        font-family: Arial, Helvetica, sans-serif;
        height: 180px;
        /*background-image: url("images/keys_leaf.png");*/
        background-repeat: no-repeat;
        background-position: 20px 15px;
    }

    #table_login {
        margin-left: 10px;

    }

    #submit_button {
        width: 120px;
    }

    #footer {
        background-color: #154D74;
        border-top: 1px solid #000 !important;
        background-position: center bottom;
        background-repeat: repeat-x;
        height: 20px;
        margin: 0;
        text-align: center;
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 1000;
        font-size: 10px;
        color: #FFFFFF;
        padding-top: 4px;
    }

    .ui-dialog {
        z-index: 2000 !important;
    }

    #dialog {
        display: none;
        width: 100%;
        height: 100%;
    }

    .dialog_div {
        width: 100%;
        display: table;
    }

    .dialog_title {
        width: 200px;
        display: table-cell;
        font-size: 15px;
        text-align: right;
    }

    .dialog_content {
        margin-left: 30px;;
    }


</style>
</head>
<body style="background: #F7FDFF;" onload="forceChangeToHttps(); checkLoginError(); initializeService(); ">

<div id="dialog" title="Basic dialog">
    <p>asddasds</p>
</div>


<div id="passwordLoginOption" class="form" style="padding-top: 10px;">

    <div id="drivertrack_logo">
        <img src="images/drivertrack_logo.png"/>
    </div>

    <div id="error_msg">
        Wrong username or password
    </div>

    <div id="login">
        <div id="login2">
            <form name="loginForm" method="post" action="j_spring_security_check">
                <table id="table_login">
                    <tr>
                        <div id="bluetill_logo">
                            <img style="height:35px; margin-top: 2px;" src="images/bluetill_logo.jpg"/>
                        </div>
                    </tr>
                    <tr>
                        <td>
                            <img style="width: 90px" src="images/drivertrack_icon.png"/>
                        </td>
                        <td style="padding-left:25px;">
                            <table>

                                <tr>
                                    <td style="text-align: right;">
                                        <label id="nameTitle" for="j_username"></label>
                                    </td>
                                    <td>
                                        <input id="nameInput" type="text" name="j_username"/>
                                    </td>
                                <tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <label id="passwordTitle" for="j_password"></label>
                                    </td>
                                    <td>
                                        <input id="passwordInput" type="password" name="j_password"/>
                                    </td>
                                <tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <input type="submit" name="loginButton" value="Login" class="button darkblue" id="submit_button"/>
            </form>
        </div>
        </br>


        <div style="text-align: center; ">
            <input id="itemItemId"/>
            <button id="getLocationItemButton" class="button darkblue"></button>
        </div>
        </br>
        <div style="text-align: center; ">
            <a href="https://play.google.com/store/apps/details?id=com.bluetill.drivertrack">
                <img alt="Android app on Google Play"
                     src="https://developer.android.com/images/brand/en_app_rgb_wo_60.png"/>
            </a>
        </div>
        <p style="font-size: 10; color: #777; margin-top: 5px; text-align: center" id="contact"></p>
    </div>


</div>
<script>
    document.getElementById("error_msg").style.visibility = "hidden";
    if (document.URL.indexOf("error") > -1) {
        document.getElementById("error_msg").style.visibility = "visible";
    }
</script>


<div id="footer">

</div>
</body>
</html>