package com.bluetill.gpstrack.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point class, sets the Website class as root view.
 */
public class Main implements EntryPoint {

    /**
     * Sets the roots panel main widget.
     */
    public void onModuleLoad() {
        (new Website()).draw();
    }
}