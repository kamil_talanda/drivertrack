package com.bluetill.gpstrack.client.settings.others;

import com.bluetill.gpstrack.client.SettingsService;
import com.bluetill.gpstrack.client.SettingsServiceAsync;
import com.bluetill.gpstrack.client.settings.Settings10nBundle;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 09:01

 */
public class SettingsCompanySymbol extends HLayout {

    private final TextItem companySymbolItem = new TextItem();
    private final Button saveButton = new Button();
    private static final Settings10nBundle BUNDLE = GWT.create(Settings10nBundle.class);

    public SettingsCompanySymbol(){
        DynamicForm dynamicForm = new DynamicForm();
        dynamicForm.setFields(companySymbolItem);

        dynamicForm.setWidth(150);

        companySymbolItem.setWidth("100%");

        companySymbolItem.setTitle(BUNDLE.companyNameTitle());
        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getByName(SettingsDTO.SETTINGS_COMPANY_SYMBOL, new AsyncCallback<SettingsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(SettingsDTO result) {
                companySymbolItem.setValue(result.getValue());
            }
        });

        saveButton.setTitle(BUNDLE.setButtonTitle());
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                SettingsDTO dto = new SettingsDTO();
                dto.setName(SettingsDTO.SETTINGS_COMPANY_SYMBOL);
                dto.setValue((String) companySymbolItem.getValue());
                ((SettingsServiceAsync)GWT.create(SettingsService.class)).set(dto, new AsyncCallback<SettingsDTO>() {

                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(SettingsDTO result) {
                        SC.say(BUNDLE.companyNameSetAlert());
                    }
                });
            };
        });

        this.addMember(dynamicForm);
        this.addMember(saveButton);
    }
}
