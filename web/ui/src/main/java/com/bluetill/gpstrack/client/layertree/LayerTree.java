package com.bluetill.gpstrack.client.layertree;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.map.MapPresenter;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;

import java.util.HashMap;

public class LayerTree extends TreeGrid implements LayerTreeUtils {

    private static final LayerTree10nBundle BUNDLE = GWT.create(LayerTree10nBundle.class);
    private static final String TITLE_WIDTH = "100%";
    private static final int BUTTON_WIDTH = 50;
    private static int uniqueID = 1;

    public LayerTree() {
        super();
        this.setHeight100();
        this.setWidth100();
        this.setBorder("0");
        this.setBooleanFalseImage("checkbox/unchecked.png");
        this.setBooleanTrueImage("checkbox/checked.png");

        TreeGridField titleField = new TreeGridField(ATTRIBUTE_ENTRY_NAME);
        titleField.setWidth(TITLE_WIDTH);
//        TreeGridField zoomButton = new TreeGridField(BUNDLE.zoomGridName());
//        zoomButton.setWidth(BUTTON_WIDTH);
        TreeGridField dsmButton = new TreeGridField("DSM");
        dsmButton.setWidth(BUTTON_WIDTH);
        TreeGridField msgButton = new TreeGridField(BUNDLE.messageGridName());
        msgButton.setWidth(BUTTON_WIDTH);

        setFields(titleField, dsmButton, msgButton);

        setFolderIcon(null);
        setNodeIcon(null);
        setShowPartialSelection(true);
        setSelectionAppearance(SelectionAppearance.CHECKBOX);
        setShowSelectedStyle(false);
        setCascadeSelection(true);
        setShowHeader(false);
        setShowRecordComponentsByCell(true);
        setShowRecordComponents(true);
        setShowAllRecords(true);
        setSelectionType(SelectionStyle.SINGLE);

        setStyleName("mainTextStyle");
    }

    public void setTree(final TreeNode[] nodes) {
        this.setData(createNewTree(nodes));
        this.getTree().openAll();
        this.addSelectionChangedHandler(new SelectionChangedHandler() {
            @Override
            public void onSelectionChanged(SelectionEvent selectionEvent) {

                if (selectionEvent.getState()) {
                    Long curId = ((Integer) selectionEvent.getRecord().toMap().get(ATTRIBUTE_ENTRY_ID)).longValue();
                    if(curId != 1)((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().showDriverTrack(curId, true);
                } else {
                    deselectAllRecords();
                    HashMap a = (HashMap) selectionEvent.getRecord().toMap();

                    String curName = (String) a.get(ATTRIBUTE_ENTRY_NAME);
                    ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().hideDriverTrack(curName);
                }
                ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().refreshDrivers();
            }
        });
    }

    public Tree createNewTree(final TreeNode[] nodes) {
        Tree tr = new Tree();
        //tr.setID("tree" + (uniqueID++));
        tr.setModelType(TreeModelType.PARENT);
        tr.setRootValue(1);
        tr.setNameProperty(ATTRIBUTE_ENTRY_NAME);
        tr.setIdField(ATTRIBUTE_ENTRY_ID);
        tr.setData(nodes);
        tr.openAll();
        return tr;
    }

}
