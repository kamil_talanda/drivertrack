package com.bluetill.gpstrack.client.items.list;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceDateTimeField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 08/01/14
 * Time: 06:32

 */
public class ItemListDataSource extends DataSource implements BaseList, ItemVars {

    static DataSourceIntegerField idField;
    static DataSourceTextField barcodeField;
    static DataSourceTextField carrierField;
    static DataSourceTextField tenantField;
    static DataSourceIntegerField qtyField;
    static DataSourceTextField nameField;
    static DataSourceTextField destinationField;
    static DataSourceDateTimeField addTimeField;
    static DataSourceDateTimeField approvedTimeField;
    static DataSourceTextField holderField;
    static DataSourceTextField statusField;

    public ItemListDataSource(List<ItemDTO> items, String[] driverArray) {
        super();
        this.setClientOnly(true);
        setGridFields(driverArray);

        for(ItemDTO itemDTO : items){
            ItemRecord itemRecord = new ItemRecord(itemDTO);
            this.addData(itemRecord);
        }
    }

    private void setGridFields(String[] driverArray){

        idField = new DataSourceIntegerField(ItemRecord.ID,"id");
        idField.setPrimaryKey(true);
        idField.setHidden(true);

        barcodeField = new DataSourceTextField(ItemRecord.BARCODE, BUNDLE.barcodeColumnName());
        carrierField = new DataSourceTextField(ItemRecord.CARRIER, BUNDLE.carrierColumnName());
        tenantField = new DataSourceTextField(ItemRecord.TENANT, BUNDLE.tenantColumnName());
        qtyField = new DataSourceIntegerField(ItemRecord.QTY,BUNDLE.qtyColumnName());
        nameField = new DataSourceTextField(ItemRecord.NAME, BUNDLE.receiverNameColumnName());
        destinationField = new DataSourceTextField(ItemRecord.DESTINATION, BUNDLE.destinationColumnName());
        addTimeField = new DataSourceDateTimeField(ItemRecord.ADD_TIME, BUNDLE.addedDateColumnName());
        approvedTimeField = new DataSourceDateTimeField(ItemRecord.APPROVE_TIME, BUNDLE.approvedDateColumnName());
        holderField = new DataSourceTextField(ItemRecord.HOLDER_NAME, BUNDLE.driverColumnName());
        holderField.setValueMap(driverArray);
        statusField = new DataSourceTextField(ItemRecord.STATUS, BUNDLE.statusColumnName());
        statusField.setValueMap(BUNDLE.statusInProgress(), BUNDLE.statusDone());

        this.setFields(idField, barcodeField, carrierField, tenantField, qtyField, nameField, destinationField, addTimeField, approvedTimeField, holderField, statusField);
    }

}