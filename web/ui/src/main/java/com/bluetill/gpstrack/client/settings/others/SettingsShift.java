package com.bluetill.gpstrack.client.settings.others;

import com.bluetill.gpstrack.client.SettingsService;
import com.bluetill.gpstrack.client.SettingsServiceAsync;
import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/02/14
 * Time: 09:02

 */
public class SettingsShift extends VLayout implements SettingsVars {

    private ToolStrip toolStrip = new ToolStrip();
    private TextItem deliveryAverageDurationText = new TextItem("deliveryAverageDuration");
    private ToolStripButton setButton = new ToolStripButton();

    public SettingsShift() {
        Label title = new Label("&nbsp" + "Delivery Average Duration [min]" + " :");
        title.setWidth(180);
        toolStrip.addMember(title);
        deliveryAverageDurationText.setShowTitle(false);

        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getByName(SettingsDTO.SETTINGS_DELIVERY_AVERAGE_DURATION, new AsyncCallback<SettingsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(SettingsDTO settingsDTO) {
                if (settingsDTO != null) deliveryAverageDurationText.setValue(settingsDTO.getValue());
            }
        });
        toolStrip.addFormItem(deliveryAverageDurationText);

        setButton.setTitle(BUNDLE.setButtonTitle());
        setButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                try {
                    Integer.parseInt(deliveryAverageDurationText.getValueAsString());

                    SettingsDTO dto = new SettingsDTO();
                    dto.setName(SettingsDTO.SETTINGS_DELIVERY_AVERAGE_DURATION);
                    dto.setValue((String) deliveryAverageDurationText.getValue());
                    ((SettingsServiceAsync) GWT.create(SettingsService.class)).set(dto, new AsyncCallback<SettingsDTO>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(SettingsDTO aVoid) {
                            SC.say("Delivery average duration has been set up.");
                        }
                    });
                } catch (NumberFormatException e) {
                    SC.say("Wrong format.");
                }
            }

            ;
        });

        toolStrip.addButton(setButton);
        this.addMember(toolStrip);

    }
}
