package com.bluetill.gpstrack.client.exception;

import com.google.gwt.i18n.client.Constants;

/**
 * Exception messages.
 */
public interface NotRegisteredExceptionL10nBundle extends Constants {

    /**
     * Exception message.
     * @return string.
     */
    String message();
}
