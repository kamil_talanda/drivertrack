package com.bluetill.gpstrack.client.items.add;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.validator.MaskValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:33

 */
public class ItemAddListGrid extends ListGrid implements ItemVars, BaseList{

    private final ItemAddListGrid instance = this;

    private final ListGridField qtyField = new ListGridField(ItemRecord.QTY, BUNDLE.qtyColumnName(), VERY_SMALL_FIELD_WIDTH);
    private final ListGridField nameField = new ListGridField(ItemRecord.NAME, BUNDLE.receiverNameColumnName());
    private final ListGridField destinationField = new ListGridField(ItemRecord.DESTINATION, BUNDLE.destinationColumnName());
    private final ListGridField removeField = setUpButtonField(REMOVE_IMAGE_URL, REMOVE);

    public ItemAddListGrid(){
        qtyField.setAlign(Alignment.CENTER);
        MaskValidator validator = new MaskValidator();
        validator.setMask("[0-9.]");
        qtyField.setValidators(validator);
        qtyField.setValidateOnChange(true);

        removeField.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(RecordClickEvent recordClickEvent) {
                instance.removeData(recordClickEvent.getRecord());
            }
        });

        this.setWidth100();
        this.setHeight100();
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setBorder(TABLE_BORDER);
        this.setAutoFetchData(true);
        this.setCanEdit(true);
        this.setFields(
                qtyField,
                nameField,
                destinationField,
                removeField
        );
    }

    private ListGridField setUpButtonField(String iconURL, String fieldName) {
        ListGridField listGridField = new ListGridField(fieldName, BUTTON_SIZE);
        listGridField.setType(ListGridFieldType.ICON);
        listGridField.setCellIcon(iconURL);
        listGridField.setCanEdit(false);
        listGridField.setCanFilter(true);
        listGridField.setFilterEditorType(new SpacerItem());
        listGridField.setCanGroupBy(false);
        listGridField.setCanSort(false);
        return listGridField;
    }

}
