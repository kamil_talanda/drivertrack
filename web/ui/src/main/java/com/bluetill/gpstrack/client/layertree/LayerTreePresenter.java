package com.bluetill.gpstrack.client.layertree;

import com.bluetill.gpstrack.client.LegendService;
import com.bluetill.gpstrack.client.LegendServiceAsync;
import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;
import com.bluetill.gpstrack.client.map.MapPresenter;
import com.bluetill.gpstrack.shared.LegendEntry;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Dialog;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.tree.TreeNode;

import java.util.List;

public class LayerTreePresenter extends Presenter<LayerTree> implements LayerTreeUtils {

    private static final LayerTree10nBundle BUNDLE = GWT.create(LayerTree10nBundle.class);
    private static final int BUTTON_WIDTH = 50;
    private static final int DIALOG_WINDOW_WIDTH = 500;


    public LayerTreePresenter() {
        super(new LayerTree() {
            @Override
            protected Canvas createRecordComponent(final ListGridRecord listGridRecord, Integer colNum) {
                switch (colNum) {
                    case 1:
//                        final Button zoomButton = new Button(BUNDLE.zoomButtonTitle());
//                        zoomButton.addClickHandler(new ClickHandler() {
//                            @Override
//                            public void onClick(ClickEvent clickEvent) {
//                                if (getDriverVisibility(listGridRecord.getAttributeAsString(ATTRIBUTE_ENTRY_NAME))) {
//                                    zoomToDriver(listGridRecord.getAttributeAsString(ATTRIBUTE_ENTRY_NAME));
//                                } else {
//                                    SC.say(BUNDLE.driverInvisibleTitle(), BUNDLE.driverInvisibleContent());
//                                }
//                            }
//                        });
//                        zoomButton.setWidth(BUTTON_WIDTH);
//                        return zoomButton;

                        final Button dsmButton = new Button("DSM");
                        dsmButton.setWidth(BUTTON_WIDTH);
                        dsmButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent clickEvent) {
                                if (((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().getLowestOrderDirections() != null)
                                    ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsZoom().zoomToPoint(((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().getLowestOrderDirections());
                                else SC.say("No delivery items on current DSM date.");

//                                Map map = ((MapPresenter) Website.getPresenter(Component.MAP)).getComponent().getMap();
//                                if (((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().getBounds() != null)
//                                    map.zoomToExtent(((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().getBounds());

//                                else SC.say("No delivery items on current DSM date.");
                            }
                        });
                        return dsmButton;


                    case 2:
                        Button msgButton = new Button(BUNDLE.messageButtonTitle());
                        msgButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent clickEvent) {
                                final Dialog dialogProperties = new Dialog();
                                dialogProperties.setWidth(DIALOG_WINDOW_WIDTH);

                                SC.askforValue(BUNDLE.messageTitle(), BUNDLE.messageContent(), "", new ValueCallback() {
                                    @Override
                                    public void execute(String value) {
                                        if (value != null) {

                                            ((LegendServiceAsync) GWT.create(LegendService.class)).setMessage(listGridRecord.getAttributeAsInt(ATTRIBUTE_ENTRY_ID).longValue(), value, new AsyncCallback<Void>() {
                                                @Override
                                                public void onFailure(Throwable caught) {
                                                    SC.say(BUNDLE.errorMessageTitle(), BUNDLE.errorMessageContent());
                                                }

                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    SC.say(BUNDLE.successMessageTitle(), BUNDLE.successMessageContent());
                                                }

                                            });
                                        } else {
                                            SC.say(BUNDLE.cancelMessageTitle(), BUNDLE.cancelMessageContent());
                                        }
                                    }
                                }, dialogProperties);
                            }
                        });
                        msgButton.setWidth(BUTTON_WIDTH);
                        return msgButton;
                    default:
                        return null;
                }
            }
        }, Component.LAYER_TREE);
    }

    public void loadTree() {
        ((LegendServiceAsync) GWT.create(LegendService.class)).getLegend(new AsyncCallback<List<LegendEntry>>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(List<LegendEntry> result) {
                final TreeNode[] nodes = new TreeNode[result.size()];
                for (int i = 0; i < nodes.length; ++i) {
                    final TreeNode node = new TreeNode();
                    node.setAttribute(ATTRIBUTE_ENTRY_NAME, result.get(i).entryName);
                    node.setAttribute(ATTRIBUTE_ENTRY_ID, result.get(i).entryId);
                    node.setIcon(IMAGE_TYPE + "\n" + result.get(i).iconBase64);
                    nodes[i] = node;
                }

                getComponent().setTree(nodes);
            }
        });
    }
}