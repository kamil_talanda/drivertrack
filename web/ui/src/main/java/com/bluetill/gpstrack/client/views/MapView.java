package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.container.View;
import com.bluetill.gpstrack.client.layertree.LayerTree;
import com.bluetill.gpstrack.client.layertree.LayerTreePresenter;
import com.bluetill.gpstrack.client.map.Map10nBundle;
import com.bluetill.gpstrack.client.map.MapPresenter;
import com.bluetill.gpstrack.client.map.MapUtilsDriver;
import com.bluetill.gpstrack.client.map.MapWrapper;
import com.bluetill.gpstrack.shared.Directions;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Slider;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.ValueChangedEvent;
import com.smartgwt.client.widgets.events.ValueChangedHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/12/13
 * Time: 11:50

 */
public class MapView implements View {

    private static final Map10nBundle BUNDLE = GWT.create(Map10nBundle.class);
    private static final int MAIN_MARGIN = 10;
    private static final int SIDE_PANEL_WIDTH = 300;
    private static final int SIDE_PANEL_HIGH_CONTRAST_WIDTH = 600;
    private static final int TIME_SLIDER_HEIGHT = 30;
    private static final int TIMESTAMP_BIN_HEIGHT = 100;
    private static final int TIMESTAMP_SLIDER_HEIGHT = 20;
    private static final String TIMESTAMP_CLASS_CSS = "timestampBin";
    private static final String MAIN_TEXT_CLASS_CSS = "mainTextStyle";
    private static final int INTERVAL_1_NUMBER = 1;
    private static final int INTERVAL_1_VALUE = 1;
    private static final int INTERVAL_2_NUMBER = 2;
    private static final int INTERVAL_2_VALUE = 3;
    private static final int INTERVAL_3_NUMBER = 3;
    private static final int INTERVAL_3_VALUE = 7;
    private static final int INTERVAL_4_NUMBER = 4;
    private static final int INTERVAL_4_VALUE = 31;
    private static final int INTERVAL_LAST_NUMBER = 5;
    private static final int INTERVAL_LAST_VALUE = 0;
    private static final Label timestampLabelMain = new Label();
    private static int timeInterval = 3;
    private final VLayout leftLayout = new VLayout();
    private final LayerTreePresenter layerTreePresenter = (LayerTreePresenter) Website.getPresenter(Component.LAYER_TREE);
    private final LayerTree layerTree = layerTreePresenter.getComponent();
    private final Button showAllDriversButton = new Button();
    private final Canvas timestampBin = new Canvas();
    private final Slider timeSlider = new Slider();
    private final Label timeSliderLabel = new Label();
    private final MapWrapper map = (MapWrapper) Website.getPresenter(Component.MAP).getComponent();
    private final HLayout mapLayout = new HLayout();
    private final HLayout leftSideAndMap = new HLayout();

    public static void setTimestampLabel(String mainTimestamp, String secTimestamp) {
        String timestamp = "<b class='" + MAIN_TEXT_CLASS_CSS + "' style='font-size: 25px;'>" + mainTimestamp + "</b>";
        timestamp += "<br><i class='" + MAIN_TEXT_CLASS_CSS + "' style='font-size: 15px;'>" + secTimestamp + "</i>";
        timestampLabelMain.setContents(timestamp);
    }

    @Override
    public Canvas getBaseLayout() {
        leftLayout.addMember(setAndGetLayerTree());

        DynamicForm form = new DynamicForm();
        DateItem dateItem = new DateItem();
        dateItem.setTitle("DSM date");
        dateItem.setValue(((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().getDsmDate());
        dateItem.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent changedEvent) {
                ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().setDsmDate((Date) changedEvent.getValue());
                ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().refreshDrivers();

            }
        });
        form.setFields(dateItem);
        leftLayout.addMember(form);
        leftLayout.addMember(setAndGetTimestampBin());
        leftLayout.addMember(setAndGetTimeSliderLayout());
//        leftLayout.addMember(setAndGetTimeSlider());
        Label legend = new Label("&nbsp&nbsp&nbsp* DSM - Delivery Schedule on Map");
        legend.setHeight(20);
        leftLayout.addMember(legend);
//        leftLayout.addMember(setAndGetShowAllDriversButton());

        leftSideAndMap.addMember(leftLayout);

        mapLayout.setHeight100();
        mapLayout.addMember(map);

        leftSideAndMap.addMember(mapLayout);

        initCalls();

        return leftSideAndMap;
    }

    private Canvas setAndGetLayerTree() {
        if (Website.isAlternativeSkinEnabled()) {
            leftLayout.setWidth(SIDE_PANEL_HIGH_CONTRAST_WIDTH);
        } else {
            leftLayout.setWidth(SIDE_PANEL_WIDTH);
        }
        layerTree.setMargin(MAIN_MARGIN);
        return layerTree;
    }

    private Canvas setAndGetTimestampBin() {
        timestampBin.setWidth100();
        timestampBin.setHeight(TIMESTAMP_BIN_HEIGHT);
        timestampBin.setStyleName(TIMESTAMP_CLASS_CSS);

        setTimestampLabel(BUNDLE.timestamp(), "");
        timestampLabelMain.setWidth100();
        timestampLabelMain.setAlign(Alignment.CENTER);
        timestampBin.addChild(timestampLabelMain);
        timestampBin.setMargin(MAIN_MARGIN);
        return timestampBin;
    }

    private Canvas setAndGetTimeSliderLayout() {
        timeSliderLabel.setHeight(TIMESTAMP_SLIDER_HEIGHT);
        timeSliderLabel.setAlign(Alignment.CENTER);
        timeSliderLabel.setStyleName(MAIN_TEXT_CLASS_CSS);
        return timeSliderLabel;
    }

    private Canvas setAndGetTimeSlider() {

        timeSlider.setVertical(false);
        timeSlider.setMinValue(INTERVAL_1_NUMBER);
        timeSlider.setMaxValue(INTERVAL_LAST_NUMBER);
        timeSlider.setNumValues(INTERVAL_LAST_NUMBER);
        timeSlider.setShowValue(false);
        timeSlider.setShowTitle(false);
        timeSlider.setShowRange(false);
        timeSlider.setHeight(TIME_SLIDER_HEIGHT);
        timeSlider.setMargin(MAIN_MARGIN);
        timeSlider.setWidth(SIDE_PANEL_WIDTH - (MAIN_MARGIN * 2));


        timeSliderLabel.setContents(BUNDLE.interval_3());
        MapUtilsDriver.setDayInterval(INTERVAL_3_VALUE);
        timeInterval = INTERVAL_3_NUMBER;
        timeSlider.setValue(timeInterval);


        timeSlider.addValueChangedHandler(new ValueChangedHandler() {

            @Override
            public void onValueChanged(ValueChangedEvent valueChangedEvent) {
                switch (valueChangedEvent.getValue()) {
                    case INTERVAL_1_NUMBER:
                        setInterval(INTERVAL_1_NUMBER, INTERVAL_1_VALUE, BUNDLE.interval_1());
                        break;
                    case INTERVAL_2_NUMBER:
                        setInterval(INTERVAL_2_NUMBER, INTERVAL_2_VALUE, BUNDLE.interval_2());
                        break;
                    case INTERVAL_3_NUMBER:
                        setInterval(INTERVAL_3_NUMBER, INTERVAL_3_VALUE, BUNDLE.interval_3());
                        break;
                    case INTERVAL_4_NUMBER:
                        setInterval(INTERVAL_4_NUMBER, INTERVAL_4_VALUE, BUNDLE.interval_4());
                        break;
                    case INTERVAL_LAST_NUMBER:
                        setInterval(INTERVAL_LAST_NUMBER, INTERVAL_LAST_VALUE, BUNDLE.interval_last());
                        break;
                    default:
                        break;
                }

            }
        });
        return timeSlider;
    }

    private Canvas setAndGetShowAllDriversButton() {
        VLayout vLayout = new VLayout();
        showAllDriversButton.setTitle(BUNDLE.zoomToAllButton());
        showAllDriversButton.setWidth100();
        showAllDriversButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsZoom().zoomToAllVisible();
            }
        });
        vLayout.setMargin(MAIN_MARGIN);
        vLayout.addMember(showAllDriversButton);
        return vLayout;
    }

    private void setInterval(int intervalNumber, int intervalValue, String intervalTitle) {
        if (timeInterval != intervalNumber) {
            timeSliderLabel.setContents(intervalTitle);
            MapUtilsDriver.setDayInterval(intervalValue);
            ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().refreshDrivers();
            timeInterval = intervalNumber;
        }
    }

    private void initCalls() {
        ((LayerTreePresenter) Website.getPresenter(Component.LAYER_TREE)).loadTree();
    }

    public void zoomToPoint(Directions directions) {
        ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsZoom().zoomToPoint(directions);
    }

    public void zoomToDriver(String driverName) {
        ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsZoom().zoomToDriver(driverName);
    }

    public void showDriverTrack(long holderId) {
        ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().showDriverTrack(holderId, true);
    }
}