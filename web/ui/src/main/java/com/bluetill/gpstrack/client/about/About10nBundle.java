package com.bluetill.gpstrack.client.about;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 30/12/13
 * Time: 10:43

 */
public interface About10nBundle extends Constants {
    String logoutButton();

    String mobileManualButton();

    String mobileManualTitle();

    String description();

    @Key("It’ll")
    String Itll();

    String googlePlayLink();
}
