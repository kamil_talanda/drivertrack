package com.bluetill.gpstrack.client.drivers;

import com.bluetill.gpstrack.client.DriverService;
import com.bluetill.gpstrack.client.DriverServiceAsync;
import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.drivers.schedule.DriverItemListModal;
import com.bluetill.gpstrack.client.layertree.LayerTree;
import com.bluetill.gpstrack.client.layertree.LayerTreePresenter;
import com.bluetill.gpstrack.client.layertree.LayerTreeUtils;
import com.bluetill.gpstrack.shared.DriverData;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 09/01/14
 * Time: 10:23
 */
public class DriverListGrid extends ListGrid implements LayerTreeUtils {

    public static final String ID_FIELD_NAME = "idField";
    public static final String BM_ID_FIELD_NAME = "bmIdField";
    public static final String NAME_FIELD_NAME = "nameField";
    public static final String DEVICE_ID_FIELD_NAME = "deviceIdField";
    public static final String TASK_LIST_FIELD_NAME = "taskListField";
    public static final String EDIT_FIELD_NAME = "editField";
    public static final String DELETE_FIELD_NAME = "deleteField";
    public static final int ID_FIELD_WIDTH = 30;
    public static final int TASKS_LIST_FIELD_WIDTH = 80;
    public static final int DELETE_FIELD_WIDTH = 60;
    private static final DriverList10nBundle BUNDLE = GWT.create(DriverList10nBundle.class);
    private LayerTree layerTree;
    private ListGrid driverGrid = this;

    public DriverListGrid() {
        this.layerTree = ((LayerTreePresenter) Website.getPresenter(Component.LAYER_TREE)).getComponent();
        this.setLeaveScrollbarGap(false);

        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setWidth100();
        this.setHeight100();
        this.setShowAllRecords(true);
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setBorder("0");

        ListGridField idField = new ListGridField(ID_FIELD_NAME, " ", ID_FIELD_WIDTH);
        ListGridField bmIdField = new ListGridField(BM_ID_FIELD_NAME, BUNDLE.driverIdColumnName(), ID_FIELD_WIDTH);
        bmIdField.setAlign(Alignment.CENTER);
        ListGridField nameField = new ListGridField(NAME_FIELD_NAME, BUNDLE.driverNameColumnName());
        ListGridField deviceIdField = new ListGridField(DEVICE_ID_FIELD_NAME, BUNDLE.deviceIdColumnName());
        ListGridField taskListField = new ListGridField(TASK_LIST_FIELD_NAME, " ", TASKS_LIST_FIELD_WIDTH);
        ListGridField editField = new ListGridField(EDIT_FIELD_NAME, " ", DELETE_FIELD_WIDTH);
        ListGridField deleteField = new ListGridField(DELETE_FIELD_NAME, " ", DELETE_FIELD_WIDTH);
        deleteField.setAlign(Alignment.CENTER);

        idField.setHidden(true);
        this.setFields(idField, bmIdField, nameField, deviceIdField, taskListField, editField, deleteField);
        this.setCanResizeFields(true);
        reload();
        this.setSortField(BM_ID_FIELD_NAME);

    }

    public void reload(){
        ((DriverServiceAsync) GWT.create(DriverService.class)).getAll(new AsyncCallback<ArrayList<DriverDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ArrayList<DriverDTO> driverDTOs) {
                ArrayList<ListGridRecord> items = new ArrayList<ListGridRecord>();
                for (DriverDTO dto : driverDTOs) {
                    items.add(DriverData.createRecord(Long.toString(dto.getId()), Long.toString(dto.getBm_id()), dto.getName(), dto.getDeviceId()));
                }
                setData(items.toArray(new ListGridRecord[items.size()]));
            }
        });
    }

    @Override
    protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {
        String fieldName = this.getFieldName(colNum);
        if (fieldName.equals(DELETE_FIELD_NAME)) {
            Button deleteButton = new Button(BUNDLE.deleteButtonTitle());
            deleteButton.setWidth100();
            deleteButton.setHeight100();
            deleteButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    SC.ask(BUNDLE.deleteDriverAlert(), new BooleanCallback() {
                        public void execute(Boolean value) {
                            if (value != null && value) {
                                ((DriverServiceAsync) GWT.create(DriverService.class)).deleteByName(record.getAttribute(NAME_FIELD_NAME), new AsyncCallback<Boolean>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {
                                    }

                                    @Override
                                    public void onSuccess(Boolean isSuccess) {
                                        deleteDriverFromListAndLayerTree(record);
                                    }
                                });
                            }
                        }
                    });
                }
            });
            return deleteButton;
        } else if (fieldName.equals(EDIT_FIELD_NAME)) {
            Button editButton = new Button(BUNDLE.editButtonTitle());
            editButton.setWidth100();
            editButton.setHeight100();
            editButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new DriverEditModal(DriverListGrid.this, record.getAttribute(ID_FIELD_NAME), record.getAttribute(NAME_FIELD_NAME)).show();
                }
            });
            return editButton;
        } else if (fieldName.equals(TASK_LIST_FIELD_NAME)) {
            Button showTaskListButton = new Button(BUNDLE.scheduleButton());
            showTaskListButton.setWidth100();
            showTaskListButton.setHeight100();
            showTaskListButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    new DriverItemListModal(Long.parseLong(record.getAttribute(ID_FIELD_NAME)), record.getAttribute(NAME_FIELD_NAME)).show();
                }
            });
            return showTaskListButton;
        } else {
            return null;
        }

    }

    private void deleteDriverFromListAndLayerTree(Record record) {
        Record recordToRemove = new Record();
        for (int i = 0; i < layerTree.getRecordList().getLength(); i++) {
            if (layerTree.getRecord(i).getAttribute(ATTRIBUTE_ENTRY_NAME).equals(record.getAttribute(NAME_FIELD_NAME))) {
                recordToRemove = layerTree.getRecord(i);
            }
        }
        layerTree.removeData(recordToRemove);
        driverGrid.removeData(record);
        DriverList.removeFromDriversPathDTOList(record.getAttribute(NAME_FIELD_NAME));
    }

}
