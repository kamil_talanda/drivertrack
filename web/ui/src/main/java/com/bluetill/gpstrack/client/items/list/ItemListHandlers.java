package com.bluetill.gpstrack.client.items.list;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.client.views.ItemView;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DateRange;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.grid.events.*;

import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/01/14
 * Time: 08:10

 */
public class ItemListHandlers implements ItemVars {
    private ItemListGrid context;

    public ItemListHandlers(ItemListGrid inContext) {
        this.context = inContext;
    }

    public RecordClickHandler getRemoveRecordClickHandler() {
        return new RecordClickHandler() {
            public void onRecordClick(final RecordClickEvent event) {
                SC.confirm(BUNDLE.removeWindowTitle(), BUNDLE.removeWindowContent(), new BooleanCallback() {
                    public void execute(Boolean value) {
                        if (Boolean.TRUE.equals(value)) {
                            context.removeData(event.getRecord());
                            ((ItemsServiceAsync) GWT.create(ItemsService.class)).deleteById(event.getRecord().getAttributeAsLong(ItemRecord.ID), new AsyncCallback<Void>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                }

                                @Override
                                public void onSuccess(Void result) {
                                }
                            });

                        }
                    }
                });
            }
        };
    }

    public RecordClickHandler getEditItemHandler() {
        return new RecordClickHandler() {
            @Override
            public void onRecordClick(final RecordClickEvent event) {
                ((ItemsPresenter) Website.getPresenter(Component.TASKS)).getItemDetailsWindow().showWindow(event.getRecord());
            }
        };
    }

    public RecordDropHandler getRecordDropHandler() {
        return new RecordDropHandler() {
            @Override
            public void onRecordDrop(RecordDropEvent recordDropEvent) {

                for (Record record : recordDropEvent.getDropRecords()) {
                    final ItemRecord itemRecord = (ItemRecord) record;
                    itemRecord.setAttribute(ItemListGrid.groupBy, recordDropEvent.getTargetRecord().getAttribute(ItemListGrid.groupBy));
                    saveRecordToDB(itemRecord);
                }
                context.redraw();
                context.groupBy(ItemListGrid.groupBy);
            }
        };
    }

    public EditCompleteHandler getEditCompleteHandler() {
        return new EditCompleteHandler() {
            @Override
            public void onEditComplete(EditCompleteEvent event) {
                Map changedValuesMap = event.getNewValues();
                ItemRecord tmpItemRecord = new ItemRecord(new ItemDTO());

                if (event.getOldRecord() != null) {
                    tmpItemRecord = new ItemRecord(event.getOldRecord());
                }
                final ItemRecord itemRecord = tmpItemRecord;

                for (Object key : changedValuesMap.keySet()) {
                    if (changedValuesMap.get(key) != null) {
                        if (changedValuesMap.get(key).getClass() == Integer.class)
                            itemRecord.setAttribute((String) key, changedValuesMap.get(key) != null ? ((Integer) changedValuesMap.get(key)).longValue() : null);
                        else itemRecord.setAttribute((String) key, changedValuesMap.get(key));
                    }
                }
                saveRecordToDB(itemRecord);

            }
        };
    }

    public FilterEditorSubmitHandler getFilterEditorSubmitHandler() {
        return new FilterEditorSubmitHandler() {
            @Override
            public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
                ItemListFilter.processFilter(event.getCriteria());
            }
        };
    }

    public ChangedHandler getDateFilterChangeHandler(final String fieldName) {
        return new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent changedEvent) {

                AdvancedCriteria criteria = new AdvancedCriteria();
                criteria.addCriteria(new Criterion("tmp", OperatorId.GREATER_OR_EQUAL)); //added to avoid empty criteria

                for (Criterion criterion : ItemView.criteria.asAdvancedCriteria().getCriteria()) {
                    if (!criterion.getValues().get("fieldName").equals(fieldName))
                        criteria.addCriteria(criterion);
                }
                DateRange dateRange = (DateRange) changedEvent.getValue();
                if (dateRange != null) {
                    if (dateRange.getStartDate() != null)
                        criteria.addCriteria(new Criterion(fieldName, OperatorId.GREATER_OR_EQUAL, dateRange.getStartDate()));
                    if (dateRange.getEndDate() != null)
                        dateRange.setEndDate(new Date(dateRange.getEndDate().getTime() + DATE_FILTER_OFFSET));
                        criteria.addCriteria(new Criterion(fieldName, OperatorId.LESS_OR_EQUAL, dateRange.getEndDate()));
                }

                ItemList.getItemsListGrid().filterData(criteria);
                ItemListFilter.processFilter(criteria);
            }
        };
    }

    private void saveRecordToDB(final ItemRecord itemRecord) {
        ((DriverServiceAsync) GWT.create(DriverService.class)).getByName(itemRecord.getAttribute(HOLDER_NAME), new AsyncCallback<DriverDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(DriverDTO dto) {
                itemRecord.setAttribute(HOLDER_ID, (long) dto.getId());
                ((ItemsServiceAsync) com.google.gwt.core.client.GWT.create(ItemsService.class)).set(itemRecord.convertToItemDTO(), new AsyncCallback<ItemDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(ItemDTO itemDTO) {
                    }
                });

            }
        });
    }
}