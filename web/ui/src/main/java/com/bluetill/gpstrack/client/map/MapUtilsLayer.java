package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.client.views.MapView;
import com.bluetill.gpstrack.shared.Directions;
import com.bluetill.gpstrack.shared.LineArray;
import com.bluetill.gpstrack.shared.MapEntry;
import com.google.gwt.core.client.GWT;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.Attributes;

import com.google.gwt.i18n.client.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 03/01/14
 * Time: 13:29
 */
public class MapUtilsLayer {

    private static final Map10nBundle BUNDLE = GWT.create(Map10nBundle.class);
    private static final String PROJECTION_TYPE = "EPSG:4326";
    private static Point endPoint;
    private long endTime;
    private MapPresenter context;
    private Point[] linePoints;

    public MapUtilsLayer(MapPresenter context) {
        this.context = context;
    }

    public Vector getVectorLayer(MapEntry mapEntry, Date dsmDate) {
        final Vector vectorLayer = new Vector(mapEntry.getEntryName());

        for (final HashMap<String, LineArray> curPointListMap : mapEntry.getPointList()) {

            LineArray curPointList = (LineArray) curPointListMap.values().toArray()[0];

            dsmDate.setHours(0);
            dsmDate.setMinutes(0);
            dsmDate.setSeconds(0);

            long start = dsmDate.getTime();
            long end = start + 1000 * 60 * 60 * 24;


            if (curPointList.getPointList().size() > 2) {
                if (endPoint == null) {
                    linePoints = new Point[curPointList.getPointList().size()];
                    for (int i = 0; i < curPointList.getPointList().size(); i++) {
                        LonLat lonLat = new LonLat(curPointList.getPointList().get(i).getLongitude(), curPointList.getPointList().get(i).getLatitude());
                        lonLat.transform(new Projection(PROJECTION_TYPE).getProjectionCode(), context.getComponent().getMap().getProjection());
                        linePoints[i] = new Point(lonLat.lon(), lonLat.lat());
                    }
                } else {
                    linePoints = new Point[curPointList.getPointList().size() + 1];
                    linePoints[0] = endPoint;
                    for (int i = 0; i < curPointList.getPointList().size(); i++) {
                        LonLat lonLat = new LonLat(curPointList.getPointList().get(i).getLongitude(), curPointList.getPointList().get(i).getLatitude());
                        lonLat.transform(new Projection(PROJECTION_TYPE).getProjectionCode(), context.getComponent().getMap().getProjection());
                        linePoints[i + 1] = new Point(lonLat.lon(), lonLat.lat());
                    }
                }

                final LineString line = new LineString(linePoints);

                final VectorFeature lineFeature = new VectorFeature(line, new MapUtilsEntryStyle(mapEntry.getEntryStyle().color, MapUtils.MapEntryType.LINE, null));

                if (curPointList.getTime() != null)
                    lineFeature.setAttributes(createAttribute(MapUtils.AttributeType.TIMESTAMP, curPointList.getTime()));
                else lineFeature.setAttributes(createAttribute(MapUtils.AttributeType.DESCRIPTION, BUNDLE.noData()));

                vectorLayer.addFeature(lineFeature);


                endPoint = linePoints[linePoints.length - 1];
                endTime = curPointList.getTime();

                Directions endPointDirections = curPointList.getPointList().get(curPointList.getPointList().size() - 1);
                if (linePoints.length > 1) {
                    context.getMapUtilsDriver().getActiveDriversMap().put(mapEntry.getEntryName(), endPointDirections);
                }


            } else if (curPointList.getPointList().size() > 0) {
                if (curPointList.getPointList().get(0) != null && curPointList.getTime() != null) {
                    LonLat lonLat = new LonLat(curPointList.getPointList().get(0).getLongitude(), curPointList.getPointList().get(0).getLatitude());
                    lonLat.transform(new Projection(PROJECTION_TYPE).getProjectionCode(), context.getComponent().getMap().getProjection());
                    endPoint = new Point(lonLat.lon(), lonLat.lat());

                    if (!(curPointList.getPointList().get(0).getDescription() == null || curPointList.getPointList().get(0).getDescription().length() == 0)) {
                        final VectorFeature pointFeature = new VectorFeature(endPoint, new MapUtilsEntryStyle(mapEntry.getEntryStyle().color, MapUtils.MapEntryType.POINT, curPointList.getPointList().get(0).getDescription()));
                        if (curPointList.getTime() != null) {
                            pointFeature.setAttributes(createAttribute(MapUtils.AttributeType.ITEM, curPointList.getTime()));
                        } else pointFeature.setAttributes(createAttribute(MapUtils.AttributeType.DESCRIPTION, BUNDLE.noData()));
                        vectorLayer.addFeature(pointFeature);
                    }
                }
            }

        }

        return vectorLayer;
    }

    public Attributes createAttribute(MapUtils.AttributeType type, Object attribute) {
        Attributes timestampAttribute = new Attributes();
        timestampAttribute.setAttribute(type.toString(), attribute.toString());
        return timestampAttribute;
    }

    public void activateLayerSelector(Vector[] vectorLayer) {
        final SelectFeatureOptions selectFeatureOptions = new SelectFeatureOptions();
        // Create the SelectFeature and its Options

        selectFeatureOptions.setHover();

        // Add Select event
        selectFeatureOptions.onSelect(new SelectFeature.SelectFeatureListener() {
            @Override
            public void onFeatureSelected(VectorFeature vectorFeature) {
                DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
                DateTimeFormat timeFormat = DateTimeFormat.getFormat("HH:mm");

                Attributes vectorAttributes = vectorFeature.getAttributes();

                String time = vectorAttributes.getAttributeAsString(MapUtils.AttributeType.TIMESTAMP.toString());
                String item = vectorAttributes.getAttributeAsString(MapUtils.AttributeType.ITEM.toString());
                String description = vectorAttributes.getAttributeAsString(MapUtils.AttributeType.DESCRIPTION.toString());

                if (time != null) {
                    MapView.setTimestampLabel(dateFormat.format(new Date(Long.parseLong(time))), timeFormat.format(new Date(Long.parseLong(time))));
                } else if (item != null){
                    MapView.setTimestampLabel(dateFormat.format(new Date(Long.parseLong(item))), " ");
                } else if(description != null){
                    MapView.setTimestampLabel(description, " ");
                }

            }
        });

        selectFeatureOptions.onUnSelect(new SelectFeature.UnselectFeatureListener() {
            @Override
            public void onFeatureUnselected(VectorFeature vectorFeature) {
                MapView.setTimestampLabel(BUNDLE.timestamp(), "");
            }
        });

        final SelectFeature selectFeatureClick = new SelectFeature(vectorLayer,
                selectFeatureOptions);

        context.getComponent().getMap().addControl(selectFeatureClick);

        selectFeatureClick.activate();
    }
}
