package com.bluetill.gpstrack.client.settings;

import com.google.gwt.core.client.GWT;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 09:22

 */
public interface SettingsVars {
    String NAME_ATTRIBUTE = "name";
    String REMOVE_ATTRIBUTE = "remove";

    Settings10nBundle BUNDLE = GWT.create(Settings10nBundle.class);

    int BUTTON_SIZE = 20;
    String REMOVE_IMAGE_URL = "icons/remove.jpeg";
}
