package com.bluetill.gpstrack.client.map;

import org.gwtopenmaps.openlayers.client.Style;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/01/14
 * Time: 11:09

 */
public class MapUtilsEntryStyle extends Style {

    private static final int LINE_WIDTH = 5;
    private static final int POINT_WIDTH = 12;
    private static final int ITEM_POINT_WIDTH = 5;
    private static final double ENTRY_OPACITY = 0.7;
    private static final String PIN_POINT_IMAGE_URL = "images/pinpoint.png";
    private static final int PIN_POINT_IMAGE_HEIGHT = 64;
    private static final int PIN_POINT_IMAGE_WIDTH = 32;

    public MapUtilsEntryStyle(String color, MapUtils.MapEntryType type, String description) {
        this.setStrokeOpacity(ENTRY_OPACITY);
        this.setFillOpacity(ENTRY_OPACITY);
        this.setStrokeColor(color);
        this.setFillColor(color);

        if (type == MapUtils.MapEntryType.LINE) {
            this.setStrokeWidth(LINE_WIDTH);
        } else if (type == MapUtils.MapEntryType.POINT) {
            this.setPointRadius(POINT_WIDTH);
            this.setFillColor("#000000");
            this.setStrokeColor("#000000");
            if (description != null) this.setLabel(description);
            this.setFontWeight("normal");

        }
    }
}