package com.bluetill.gpstrack.client.drivers;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 20/12/13
 * Time: 15:25

 */
public interface DriverList10nBundle extends Constants {
    String driverIdColumnName();

    String driverNameColumnName();

    String deviceIdColumnName();

    String deleteColumnName();

    String deleteDriverAlert();

    String newDriverButton();

    String newDriverWindowTitle();

    String newDriverWindowContent();

    String driverExists();

    String deleteButtonTitle();

    String exportButtonText();

    String importButtonText();

    String saveButtonText();

    String closeButtonText();

    String exportItemTitleNo();

    String exportItemTitleDate();

    String exportItemTitleDriverNo();

    String exportItemAddress();

    String exportItemTitleSku();

    String exportItemTitleDescription();

    String importWarningNotReady();

    String modalTitle();

    String scheduleButton();

    String refreshButtonText();

    String editButtonTitle();
}
