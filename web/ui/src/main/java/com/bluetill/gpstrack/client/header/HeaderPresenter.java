package com.bluetill.gpstrack.client.header;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/03/14
 * Time: 09:21

 */
public class HeaderPresenter extends Presenter<Header> {

    public HeaderPresenter() {
        super(new Header(), Component.HEADER);
    }
}
