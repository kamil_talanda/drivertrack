package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.items.ItemBarcode;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 23/01/14
 * Time: 08:50

 */
public class ItemListFooterAdd extends HLayout implements ItemVars {

    private static final DynamicForm newItemForm = new DynamicForm();
    private final TextItem barcodeInputItem = getListItem(BUNDLE.barcodeColumnName(), MEDIUM_FIELD_WIDTH);
    private final SelectItem carrierSelectItem = getSelectItem();
    private final SelectItem tenantSelectItem = getSelectItem();
    private final TextItem qtyInputItem = getListItem(BUNDLE.qtyColumnName(), VERY_SMALL_FIELD_WIDTH);
    private final TextItem receiverInputItem = getListItem(BUNDLE.receiverNameColumnName());
    private final TextItem destinationInputItem = getListItem(BUNDLE.destinationColumnName());
    private final TextItem descriptionInputItem = getListItem(BUNDLE.descriptionColumnName());
    private final TextItem addedInputItem = getListItem(BUNDLE.addedDateColumnName(), SMALL_FIELD_WIDTH);
    private final TextItem approvedInputItem = getListItem(BUNDLE.approvedDateColumnName(), SMALL_FIELD_WIDTH);
    private final SelectItem driverSelectItem = getSelectItem();
    private final SelectItem statusSelectItem = getSelectItem();

    public ItemListFooterAdd() {
        newItemForm.setCellPadding(0);

        qtyInputItem.setKeyPressFilter("[0-9.]");

        generateNewBarcode(false);
        driverSelectItem.setValueMap(ItemUIUtils.getDriversMap());
        driverSelectItem.setValue(Long.toString(EMPTY_HOLDER_ID));
        statusSelectItem.setValueMap(ItemUIUtils.getStatusMap());

        ItemUIUtils.setCarriersMap(carrierSelectItem);
        ItemUIUtils.setTenantMap(tenantSelectItem);

        newItemForm.setNumCols(10);
        newItemForm.setFields(
                barcodeInputItem,
                carrierSelectItem,
                tenantSelectItem,
                qtyInputItem,
                receiverInputItem,
                destinationInputItem,
                descriptionInputItem,
                driverSelectItem,
                statusSelectItem
        );
        this.addMember(newItemForm);
    }

    public ItemDTO getDetails() {
        ItemDTO itemDTO = null;
        if (qtyInputItem.validate()) {
            itemDTO = new ItemDTO();
            itemDTO.setBarcode(barcodeInputItem.getValueAsString());
            itemDTO.setCarrier(carrierSelectItem.getValueAsString());
            itemDTO.setTenant(tenantSelectItem.getValueAsString());
            itemDTO.setQty(qtyInputItem.getValue() != null ? Long.parseLong(qtyInputItem.getValueAsString()) : 1);
            itemDTO.setName(receiverInputItem.getValueAsString());
            itemDTO.setDestination(destinationInputItem.getValueAsString());
            itemDTO.setDescription(descriptionInputItem.getValueAsString());
            itemDTO.setAddTime(new Date().getTime());
            //TODO holder id to name
//            itemDTO.setHolderName(ItemUIUtils.getDriversMap().get(driverSelectItem.getValueAsString()));
            itemDTO.setStatus(Long.parseLong(statusSelectItem.getValueAsString()));
        }
        return itemDTO;
    }

    public void generateNewBarcode(final boolean offset) {

        int itemOffset = 0;
        if(offset) itemOffset = 1;

        new ItemBarcode(){
            @Override
            public void barcodeTextAction() {
                barcodeInputItem.setValue(barcodeText);
            }

            @Override
            public void barcodeImageAction() {
            }
        }.generateBarcodeText(itemOffset);
    }

    private SelectItem getSelectItem() {
        SelectItem result = new SelectItem();
        result.setWidth(SMALL_FIELD_WIDTH);
        result.setShowTitle(false);
        result.setDefaultToFirstOption(true);
        return result;
    }

    private TextItem getListItem(String hint, int width) {
        TextItem result = new TextItem();
        result.setShowTitle(false);
        result.setWidth(width);
        result.setHint(hint);
        result.setShowHintInField(true);
        return result;
    }

    private TextItem getListItem(String hint) {
        TextItem result = new TextItem();
        result.setShowTitle(false);
        result.setHint(hint);
        result.setShowHintInField(true);
        return result;
    }
}
