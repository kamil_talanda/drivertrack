package com.bluetill.gpstrack.client.base;

import com.bluetill.gpstrack.client.container.ContainerPresenter;
import com.bluetill.gpstrack.client.drivers.DriverListPresenter;
import com.bluetill.gpstrack.client.exception.NotRegisteredException;
import com.bluetill.gpstrack.client.footer.FooterPresenter;
import com.bluetill.gpstrack.client.header.HeaderPresenter;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.client.layertree.LayerTreePresenter;
import com.bluetill.gpstrack.client.map.MapPresenter;
import com.bluetill.gpstrack.client.settings.SettingsPresenter;

import java.util.HashMap;
import java.util.Map;

/**
 * Presenter factory.
 */
// CHECKSTYLE:LINES_2 ClassDataAbstractionCoupling
// CHECKSTYLE:LINES_1 ClassFanOutComplexity
public class PresenterFactory {

    // Map of lazily initialized presenters.
    private final Map<Component, Presenter<?>> presenterMap;

    /**
     * Instantiates new factory instance with empty presenter hashmap.
     */
    public PresenterFactory() {
        this.presenterMap = new HashMap<Component, Presenter<?>>();
    }

    /**
     * Retrieves presenter instance.
     * Instantiates if necessary.
     *
     * @param component - components presenter instance we want to retrieve.
     * @return new presenter instance.
     * @throws NotRegisteredException - exception thrown if component is not registered.
     */
    public Presenter<?> getPresenter(final Component component) throws NotRegisteredException {
        if (!this.presenterMap.containsKey(component)) {
            this.presenterMap.put(component, instance(component));
        }
        return this.presenterMap.get(component);
    }

    public Presenter<?> resetPresenter(final Component component) throws NotRegisteredException {
        if (!this.presenterMap.containsKey(component)) {
            this.presenterMap.remove(component);
        }
        this.presenterMap.put(component, instance(component));
        return this.presenterMap.get(component);
    }

    /**
     * Creates presenter instance associated with this component.
     *
     * @param component - components presenter instance we want to retrieve.
     * @return presenter instance.
     * @throws NotRegisteredException - exception thrown if component is not registered in
     *                                this method.
     */
    // CHECKSTYLE:LINES_4 CyclomaticComplexity
    // CHECKSTYLE:LINES_3 ReturnCount
    // CHECKSTYLE:LINES_2 MethodLength
    // CHECKSTYLE:LINES_1 NCSS
    private static Presenter<?> instance(final Component component) throws NotRegisteredException {
        // Because GWT does not support reflection.
        switch (component) {
            case HEADER:
                return new HeaderPresenter();
            case FOOTER:
                return new FooterPresenter();
            case CONTAINER:
                return new ContainerPresenter();
            case MAP:
                return new MapPresenter();
            case LAYER_TREE:
                return new LayerTreePresenter();
            case TASKS:
                return new ItemsPresenter();
            case DRIVER:
                return new DriverListPresenter();
            case SETTINGS:
                return new SettingsPresenter();
            default:
                throw new NotRegisteredException();
        }
    }
}
