package com.bluetill.gpstrack.client.base;

import com.google.gwt.user.client.ui.Widget;

/**
 * Basic presenter interface.
 */
public abstract class Presenter<W extends Widget> {

    // Component instance.
    private final W component;


    // Enum presenter identifier.
    private final Component identifier;

    /**
     * For setting the private component field.
     *
     * @param component - widget associated with this presenter.
     */
    public Presenter(W component, Component identifier) {
        this.component = component;
        this.identifier = identifier;
    }

    /**
     * Gets this presenters paired view.
     *
     * @return widget component.
     */
    public W getComponent() {
        return this.component;
    }

    /**
     * Gets presenters component identifier.
     *
     * @return - component enum identifier.
     */
    public Component getIdentifier() {
        return identifier;
    }
}
