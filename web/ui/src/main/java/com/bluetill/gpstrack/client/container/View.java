package com.bluetill.gpstrack.client.container;

import com.smartgwt.client.widgets.Canvas;

/**
 * Abstract view.
 */
public interface View {
    
    /**
     * Returns view's base layout.
     * @return view's base layout
     */
    Canvas getBaseLayout();
}
