package com.bluetill.gpstrack.client.settings.list;

import com.bluetill.gpstrack.client.TenantsService;
import com.bluetill.gpstrack.client.TenantsServiceAsync;
import com.bluetill.gpstrack.shared.dto.TenantsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 09:05

 */
public class SettingsTenant extends SettingsList {

    public SettingsTenant() {
        super();
        setAddButtonTitle(BUNDLE.addNewButtonTenantTitle());
    }

    @Override
    protected void loadData() {
        ((TenantsServiceAsync) GWT.create(TenantsService.class)).getAll(new AsyncCallback<ArrayList<TenantsDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ArrayList<TenantsDTO> results) {
                ArrayList<SettingsRecord> tenantsList = new ArrayList<SettingsRecord>();
                for (TenantsDTO dto : results) {
                    tenantsList.add(new SettingsRecord(dto.getName()));
                }
                SettingsRecord[] tenantsArray = new SettingsRecord[tenantsList.size()];
                tenantsArray = tenantsList.toArray(tenantsArray);
                listGrid.setData(tenantsArray);
            }
        });
    }

    @Override
    protected void addNewRecordToDB(String name) {
        TenantsDTO dto = new TenantsDTO();
        dto.setName(name);
        ((TenantsServiceAsync) GWT.create(TenantsService.class)).set(dto, new AsyncCallback<TenantsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(TenantsDTO result) {
            }
        });
    }

    @Override
    protected void removeRecordFromDB(String name) {
        ((TenantsServiceAsync) GWT.create(TenantsService.class)).deleteByName(name, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(Boolean aBoolean) {
            }
        });
    }

}
