package com.bluetill.gpstrack.client.items;

import com.google.gwt.i18n.client.Constants;

public interface Item10nBundle extends Constants {
    String windowTitleDetails();
    String windowTitleAdd();
    String statusTitle();
    String destinationTitle();
    String holderTitle();
    String descriptionTitle();
    String deleteButtonText();
    String saveButtonText();
    String closeButtonText();
    String printButtonText();

    String statusInProgress();

    String statusDone();

    String addTimeTitle();

    String approveTimeTitle();

    String windowTitle();

    String addButtonText();

    String editButtonText();

    String barcodeColumnName();

    String receiverNameColumnName();

    String destinationColumnName();

    String addedDateColumnName();

    String approvedDateColumnName();

    String driverColumnName();

    String statusColumnName();

    String editColumnName();

    String deleteColumnName();

    String deleteAlert();

    String printEmptyBarcodes();

    String unassigned();

    String assigned();

    String newItemMessage_1();

    String newItemMessage_2();

    String approvePointButton();

    String approveDateTitle();

    String addDateTitle();

    String signatureTitle();

    String carrierColumnName();

    String tenantColumnName();

    String qtyColumnName();

    String removeWindowTitle();

    String removeWindowContent();

    String barcodeTitle();

    String carrierTitle();

    String tenantTitle();

    String qtyTitle();

    String receiverTitle();

    String qtyTypeAlert();

    String addListButtonText();

    String lockBarcodeText();

    String generateBarcodeText();

    String generateBarcodeLockedAlert();

    String descriptionColumnName();

    String groupByText();

    String emptyBarcodeSetNumberAlert();

    String emailButtonText();

    String addItemButtonText();

    String newDeliveryItemTitle();

    String enteredText();

    String newDeliveryItemText();

    String printBarcodeQuestion();

    String customerEmailTitle();

    String customerEmailText();

    String successText();

    String wrongEmailText();

    String disableGrouping();

    String emptyListAlertText();

    String exportTitle();

    String showBarcodeQueue();

    String printBarcodeQueue();

    String barcodeQueueWindowTitle();

    String clearButton();

    String exportButtonText();

    String importButtonText();

}
