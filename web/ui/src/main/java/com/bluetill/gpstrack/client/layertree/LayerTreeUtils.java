package com.bluetill.gpstrack.client.layertree;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 09/01/14
 * Time: 08:55

 */
public interface LayerTreeUtils {
    String ATTRIBUTE_ENTRY_NAME = "entryName";
    String ATTRIBUTE_ENTRY_ID = "entryId";
    String IMAGE_TYPE = "data:image/png;base64,";
}
