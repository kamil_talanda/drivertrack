package com.bluetill.gpstrack.client.items;

import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Img;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 08/01/14
 * Time: 14:22

 */
public abstract class ItemBarcode implements ItemVars{


    public String barcodeText = null;
    public Img barcodeImage = null;

    protected ItemBarcode(String barcodeText) {
        this.barcodeText = barcodeText;
    }

    protected ItemBarcode() {
    }

    public abstract void barcodeTextAction();
    public abstract void barcodeImageAction();

    public void generateBarcodeText(final int numberOffset) {
        if (barcodeText == null) {
            ((ItemsServiceAsync) GWT.create(ItemsService.class)).getMaxItemId(new AsyncCallback<Integer>() {
                @Override
                public void onFailure(Throwable throwable) {
                }

                @Override
                public void onSuccess(Integer itemsNumber) {
                    int barcodeNumber = itemsNumber + numberOffset;
                    barcodeText = ItemUIUtils.generateBarcode(null, barcodeNumber);
                    barcodeTextAction();
                }
            });
        } else {
            barcodeTextAction();
        }
    }

    public void generateBarcodeImageBase64(final int barcodeWidth, final int barcodeHeight) {
        ((ItemsServiceAsync) GWT.create(ItemsService.class)).getBarcodeImage(barcodeText, barcodeWidth, barcodeHeight, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(String resultBarcodeBase64) {
                barcodeImage = ItemUIUtils.getBarcodeImage(resultBarcodeBase64, barcodeWidth, barcodeHeight);
                barcodeImageAction();
            }
        });
    }

    public void generateFullBarcode(final int numberOffset, final int barcodeLabelWidth, final int barcodeLabelHeight) {

        if (barcodeText == null) {
            ((ItemsServiceAsync) GWT.create(ItemsService.class)).getMaxItemId(new AsyncCallback<Integer>() {
                @Override
                public void onFailure(Throwable throwable) {
                }

                @Override
                public void onSuccess(Integer itemsNumber) {
                    int barcodeNumber = itemsNumber + numberOffset;
                    barcodeText = ItemUIUtils.generateBarcode(null, barcodeNumber);
                    generateBarcodeImageBase64(barcodeLabelWidth, new Double(barcodeLabelHeight*0.5).intValue());
                }
            });
        } else {
            generateBarcodeImageBase64(barcodeLabelWidth, new Double(barcodeLabelHeight*0.5).intValue());
        }
    }


//    public void generateFullBarcode(final int numberOffset, final int barcodeWidth, final int barcodeHeight) {
//        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getCompanySymbol(new AsyncCallback<String>() {
//            @Override
//            public void onFailure(Throwable throwable) {
//            }
//
//            @Override
//            public void onSuccess(final String companySymbol) {
//                if (barcodeText == null) {
//                    ((ItemsServiceAsync) GWT.create(ItemsService.class)).getItemsNumber(new AsyncCallback<Integer>() {
//                        @Override
//                        public void onFailure(Throwable throwable) {
//                        }
//
//                        @Override
//                        public void onSuccess(Integer itemsNumber) {
//                            int barcodeNumber = itemsNumber + numberOffset;
//                            barcodeText = generateBarcode(companySymbol, barcodeNumber);
//                            generateBarcodeImageBase64(barcodeWidth, barcodeHeight);
//                        }
//                    });
//                } else {
//                    generateBarcodeImageBase64(barcodeWidth, barcodeHeight);
//                }
//
//            }
//        });
//    }

}
