package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.drivers.DriverVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/02/14
 * Time: 13:14

 */
public class DriverItemListModal extends ModalWindow implements BaseList, DriverVars {

    private static final int MODAL_WIDTH = 610;
    private static final int MODAL_HEIGHT = 360;
    private static final int MODAL_MARGIN = 20;
    private Long driverId;
    private DriverItemListGrid driverTaskListGrid;
    private DriverItemListMenu driverTaskListMenu;


    public DriverItemListModal(Long driverId, String driverName) {
        super();

        this.setTitle(driverName + BUNDLE.modalTitle());
        this.setWidth(MODAL_WIDTH);
        this.setHeight(MODAL_HEIGHT);
        this.setMembersMargin(MODAL_MARGIN);
        this.driverId = driverId;

        reload(true);
    }

    public void reload() {
        reload(false);
    }

    public void reload(final boolean init) {
        ((ItemsServiceAsync) GWT.create(ItemsService.class)).getByDriverId(driverId, new AsyncCallback<List<ItemDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(final List<ItemDTO> itemDTOs) {
                if (!init) {
                    removeItem(driverTaskListGrid);
                    removeItem(driverTaskListMenu);
                }

                driverTaskListGrid = new DriverItemListGrid(DriverItemListModal.this, itemDTOs, driverId);
                driverTaskListMenu = new DriverItemListMenu(DriverItemListModal.this, driverId);
                addItem(driverTaskListGrid);
                addItem(driverTaskListMenu);

                new DriverItemListCalculator(driverId) {
                    @Override
                    protected void successAction() {
                    }
                }.execute();
            }
        });
    }

    public List<ItemDTO> getItemList() {
        return driverTaskListGrid.getItems();
    }

    public Long getDriverId() {
        return driverId;
    }
}