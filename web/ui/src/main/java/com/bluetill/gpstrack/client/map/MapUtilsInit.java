package com.bluetill.gpstrack.client.map;

import com.google.gwt.core.client.GWT;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapUnits;
import org.gwtopenmaps.openlayers.client.MapWidget;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 03/01/14
 * Time: 13:37

 */
public class MapUtilsInit {
    private static final Map10nBundle BUNDLE = GWT.create(Map10nBundle.class);

    private static final String mapWrapperProjectionType = "EPSG:900913";
    private static final int numZoomLevel = 22;
    private static final String FILL_SIZE = "100%";

    public static MapWrapper create() {
        final MapOptions config = new MapOptions();
        config.setNumZoomLevels(numZoomLevel);
        config.setUnits(MapUnits.METERS);
        config.setProjection(mapWrapperProjectionType);
        return new MapWrapper(new MapWidget(FILL_SIZE, FILL_SIZE, config));
    }
}
