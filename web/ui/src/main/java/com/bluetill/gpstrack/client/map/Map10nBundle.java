package com.bluetill.gpstrack.client.map;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 02/01/14
 * Time: 10:13

 */
public interface Map10nBundle extends Constants {
    String noData();

    String timestamp();

    String errorTitle();

    String nothingToShow();

    String interval_1();

    String interval_2();

    String interval_3();

    String interval_4();

    String interval_last();

    String zoomToAllButton();
}
