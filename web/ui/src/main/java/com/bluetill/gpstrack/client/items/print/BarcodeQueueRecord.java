package com.bluetill.gpstrack.client.items.print;

import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/05/14
 * Time: 10:46

 */
public class BarcodeQueueRecord extends ListGridRecord implements ItemVars {

    public BarcodeQueueRecord(String barcode, long qty) {
        this.setAttribute(BARCODE, barcode);
        this.setAttribute(QTY, qty);
    }

    public BarcodeQueueRecord(ItemDTO itemDTO) {
        this(itemDTO.getBarcode(), itemDTO.getQty());
    }
}
