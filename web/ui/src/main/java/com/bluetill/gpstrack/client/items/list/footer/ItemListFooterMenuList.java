package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.items.*;
import com.bluetill.gpstrack.client.items.add.ItemAdd;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.bluetill.gpstrack.client.items.list.ItemListGrid;
import com.bluetill.gpstrack.client.views.ItemView;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/01/14
 * Time: 09:51

 */
public class ItemListFooterMenuList extends ToolStrip implements ItemVars {

    private static final int INPUT_NUMBER_OF_BARCODE_WIDTH = 30;
    private final ToolStripButton exportButton = new ToolStripButton();
    private final ToolStripButton addButton = new ToolStripButton();
    private final ToolStripButton groupButton = new ToolStripButton();
    private final TextItem numberOfBarcodeSetText = new TextItem();
    private final ToolStripButton printEmptyBarcodeSetButton = new ToolStripButton();
    private final UploadItem upload = new UploadItem();
    private String newGroupBy;
    private String newGroupTitle;
    private ItemList context;

    public ItemListFooterMenuList(final ItemList context) {
        this.context = context;

        if (ItemListGrid.groupBy == null){
            newGroupBy = ItemRecord.HOLDER_NAME;
            newGroupTitle = BUNDLE.holderTitle();
        }
        else newGroupBy = null;

        exportButton.setActionType(SelectionType.BUTTON);
        exportButton.setTitle("EXPORT");
        this.addExportButtonClickHandler();
//        this.addButton(exportButton);

        addButton.setActionType(SelectionType.BUTTON);
        addButton.setTitle(BUNDLE.addButtonText());
        this.addAddButtonCLickHandler();
//        this.addButton(addButton);

        if (newGroupBy == null) groupButton.setTitle(BUNDLE.disableGrouping());
        else groupButton.setTitle(BUNDLE.groupByText() + " " + newGroupTitle.toUpperCase());

        groupButton.setActionType(SelectionType.BUTTON);
        this.addGroupButtonClickHandler();
        this.addButton(groupButton);

        numberOfBarcodeSetText.setKeyPressFilter("[0-9.]");
        numberOfBarcodeSetText.setShowTitle(false);
        numberOfBarcodeSetText.setWidth(INPUT_NUMBER_OF_BARCODE_WIDTH);
        numberOfBarcodeSetText.setValue(DEFAULT_NUMBER_OF_BARCODE_IN_SET);
        numberOfBarcodeSetText.setAlign(Alignment.RIGHT);
//        this.addFormItem(numberOfBarcodeSetText);

        printEmptyBarcodeSetButton.setTitle(BUNDLE.printEmptyBarcodes());
//        this.addPrintEmptyBarcodeSetButtonClickHandler();
//        this.addButton(printEmptyBarcodeSetButton);
    }

    private void addExportButtonClickHandler() {
        exportButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                ArrayList<ItemDTO> itemsToExport = new ArrayList<ItemDTO>();
                ListGridRecord[] a = context.getItemsListGrid().getRecords();
                if (a.length > 0) {
                    for (ListGridRecord record : a) {

                        long status = 0l;

                        if (record.getAttribute(ItemRecord.STATUS).equals(BUNDLE.statusDone())) status = 1l;

                        ItemDTO itemToAdd = new ItemDTO(
                                record.getAttributeAsLong(ItemRecord.ID),
                                record.getAttribute(ItemRecord.BARCODE),
                                record.getAttribute(ItemRecord.CARRIER),
                                record.getAttribute(ItemRecord.TENANT),
                                record.getAttributeAsLong(ItemRecord.QTY),
                                record.getAttributeAsLong(ItemRecord.HOLDER_ID),
                                record.getAttribute(ItemRecord.NAME),
                                record.getAttribute(ItemRecord.DESTINATION),
                                record.getAttributeAsLong(ItemRecord.ADD_TIME),
                                record.getAttributeAsLong(ItemRecord.APPROVE_TIME),
                                record.getAttribute(ItemRecord.APPROVE_POINT),
                                record.getAttribute(ItemRecord.SIGNATURE),
                                status,
                                record.getAttribute(ItemRecord.DESCRIPTION),
                                record.getAttribute(ItemRecord.HOLDER_NAME),
                                record.getAttributeAsLong(ItemRecord.DELIVERY_TIME_PLANNED),
                                record.getAttributeAsLong(ItemRecord.ITEM_LIST),
                                record.getAttributeAsLong(ItemRecord.ITEM_LIST_ORDER),
                                record.getAttributeAsLong(ItemRecord.BM_NO)
                        );
                        itemsToExport.add(itemToAdd);
                    }
                } else if(ItemView.criteria.getValues().size()==0){
                    itemsToExport = (ArrayList<ItemDTO>) ItemList.items;
                }

                StringBuilder exportedCSV = ItemUIUtils.exportCSV(itemsToExport);
                Window.Location.assign(GWT.getHostPageBaseURL() + "csvService?data=" + exportedCSV);
            }
        });
    }

    private void addAddButtonCLickHandler() {
        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                new ItemAdd();
            }
        });
    }

    private void addGroupButtonClickHandler() {
        groupButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ItemListGrid.groupBy = newGroupBy;
                ((ItemsPresenter) Website.getPresenter(Component.TASKS)).resetItemView();
            }
        });
    }

    private void addPrintEmptyBarcodeSetButtonClickHandler() {
        printEmptyBarcodeSetButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                if (numberOfBarcodeSetText.getValue() != null) {
                    final int totalBarcodeSetNumber = Integer.parseInt(numberOfBarcodeSetText.getValueAsString());
                    final HashMap<String, Img> barcodeMap = new HashMap<String, Img>();

                    for (int i = 0; i < totalBarcodeSetNumber; i++) {
                        final int generatedBarcodeSetCounter = i;
                        ItemBarcode itemBarcode = new ItemBarcode() {

                            @Override
                            public void barcodeTextAction() {
                            }

                            @Override
                            public void barcodeImageAction() {
                                barcodeMap.put(barcodeText, barcodeImage);
                                if (generatedBarcodeSetCounter == totalBarcodeSetNumber - 1) {
                                    Canvas.printComponents(new Object[]{ItemUIUtils.generateBarcodePrintSet(barcodeMap)});
                                }
                            }
                        };
                        itemBarcode.generateFullBarcode(i, PRINT_BARCODE_WIDTH, PRINT_BARCODE_HEIGHT);
                    }


                } else SC.say(BUNDLE.emptyBarcodeSetNumberAlert());
            }
        });
    }


}
