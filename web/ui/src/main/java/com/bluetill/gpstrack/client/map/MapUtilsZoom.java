package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.client.GlobalVars;
import com.bluetill.gpstrack.shared.Directions;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.util.SC;
import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Projection;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 03/01/14
 * Time: 12:23

 */
public class MapUtilsZoom implements GlobalVars{

    private static final Map10nBundle BUNDLE = GWT.create(Map10nBundle.class);
    private static final Directions initDirections = new Directions(151.215667, -33.866906);  //Sydney center
    private MapPresenter context;

    public MapUtilsZoom(MapPresenter context) {
        this.context = context;
        zoomToPoint(initDirections);
    }

    public void zoomToPoint(Directions directions) {
        Map map = context.getComponent().getMap();
        if (directions != null) {
            LonLat lonLat = new LonLat(directions.getLongitude(), directions.getLatitude());
            lonLat.transform(new Projection(projectionType).getProjectionCode(), map.getProjection());
            map.setCenter(lonLat, pointZoomLevel);
        }
    }

    public void zoomToDriver(String name) {
        zoomToPoint(context.getMapUtilsDriver().getActiveDriversMap().get(name));
    }

    public void zoomToAllVisible() {
        ArrayList<Directions> pointList = new ArrayList<Directions>();
        ArrayList<String> driversNameList = new ArrayList<String>();
        Set<String> driversNameSet = context.getMapUtilsDriver().getActiveDriversMap().keySet();
        for (String key : driversNameSet) {
            if (context.getMapUtilsDriver().getDriverVisibility(key)) {
                pointList.add(context.getMapUtilsDriver().getActiveDriversMap().get(key));
                driversNameList.add(key);
            }
        }
        if (driversNameList.size() > 1) {
            Map map = context.getComponent().getMap();
            Bounds bounds = new Bounds();
            for (Directions curPoint : pointList) {
                LonLat lonLat = new LonLat(curPoint.getLongitude(), curPoint.getLatitude());
                lonLat.transform(new Projection(projectionType).getProjectionCode(), map.getProjection());
                bounds.extend(lonLat);
            }
            map.zoomToExtent(bounds);
        } else if (driversNameList.size() == 1) {
            zoomToDriver(driversNameList.get(0));
        } else {
            SC.say(BUNDLE.errorTitle(), BUNDLE.nothingToShow());
        }
    }
}
