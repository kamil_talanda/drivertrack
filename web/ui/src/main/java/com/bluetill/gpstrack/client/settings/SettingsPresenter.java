package com.bluetill.gpstrack.client.settings;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 08:42

 */
public class SettingsPresenter extends Presenter<SettingsMain> {

    public SettingsPresenter(){
        super(new SettingsMain(), Component.SETTINGS);
    }
}
