package com.bluetill.gpstrack.client.items.print;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.items.ItemVars;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/05/14
 * Time: 09:43

 */
public class BarcodeQueueWindow extends ModalWindow implements BaseList, ItemVars {

    private final BarcodeQueueListGrid barcodeQueueListGrid = new BarcodeQueueListGrid();
    private final BarcodeQueueMenu barcodeQueueMenu = new BarcodeQueueMenu(this);

    public BarcodeQueueWindow() {
        super();

        this.setTitle(BUNDLE.barcodeQueueWindowTitle());
        this.setWidth(WINDOW_WIDTH);
        this.setHeight(WINDOW_HEIGHT_WITHOUT_SIGNATURE);
        this.setMembersMargin(DEFAULT_MEMBERS_MARGIN);
        this.addItem(barcodeQueueListGrid);
        this.addItem(barcodeQueueMenu);

        this.show();
    }
}
