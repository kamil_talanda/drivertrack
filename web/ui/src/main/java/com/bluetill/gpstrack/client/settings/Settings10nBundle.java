package com.bluetill.gpstrack.client.settings;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/12/13
 * Time: 11:17
 */
public interface Settings10nBundle extends Constants {
    String companyNameTitle();
    String companyNameSetAlert();


    String removeAlertTitle();

    String removeAlertText();

    String alreadyExistsAlertText();

    String addNewButtonBaseTitle();

    String addNewButtonCarrierTitle();

    String addNewButtonTenantTitle();

    String emailSignatureText();

    String emailSignatureSetAlert();

    String setButtonTitle();

    String emailFromText();

    String emailFromSetAlert();

    String stateDialogTitle();

    String questionStateDialogText_1();

    String questionStateDialogDisable();

    String questionStateDialogEnable();

    String errorMessage();

    String changePasswordButtonTitle();

    String stateButtonEnabled();

    String stateButtonDisabled();

    String userListGridNameColumnTitle();

    String userListGridPasswordColumnTitle();

    String userListGridStateColumnTitle();

    String changePasswordDialogTitle();

    String changePasswordDialogText();

    String changePasswordSuccess();

    String addNewUserDialogUserNameTitle();

    String addNewUserDialogPasswordTitle();

    String addNewUserButtonTitle();
}
