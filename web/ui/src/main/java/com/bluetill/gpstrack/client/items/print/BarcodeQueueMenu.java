package com.bluetill.gpstrack.client.items.print;

import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/05/14
 * Time: 10:28

 */
public class BarcodeQueueMenu extends HLayout implements ItemVars {

    private BarcodeQueueWindow context;

    protected final Button printButton = new Button();
    protected final Button closeButton = new Button();
    protected final Label spacer = new Label();

    public BarcodeQueueMenu(final BarcodeQueueWindow context) {
        this.context = context;
        spacer.setWidth("100%");
        this.addMember(printButton);
        this.addMember(spacer);
        this.addMember(closeButton);
        this.setLayoutMargin(DEFAULT_MEMBERS_MARGIN);

        printButton.setTitle(BUNDLE.printBarcodeQueue());
        printButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ItemUIUtils.printBarcodeList(BarcodeQueueListGrid.barcodeQueue);
            }
        });

        closeButton.setTitle(BUNDLE.closeButtonText());
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                context.destroy();
            }
        });

    }
}
