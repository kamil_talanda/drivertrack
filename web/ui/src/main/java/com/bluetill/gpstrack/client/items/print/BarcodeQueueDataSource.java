package com.bluetill.gpstrack.client.items.print;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/05/14
 * Time: 10:54

 */
public class BarcodeQueueDataSource extends DataSource implements BaseList, ItemVars {

    static DataSourceTextField barcodeField;
    static DataSourceIntegerField qtyField;

    public BarcodeQueueDataSource(List<ItemDTO> items) {
        super();
        this.setClientOnly(true);
        setGridFields();

        for(ItemDTO itemDTO : items){
            BarcodeQueueRecord barcodeRecord = new BarcodeQueueRecord(itemDTO);
            this.addData(barcodeRecord);
        }
    }

    private void setGridFields(){


        barcodeField = new DataSourceTextField(ItemRecord.BARCODE, BUNDLE.barcodeColumnName());
        barcodeField.setPrimaryKey(true);
        qtyField = new DataSourceIntegerField(ItemRecord.QTY,BUNDLE.qtyColumnName());

        this.setFields(barcodeField, qtyField);
    }
}
