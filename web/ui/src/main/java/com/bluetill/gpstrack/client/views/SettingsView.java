package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.container.View;
import com.bluetill.gpstrack.client.settings.SettingsMain;
import com.smartgwt.client.widgets.Canvas;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 25/12/13
 * Time: 09:00

 */
public class SettingsView implements View {

    @Override
    public Canvas getBaseLayout() {
        return (SettingsMain) Website.getPresenter(Component.SETTINGS).getComponent();
    }
}
