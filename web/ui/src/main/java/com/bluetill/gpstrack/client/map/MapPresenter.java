package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3MapType;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3Options;

/**
 * Map presenter.
 */
public class MapPresenter extends Presenter<MapWrapper> {

    private MapUtilsZoom mapUtilsZoom;
    private MapUtilsDriver mapUtilsDriver;
    private MapUtilsLayer mapUtilsLayer;

    public MapUtilsLayer getMapUtilsLayer() {
        return mapUtilsLayer;
    }

    public MapUtilsDriver getMapUtilsDriver() {
        return this.mapUtilsDriver;
    }

    public void refreshPresenter(){

    }
    public MapUtilsZoom getMapUtilsZoom() {
        return mapUtilsZoom;
    }

    public MapPresenter() {
        super(MapUtilsInit.create(), Component.MAP);
        Map map = getComponent().getMap();

        GoogleV3Options gNormalOptions = new GoogleV3Options();
        gNormalOptions.setIsBaseLayer(true);
        gNormalOptions.setType(GoogleV3MapType.G_NORMAL_MAP);
        GoogleV3 gNormal = new GoogleV3("Google Normal", gNormalOptions);

        map.addLayer(gNormal);
        map.addControl(new ScaleLine());

        mapUtilsZoom = new MapUtilsZoom(this);
        mapUtilsDriver = new MapUtilsDriver(this);
        mapUtilsLayer = new MapUtilsLayer(this);

    }

}