package com.bluetill.gpstrack.client.drivers.schedule.utils;

import com.bluetill.gpstrack.client.base.ModalWindow;
import com.smartgwt.client.widgets.Label;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 16/12/14
 * Time: 09:41

 */
public class WaitingModal extends ModalWindow{
    public WaitingModal() {
        setCanDrop(false);
        setTitle("Please wait");
        setWidth(300);
        setHeight(150);
        setPadding(20);

        Label content = new Label("Data is being updated. Please wait...");
        content.setMargin(10);
        content.setIcon("loading.gif");
        addItem(content);
    }
}
