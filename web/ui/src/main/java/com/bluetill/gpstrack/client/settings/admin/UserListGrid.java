package com.bluetill.gpstrack.client.settings.admin;

import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.client.UserServiceAsync;
import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.bluetill.gpstrack.shared.UserData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.ChangedEvent;
import com.smartgwt.client.widgets.grid.events.ChangedHandler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/05/14
 * Time: 08:28

 */
public class UserListGrid extends ListGrid implements SettingsVars {

    public static final String NAME_FIELD_NAME = "nameField";
    public static final String FULL_NAME_FIELD_NAME = "fullNameField";
    public static final String CHANGE_PASS_FIELD_NAME = "changePassField";
    public static final String ENABLE_FIELD_NAME = "enableField";
    public static final int NAME_FIELD_WIDTH = 140;
    public static final int DELETE_FIELD_WIDTH = 80;
    public static final int CHANGE_PASS_FIELD_WIDTH = 140;
    private static final String STATE_BUTTON_ENABLED_STYLE_CSS = "buttonStateEnabled";
    private static final String STATE_BUTTON_DISABLED_STYLE_CSS = "buttonStateDisabled";
    private ListGridField nameField = new ListGridField(NAME_FIELD_NAME, BUNDLE.userListGridNameColumnTitle(), NAME_FIELD_WIDTH);
    private ListGridField fullNameField = new ListGridField(FULL_NAME_FIELD_NAME, "FULL NAME");
    private ListGridField changePassField = new ListGridField(CHANGE_PASS_FIELD_NAME, BUNDLE.userListGridPasswordColumnTitle(), CHANGE_PASS_FIELD_WIDTH);
    private ListGridField stateField = new ListGridField(ENABLE_FIELD_NAME, BUNDLE.userListGridStateColumnTitle(), DELETE_FIELD_WIDTH);

    public UserListGrid(final List<UserDTO> userDTOList) {
        this.setLeaveScrollbarGap(false);

        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setWidth100();
        this.setHeight100();
        this.setShowAllRecords(true);
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setBorder("0");

        nameField.setCanEdit(false);
        fullNameField.setCanEdit(true);
        fullNameField.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(final ChangedEvent changedEvent) {
                UserDTO dto = new UserDTO();
                dto.setUsername(userDTOList.get(changedEvent.getRowNum() + 1).getUsername());
                ((UserServiceAsync) GWT.create(UserService.class)).get(userDTOList.get(changedEvent.getRowNum() + 1).getUsername(), new AsyncCallback<UserDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(UserDTO userDTO) {
                        userDTO.setFull_name((String) changedEvent.getValue());
                        ((UserServiceAsync) GWT.create(UserService.class)).set(userDTO, new AsyncCallback<UserDTO>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                            }

                            @Override
                            public void onSuccess(UserDTO result) {
                            }
                        });
                    }
                });
            }
        });
        changePassField.setAlign(Alignment.CENTER);
        stateField.setAlign(Alignment.CENTER);

        this.setFields(nameField, fullNameField, changePassField, stateField);
        this.setCanResizeFields(true);
        this.setCanEdit(true);
        this.setData(UserData.getRecords((ArrayList<UserDTO>) userDTOList));
    }

    @Override
    protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {
        String fieldName = this.getFieldName(colNum);
        if (fieldName.equals(ENABLE_FIELD_NAME)) {

            final Button stateButton = new Button();
            stateButton.setWidth100();
            stateButton.setHeight100();

            if (record.getAttributeAsBoolean(ENABLE_FIELD_NAME)) {
                enableStateButton(stateButton);
            } else {
                disableStateButton(stateButton);
            }

            stateButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    String question = BUNDLE.questionStateDialogText_1();
                    question += " ";
                    if (record.getAttributeAsBoolean(ENABLE_FIELD_NAME))
                        question += BUNDLE.questionStateDialogDisable();
                    else question += BUNDLE.questionStateDialogEnable();
                    question += " ";
                    question += record.getAttribute(NAME_FIELD_NAME).toUpperCase();
                    question += "?";
                    SC.ask(BUNDLE.stateDialogTitle(), question, new BooleanCallback() {
                        public void execute(Boolean value) {
                            if (value != null && value) {
                                ((UserServiceAsync) GWT.create(UserService.class)).get(record.getAttributeAsString(NAME_FIELD_NAME), new AsyncCallback<UserDTO>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {

                                    }
                                    @Override
                                    public void onSuccess(UserDTO result) {
                                        result.setEnabled(!result.isEnabled());
                                        ((UserServiceAsync) GWT.create(UserService.class)).set(result, new AsyncCallback<UserDTO>() {
                                            @Override
                                            public void onFailure(Throwable throwable) {
                                                SC.say(BUNDLE.errorMessage());
                                            }

                                            @Override
                                            public void onSuccess(UserDTO result) {
                                                disableStateButton(stateButton);
                                                record.setAttribute(ENABLE_FIELD_NAME, result.isEnabled());
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
            return stateButton;
        } else if (fieldName.equals(CHANGE_PASS_FIELD_NAME)) {
            Button changePassButton = new Button(BUNDLE.changePasswordButtonTitle());
            changePassButton.setWidth100();
            changePassButton.setHeight100();
            changePassButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent clickEvent) {
                    SC.askforValue(BUNDLE.changePasswordDialogTitle(), BUNDLE.changePasswordDialogText(), new ValueCallback() {
                        @Override
                        public void execute(final String password) {
                            ((UserServiceAsync) GWT.create(UserService.class)).get(record.getAttributeAsString(NAME_FIELD_NAME), new AsyncCallback<UserDTO>() {

                                @Override
                                public void onFailure(Throwable throwable) {
                                }

                                @Override
                                public void onSuccess(UserDTO userDTO) {
                                    userDTO.setPassword(md5Message(password));
                                    ((UserServiceAsync) GWT.create(UserService.class)).set(userDTO, new AsyncCallback<UserDTO>() {
                                        @Override
                                        public void onFailure(Throwable throwable) {
                                            SC.say(BUNDLE.errorMessage());
                                        }

                                        @Override
                                        public void onSuccess(UserDTO result) {
                                            if (result == null) SC.say(BUNDLE.errorMessage());
                                            else SC.say(BUNDLE.changePasswordSuccess());
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
            return changePassButton;
        } else if (fieldName.equals(FULL_NAME_FIELD_NAME)) {

        }
        return null;
    }

    private void enableStateButton(Button stateButton) {
        stateButton.setTitle(BUNDLE.stateButtonEnabled());
        stateButton.setBaseStyle(STATE_BUTTON_ENABLED_STYLE_CSS);
    }

    private void disableStateButton(Button stateButton) {
        stateButton.setTitle(BUNDLE.stateButtonDisabled());
        stateButton.setBaseStyle(STATE_BUTTON_DISABLED_STYLE_CSS);
    }

    private String md5Message(String message) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(message.getBytes());
            byte byteData[] = messageDigest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return message;
    }
}
