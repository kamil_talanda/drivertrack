package com.bluetill.gpstrack.client.layertree;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 09/01/14
 * Time: 08:31

 */
public interface LayerTree10nBundle extends Constants {
    String driverInvisibleTitle();

    String driverInvisibleContent();

    String zoomButtonTitle();

    String messageButtonTitle();

    String messageTitle();

    String messageContent();

    String errorMessageTitle();

    String errorMessageContent();

    String successMessageTitle();

    String successMessageContent();

    String cancelMessageTitle();

    String cancelMessageContent();

    String zoomGridName();

    String messageGridName();
}
