package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.container.View;
import com.bluetill.gpstrack.client.drivers.DriverList;
import com.bluetill.gpstrack.client.drivers.DriverListPresenter;
import com.smartgwt.client.widgets.Canvas;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/12/13
 * Time: 12:12

 */
public class DriverView implements View {

    private MainView context;

    public DriverView(MainView context) {
        this.context = context;
    }

    @Override
    public Canvas getBaseLayout() {
        DriverListPresenter presenter = (DriverListPresenter) Website.getPresenter(Component.DRIVER);
        presenter.passContext(context);
        return (DriverList) presenter.getComponent();
    }
}
