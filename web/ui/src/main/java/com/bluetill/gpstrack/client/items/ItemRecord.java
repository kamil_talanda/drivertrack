package com.bluetill.gpstrack.client.items;

import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.sql.Timestamp;

public class ItemRecord extends ListGridRecord implements ItemVars {

    public ItemRecord(Long id, String barcode, String carrier, String tenant, Long qty, Long holderId, String name, String destination, Long addTime, Long approvedTime, Long deliveryTimePlanned, String approvePoint, String signature, String status, String description, String holderName, Long itemList, Long itemListOrder, Long bmNo) {
        this.setAttribute(ID, id);
        this.setAttribute(BARCODE, barcode);
        this.setAttribute(CARRIER, carrier);
        this.setAttribute(TENANT, tenant);
        this.setAttribute(QTY, qty);
        this.setAttribute(HOLDER_ID, holderId);
        this.setAttribute(NAME, name);
        this.setAttribute(DESTINATION, destination);
        if (addTime != null) this.setAttribute(ADD_TIME, new Timestamp(addTime));
        if (approvedTime != null) this.setAttribute(APPROVE_TIME, new Timestamp(approvedTime));
        if (deliveryTimePlanned != null) this.setAttribute(DELIVERY_TIME_PLANNED, new Timestamp(deliveryTimePlanned));
        this.setAttribute(APPROVE_POINT, approvePoint);
        this.setAttribute(SIGNATURE, signature);
        this.setAttribute(STATUS, status);
        this.setAttribute(DESCRIPTION, description);
        this.setAttribute(HOLDER_NAME, holderName);
        this.setAttribute(ITEM_LIST, itemList);
        this.setAttribute(ITEM_LIST_ORDER, itemListOrder);
        this.setAttribute(BM_NO, bmNo);
    }

    public ItemRecord(ListGridRecord listGridRecord) {
        this.setAttribute(ID, listGridRecord.getAttribute(ID));
        this.setAttribute(BARCODE, listGridRecord.getAttribute(BARCODE));
        this.setAttribute(CARRIER, listGridRecord.getAttribute(CARRIER));
        this.setAttribute(TENANT, listGridRecord.getAttribute(TENANT));
        this.setAttribute(QTY, listGridRecord.getAttribute(QTY));
        this.setAttribute(HOLDER_ID, listGridRecord.getAttribute(HOLDER_ID));
        this.setAttribute(NAME, listGridRecord.getAttribute(NAME));
        this.setAttribute(DESTINATION, listGridRecord.getAttribute(DESTINATION));
        if (listGridRecord.getAttribute(ADD_TIME) != null)
            this.setAttribute(ADD_TIME, new Timestamp(listGridRecord.getAttributeAsDate(ADD_TIME).getTime()));
        if (listGridRecord.getAttribute(APPROVE_TIME) != null)
            this.setAttribute(APPROVE_TIME, new Timestamp(listGridRecord.getAttributeAsDate(APPROVE_TIME).getTime()));
        if (listGridRecord.getAttribute(DELIVERY_TIME_PLANNED) != null)
            this.setAttribute(DELIVERY_TIME_PLANNED, new Timestamp(listGridRecord.getAttributeAsDate(DELIVERY_TIME_PLANNED).getTime()));
        this.setAttribute(APPROVE_POINT, listGridRecord.getAttribute(APPROVE_POINT));
        this.setAttribute(SIGNATURE, listGridRecord.getAttribute(SIGNATURE));
        this.setAttribute(STATUS, listGridRecord.getAttribute(STATUS));
        this.setAttribute(DESCRIPTION, listGridRecord.getAttribute(DESCRIPTION));
        this.setAttribute(HOLDER_NAME, listGridRecord.getAttribute(HOLDER_NAME));
        this.setAttribute(ITEM_LIST, listGridRecord.getAttribute(ITEM_LIST));
        this.setAttribute(ITEM_LIST_ORDER, listGridRecord.getAttribute(ITEM_LIST_ORDER));
        this.setAttribute(BM_NO, listGridRecord.getAttribute(BM_NO));
    }

    public ItemRecord(Record record) {
        this.setAttribute(ID, record.getAttribute(ID));
        this.setAttribute(BARCODE, record.getAttribute(BARCODE));
        this.setAttribute(CARRIER, record.getAttribute(CARRIER));
        this.setAttribute(TENANT, record.getAttribute(TENANT));
        this.setAttribute(QTY, record.getAttribute(QTY));
        this.setAttribute(HOLDER_ID, record.getAttribute(HOLDER_ID));
        this.setAttribute(NAME, record.getAttribute(NAME));
        this.setAttribute(DESTINATION, record.getAttribute(DESTINATION));
        if (record.getAttribute(ADD_TIME) != null)
            this.setAttribute(ADD_TIME, new Timestamp(record.getAttributeAsDate(ADD_TIME).getTime()));
        if (record.getAttribute(APPROVE_TIME) != null)
            this.setAttribute(APPROVE_TIME, new Timestamp(record.getAttributeAsDate(APPROVE_TIME).getTime()));
        if (record.getAttribute(DELIVERY_TIME_PLANNED) != null)
            this.setAttribute(DELIVERY_TIME_PLANNED, new Timestamp(record.getAttributeAsDate(DELIVERY_TIME_PLANNED).getTime()));
        this.setAttribute(APPROVE_POINT, record.getAttribute(APPROVE_POINT));
        this.setAttribute(SIGNATURE, record.getAttribute(SIGNATURE));
        this.setAttribute(STATUS, record.getAttribute(STATUS));
        this.setAttribute(DESCRIPTION, record.getAttribute(DESCRIPTION));
        this.setAttribute(HOLDER_NAME, record.getAttribute(HOLDER_NAME));
        this.setAttribute(ITEM_LIST, record.getAttribute(ITEM_LIST));
        this.setAttribute(ITEM_LIST_ORDER, record.getAttribute(ITEM_LIST_ORDER));
        this.setAttribute(BM_NO, record.getAttribute(BM_NO));
    }

    public ItemRecord(ItemDTO itemDTO) {
        this(itemDTO.getId(),
                itemDTO.getBarcode(),
                itemDTO.getCarrier(),
                itemDTO.getTenant(),
                itemDTO.getQty(),
                itemDTO.getHolderId(),
                itemDTO.getName(),
                itemDTO.getDestination(),
                itemDTO.getAddTime(),
                itemDTO.getApproveTime(),
                itemDTO.getDeliveryTimePlanned(),
                itemDTO.getApprovePoint(),
                itemDTO.getSignatureImage(),
                itemDTO.getStatus() != null ? ItemUIUtils.getStatusMap().get(itemDTO.getStatus().toString()) : null,
                itemDTO.getDescription(),
                itemDTO.getHolderName(),
                itemDTO.getItemList(),
                itemDTO.getItemListOrder(),
                itemDTO.getBmNo());
    }

    public ItemDTO convertToItemDTO() {
        return new ItemDTO(
                this.getAttributeAsLong(ID),
                this.getAttribute(BARCODE),
                this.getAttribute(CARRIER),
                this.getAttribute(TENANT),
                Long.valueOf(this.getAttributeAsString(QTY) != null ? this.getAttributeAsString(QTY) : "1"),
                this.getAttributeAsLong(HOLDER_ID),
                this.getAttribute(NAME),
                this.getAttribute(DESTINATION),
                this.getAttributeAsString(ADD_TIME) != null ? Long.valueOf(this.getAttributeAsDate(ADD_TIME).getTime()) : null,
                this.getAttributeAsString(APPROVE_TIME) != null ? Long.valueOf(this.getAttributeAsDate(APPROVE_TIME).getTime()) : null,
                this.getAttribute(APPROVE_POINT),
                this.getAttribute(SIGNATURE),
                ItemUIUtils.getStatusLong(this.getAttribute(STATUS)),
                this.getAttribute(DESCRIPTION),
                this.getAttribute(HOLDER_NAME),
                this.getAttributeAsString(DELIVERY_TIME_PLANNED) != null ? Long.valueOf(this.getAttributeAsDate(DELIVERY_TIME_PLANNED).getTime()) : null,
                this.getAttributeAsString(ITEM_LIST) != null ? Long.valueOf(this.getAttribute(ITEM_LIST)) : null,
                this.getAttributeAsString(ITEM_LIST_ORDER) != null ? Long.valueOf(this.getAttribute(ITEM_LIST_ORDER)) : null,
                this.getAttributeAsString(BM_NO) != null ? Long.valueOf(this.getAttribute(BM_NO)) : null
        );
    }

}
