package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.drivers.DriverListPresenter;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.ItemListDataSource;
import com.bluetill.gpstrack.client.map.MapPresenter;
import com.bluetill.gpstrack.client.map.MapUtils;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.grid.*;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:33

 */
public class DriverItemListGrid extends ListGrid implements ItemVars, BaseList {

    private DriverItemListModal contextModal;
    private List<ItemDTO> items;
    private Long driverId;

    private ListGridField invoiceNoField = new ListGridField(ItemRecord.BARCODE, "Invoice no", SMALL_FIELD_WIDTH);
    private ListGridField destinationField = new ListGridField(ItemRecord.DESTINATION, BUNDLE.destinationColumnName());
    private ListGridField itemListField = new ListGridField(ItemRecord.ITEM_LIST, "", SMALL_FIELD_WIDTH);
    private ListGridField itemListOrderField = new ListGridField(ItemRecord.ITEM_LIST_ORDER, "", SMALL_FIELD_WIDTH);
    private ListGridField timeField = new ListGridField(ItemRecord.APPROVE_TIME, "time", MEDIUM_FIELD_WIDTH);
    private ListGridField statusField = new ListGridField(ItemRecord.STATUS, "status", MEDIUM_FIELD_WIDTH);
    private ListGridField editField = setUpButtonField(EDIT_IMAGE_URL, EDIT);
    private ListGridField removeField = setUpButtonField(REMOVE_IMAGE_URL, REMOVE);

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public DriverItemListGrid(DriverItemListModal contextModal, List<ItemDTO> items, Long driverId) {
        this.setHeight100();
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setBorder(TABLE_BORDER);
        this.setDataSource(new ItemListDataSource(items, null));
        this.setAutoFetchData(true);
        this.setLeaveScrollbarGap(false);
        this.contextModal = contextModal;
        this.items = items;
        this.driverId = driverId;

        this.setGridFields();

        sort(ItemRecord.APPROVE_TIME, SortDirection.ASCENDING);
        groupBy(ItemRecord.ITEM_LIST);
    }

    private void setGridFields() {

        destinationField.setCanEdit(false);

        timeField.setAlign(Alignment.CENTER);
        timeField.setCellFormatter(new CellFormatter() {
            @Override
            public String format(Object value, ListGridRecord listGridRecord, int i, int i2) {
                String output = null;
                if (value != null) {
                    DateTimeFormat outputFormat = DateTimeFormat.getFormat("HH:mm");
                    output = outputFormat.format((Date)value);
                }
                return output;
            }
        });

        editField.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(RecordClickEvent event) {
                new DriverItemModal(contextModal, items, event.getRecord().getAttributeAsLong(ItemRecord.ID)).show();
            }
        });

        removeField.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(final RecordClickEvent event) {
                SC.confirm(BUNDLE.removeWindowTitle(), BUNDLE.removeWindowContent(), new BooleanCallback() {
                    public void execute(Boolean value) {
                        if (Boolean.TRUE.equals(value)) {
                            removeData(event.getRecord());
                            ((ItemsServiceAsync) GWT.create(ItemsService.class)).deleteById(event.getRecord().getAttributeAsLong(ItemRecord.ID), new AsyncCallback<Void>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                }

                                @Override
                                public void onSuccess(Void result) {

                                }
                            });

                        }
                    }
                });
            }
        });

        itemListField.setHidden(true);
        itemListOrderField.setHidden(true);

        itemListField.setGroupValueFunction(new GroupValueFunction() {
            @Override
            public Object getGroupValue(Object value, ListGridRecord listGridRecord, ListGridField listGridField, String s, ListGrid listGrid) {
                if (value != null){
                    DateTimeFormat outputFormat = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");
                    return  outputFormat.format(new Date((Long)value));
                }
                else return "not assigned";
            }
        });

        this.setFields(
                invoiceNoField,
                destinationField,
                timeField,
                statusField,

                itemListField,
                itemListOrderField,

                editField,
                removeField
        );
    }

    private ListGridField setUpButtonField(String iconURL, String fieldName) {
        ListGridField listGridField = new ListGridField(fieldName, BUTTON_SIZE);
        listGridField.setType(ListGridFieldType.ICON);
        listGridField.setCellIcon(iconURL);
        listGridField.setCanEdit(false);
        listGridField.setCanFilter(true);
        listGridField.setFilterEditorType(new SpacerItem());
        listGridField.setCanGroupBy(false);
        listGridField.setCanSort(false);
        return listGridField;
    }

    private void zoomToPoint(String point, Long driverId) {
        ((MapPresenter) Website.resetPresenter(Component.MAP)).refreshPresenter();
        ((DriverListPresenter) Website.getPresenter(Component.DRIVER)).zoomToPoint(MapUtils.getPointFromWKT(point, null), driverId);
        contextModal.hide();
    }

    private void zoomToDriver(String driverName) {
//        ((MapPresenter) Website.resetPresenter(Component.MAP)).refreshPresenter();
//        ((LayerTreePresenter) Website.resetPresenter(Component.LAYER_TREE)).refreshLayerTree();
//        ((LayerTreePresenter) Website.getPresenter(Component.LAYER_TREE)).selectDriver();
        ((DriverListPresenter) Website.getPresenter(Component.DRIVER)).zoomToDriver(driverName);
        contextModal.hide();
    }
}
