package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.drivers.DriverVars;
import com.bluetill.gpstrack.client.drivers.schedule.utils.DriverNotifier;
import com.bluetill.gpstrack.client.drivers.schedule.utils.WaitingModal;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:40
 */
public class DriverItemListMenu extends HLayout implements DriverVars {

    protected final Button refreshButton = new Button();
    protected final Button closeButton = new Button();
    protected final Label spacer = new Label();

    public DriverItemListMenu(final DriverItemListModal contextModal, final Long driverId){

        spacer.setWidth("100%");

        this.addMember(refreshButton);
        this.addMember(spacer);
        this.addMember(closeButton);

        refreshButton.setTitle(BUNDLE.refreshButtonText());
        refreshButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                final WaitingModal modal = new WaitingModal();
                modal.show();
                new DriverItemListCalculator(driverId) {
                    @Override
                    protected void successAction() {
                        modal.hide();
                        contextModal.reload();
                        new DriverNotifier(driverId).notifyChanges();
                    }
                }.execute();
            }
        });
        closeButton.setTitle(BUNDLE.closeButtonText());
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                contextModal.destroy();
            }
        });

    }
}
