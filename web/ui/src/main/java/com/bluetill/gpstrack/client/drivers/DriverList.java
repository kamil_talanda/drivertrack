package com.bluetill.gpstrack.client.drivers;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.drivers.schedule.DriverItemListMenuImport;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Dialog;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/12/13
 * Time: 19:26

 */
public class DriverList extends VLayout implements BaseList, GlobalVars, DriverVars {

    private static List<DriversPathDTO> driversPathDTOList = new ArrayList<DriversPathDTO>();
    private DriverList driversList = this;

    final Button addButton = new Button(BUNDLE.newDriverButton());

    public DriverList() {
        this.setMargin(MAIN_MARGIN);

        ((MapServiceAsync) GWT.create(MapService.class)).getAll(new AsyncCallback<List<DriversPathDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(final List<DriversPathDTO> resultDriverList) {
                driversPathDTOList = resultDriverList;
                driversList.addMember(new DriverListGrid());
                addButton.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent clickEvent) {
                        final Dialog dialogProperties = new Dialog();
                        dialogProperties.setWidth(500);

                        SC.askforValue(BUNDLE.newDriverWindowTitle(), BUNDLE.newDriverWindowTitle(), "", new ValueCallback() {
                            @Override
                            public void execute(final String value) {
                                if (value != null) {
                                    DriverDTO dto = new DriverDTO();
                                    dto.setName(value.toLowerCase());
                                    ((DriverServiceAsync) GWT.create(DriverService.class)).set(dto, new AsyncCallback<DriverDTO>() {

                                        @Override
                                        public void onFailure(Throwable throwable) {
                                        }

                                        @Override
                                        public void onSuccess(DriverDTO result) {
                                            if (result == null) {
                                                SC.say(BUNDLE.driverExists());
                                            } else {
                                                DriversPathDTO driversPathDTO = new DriversPathDTO();
                                                driversPathDTO.setId(result.getId());
                                                driversPathDTO.setName(value.toLowerCase());
                                                driversPathDTOList.add(driversPathDTO);
                                                HLayout menuLayout = new HLayout();
                                                menuLayout.addMember(addButton);
                                                menuLayout.addMember(new DriverItemListMenuImport());
                                                driversList.setMembers(new DriverListGrid(), menuLayout);
                                            }
                                        }
                                    });

                                } else {
                                    SC.say("Error", "Operation canceled.");
                                }
                            }
                        }, dialogProperties);
                    }
                });

                HLayout menuLayout = new HLayout();
                menuLayout.addMember(addButton);
                menuLayout.addMember(new DriverItemListMenuImport());
                driversList.addMember(menuLayout);
            }

        });
    }

    public static void removeFromDriversPathDTOList(String driverName) {
        List<DriversPathDTO> tmpList = new ArrayList<DriversPathDTO>();
        for (DriversPathDTO driversPathDTO : driversPathDTOList) {
            if (!driversPathDTO.getName().equals(driverName)) tmpList.add(driversPathDTO);
        }
        driversPathDTOList = tmpList;
    }

}
