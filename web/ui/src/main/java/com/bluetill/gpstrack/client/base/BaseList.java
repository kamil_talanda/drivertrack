package com.bluetill.gpstrack.client.base;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/12/13
 * Time: 19:29

 */
public interface BaseList {
    String TABLE_BORDER = "0";
    String FOOTER_HEIGHT = "35px";
    String FOOTER_WIDTH = "100%";
    int FOOTER_PADDING = 5;
    int BUTTON_FIELD_WIDTH = 80;

    int SMALL_SEPARATOR = 150;

}
