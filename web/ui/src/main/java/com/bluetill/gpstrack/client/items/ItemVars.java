package com.bluetill.gpstrack.client.items;

import com.google.gwt.core.client.GWT;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/01/14
 * Time: 07:05

 */
public interface ItemVars {

    String ID = "id";
    String BARCODE = "barcode";
    String CARRIER = "carrier";
    String TENANT = "tenant";
    String QTY = "qty";
    String HOLDER_ID = "holderId";
    String NAME = "receiver";
    String DESTINATION = "destination";
    String ADD_TIME = "addTime";
    String APPROVE_TIME = "approveTime";
    String DELIVERY_TIME_PLANNED = "deliveryTimePlanned";
    String APPROVE_POINT = "approvePoint";
    String SIGNATURE = "signature";
    String STATUS = "status";
    String DESCRIPTION = "description";
    String ITEM_LIST = "item_list";
    String ITEM_LIST_ORDER = "item_list_order";
    String BM_NO = "bm_no";

    String EDIT = "editAction";
    String ZOOM = "zoomAction";
    String REMOVE = "removeAction";

    String HOLDER_NAME = "holderName";

    int INIT_TIME_OFFSET = 31;

    int DATE_FILTER_OFFSET = 24 * 60  * 60 * 1000 - 1;

    public static long EMPTY_HOLDER_ID = -1;

    int DEFAULT_MEMBERS_MARGIN = 10;

    int VERY_SMALL_FIELD_WIDTH = 50;
    int SMALL_FIELD_WIDTH = 75;
    int MEDIUM_FIELD_WIDTH = 100;

    int PRINT_BARCODE_WIDTH = 151;
    int PRINT_BARCODE_HEIGHT = 57;

    Integer DETAILS_BARCODE_HEIGHT = 100;
    Integer DETAILS_BARCODE_WIDTH = 270;

    Long STATUS_DONE = (long)1;
    Long STATUS_IN_PROGRESS = (long)0;

    String IMAGE_FORMAT = "data:image/png;base64,";

    int DEFAULT_NUMBER_OF_BARCODE_IN_SET = 10;

    String BARCODE_DATE_FORMAT = "yyMM";

    Item10nBundle BUNDLE = GWT.create(Item10nBundle.class);

    String EMAIL_REGEX = "^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$";

    String NO_CARRIER = "No Carrier";
    String NO_TENANT = "No Tenant";
    String NO_DRIVER = "No Driver";

    String WINDOW_HEIGHT_WITHOUT_SIGNATURE = "360px";
    String WINDOW_WIDTH = "610px";
    int BUTTON_SIZE = 20;

    String EDIT_IMAGE_URL = "icons/edit.png";
    String REMOVE_IMAGE_URL = "icons/remove.jpeg";

    String START_NEW_LINE = "_START_NEW_LINE_ITEM_";
}
