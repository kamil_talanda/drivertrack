package com.bluetill.gpstrack.client.settings.others;

import com.bluetill.gpstrack.client.GeocoderService;
import com.bluetill.gpstrack.client.GeocoderServiceAsync;
import com.bluetill.gpstrack.client.SettingsService;
import com.bluetill.gpstrack.client.SettingsServiceAsync;
import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/02/14
 * Time: 09:02

 */
public class SettingsOfficeAddress extends VLayout implements SettingsVars {

    private ToolStrip toolStrip = new ToolStrip();
    private TextItem destinationInputText = new TextItem("destination");
    private TextItem destinationText = new TextItem("destination_value");
    private ToolStripButton setButton = new ToolStripButton();

    public SettingsOfficeAddress() {
        toolStrip.addMember(new Label("&nbsp" + "Office address" + " :"));
        destinationInputText.setShowTitle(false);
        destinationInputText.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent changedEvent) {
                resolveDestination();
            }
        });
        destinationInputText.setWidth("100%");
        destinationText.setShowTitle(false);
        destinationText.setDisabled(true);
        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getByName(SettingsDTO.SETTINGS_OFFICE_ADDRESS, new AsyncCallback<SettingsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(SettingsDTO settingsDTO) {
                if (settingsDTO != null) destinationText.setValue(settingsDTO.getValue());
                if (settingsDTO != null) destinationInputText.setValue(settingsDTO.getValue());
            }
        });
        toolStrip.addFormItem(destinationInputText);
        toolStrip.addFormItem(destinationText);

        setButton.setTitle(BUNDLE.setButtonTitle());
        setButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                SettingsDTO dto = new SettingsDTO();
                dto.setName(SettingsDTO.SETTINGS_OFFICE_ADDRESS);
                dto.setValue((String) destinationText.getValue());
                ((SettingsServiceAsync) GWT.create(SettingsService.class)).set(dto, new AsyncCallback<SettingsDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(SettingsDTO aVoid) {
                        destinationInputText.setValue(destinationText.getValue());
                        SC.say("Office address has been set up.");
                    }
                });
            }

            ;
        });

        toolStrip.addButton(setButton);
        this.addMember(toolStrip);
        resolveDestination();

    }

    private void resolveDestination(){
        ((GeocoderServiceAsync) GWT.create(GeocoderService.class)).geocodeAddress(destinationInputText.getValueAsString(), new AsyncCallback<String[]>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(String[] locationDetails) {
                if(locationDetails != null && locationDetails.length > 1){
                    destinationText.setValue(locationDetails[1]);
                }

            }
        });
    }
}
