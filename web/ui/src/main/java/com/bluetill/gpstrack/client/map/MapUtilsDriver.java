package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.shared.Directions;
import com.bluetill.gpstrack.shared.LineArray;
import com.bluetill.gpstrack.shared.MapEntry;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 03/01/14
 * Time: 12:53
 */
public class MapUtilsDriver extends MapUtils implements GlobalVars {

    private static final int INIT_DAY_INTERVAL = 7;
    private static final long DAY_MILLISECONDS = 1000 * 60 * 60 * 24;
    private static int dayInterval;
    private static boolean inProgress = false;
    public HashMap<String, Vector> vectorMap = new HashMap<String, Vector>();
    private Bounds bounds;
    private MapPresenter context;
    private ArrayList<DriversPathDTO> mainDriversList;      //all drivers with all details (DTO)
    private HashMap<String, Directions> activeDriversMap;   //drivers already loaded and cached
    private String visibleDriver;           //drivers visible - checked on the tree
    private Date dsmDate = new Date();
    private Directions lowestOrderDirections;

    public MapUtilsDriver(MapPresenter context) {
        this.context = context;

        dayInterval = INIT_DAY_INTERVAL;
        mainDriversList = new ArrayList<DriversPathDTO>();
        activeDriversMap = new HashMap<String, Directions>();

        ((MapServiceAsync) GWT.create(MapService.class)).getAll(new AsyncCallback<List<DriversPathDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<DriversPathDTO> driversPathDTOList) {
                for (DriversPathDTO driversPathDTO : driversPathDTOList) {
                    mainDriversList.add(driversPathDTO);
                }
            }
        });
    }

    public static void setDayInterval(int dayInterval) {
        MapUtilsDriver.dayInterval = dayInterval;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Directions getLowestOrderDirections() {
        return lowestOrderDirections;
    }

    public void setLowestOrderDirections(Directions lowestOrderDirections) {
        this.lowestOrderDirections = lowestOrderDirections;
    }

    public Date getDsmDate() {
        return dsmDate;
    }

    public void setDsmDate(Date dsmDate) {
        this.dsmDate = dsmDate;
    }

    public HashMap<String, Directions> getActiveDriversMap() {
        return activeDriversMap;
    }

    //TODO check this functions to make it simpler
    public void showDriverTrack(final Long driverId, final boolean zoom) {
        if (!inProgress) {
            inProgress = true;
            ((DriverServiceAsync) GWT.create(DriverService.class)).get(driverId, new AsyncCallback<DriverDTO>() {
                @Override
                public void onFailure(Throwable throwable) {
                    inProgress = false;
                }

                @Override
                public void onSuccess(DriverDTO dto) {
                    final Map map = context.getComponent().getMap();
                    if (dto != null && map.getLayerByName(dto.getName()) == null) {


                        ((MapServiceAsync) GWT.create(MapService.class)).getDriverTrack(map.getProjection(), dto.getId(), dsmDate.getTime(), new AsyncCallback<DriversPathDTO>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                                inProgress = false;
                            }

                            @Override
                            public void onSuccess(final DriversPathDTO driversPathDTO) {
                                final MapEntry mapEntry = new MapEntry((int) (long) driversPathDTO.getId(), driversPathDTO.getName(), driversPathDTO.getStyle(), new ArrayList<HashMap<String, LineArray>>());

                                for (LineArray lineArray : driversPathDTO.getPointList()) {
                                    addMapToPointLineList(null, lineArray, mapEntry.getPointList());
                                }
                                ((ItemsServiceAsync) GWT.create(ItemsService.class)).getByDriverId(driversPathDTO.getId(), new AsyncCallback<List<ItemDTO>>() {
                                    @Override
                                    public void onFailure(Throwable throwable) {
                                        inProgress = false;
                                    }

                                    @Override
                                    public void onSuccess(final List<ItemDTO> itemDTOs) {
                                        bounds = null;
                                        if (itemDTOs.size() > 0) {
                                            ItemDTO lowestOrder = null;
                                            for (final ItemDTO itemDTO : itemDTOs) {
                                                if (itemDTO.getApproveTime() != null) {
                                                    Date approveTime = new Date(itemDTO.getApproveTime());

                                                    if (approveTime.getTime() / DAY_MILLISECONDS == dsmDate.getTime() / DAY_MILLISECONDS) {
                                                        ArrayList<Directions> directionsList = new ArrayList<Directions>();
                                                        directionsList.add(MapUtils.getPointFromWKT(itemDTO.getApprovePoint(), Long.toString(itemDTO.getItemListOrder())));
                                                        Long approveTimestamp = new Date().getTime();
                                                        if (itemDTO.getApproveTime() != null)
                                                            approveTimestamp = itemDTO.getApproveTime();
                                                        addMapToPointLineList(itemDTO.getName(), new LineArray(approveTimestamp, directionsList), mapEntry.getPointList());
                                                        if (lowestOrder == null || itemDTO.getItemListOrder() < lowestOrder.getItemListOrder()) {
                                                            Directions directions = getPointFromWKT(itemDTO.getApprovePoint(), Long.toString(itemDTO.getItemListOrder()));
                                                            LonLat lonLat = new LonLat(directions.getLongitude(), directions.getLatitude());
                                                            lonLat.transform(new Projection(projectionType).getProjectionCode(), map.getProjection());
                                                            bounds = new Bounds();
                                                            bounds.extend(lonLat);
                                                            lowestOrder = itemDTO;
                                                            lowestOrderDirections = directions;
                                                        }

                                                    }

                                                    if (itemDTOs.indexOf(itemDTO) == itemDTOs.size() - 1) {
                                                        showMapEntryOnTheMap(mapEntry);
                                                        if (zoom)
                                                            context.getMapUtilsZoom().zoomToDriver(driversPathDTO.getName());
//                                                        else if (bounds != null)
//                                                            context.getComponent().getMap().zoomToExtent(getBounds());
                                                    }
                                                }
                                            }
                                        }
                                        inProgress = false;
                                    }
                                });
                            }
                        });
                    } else {
                        inProgress = false;
                        if (zoom && dto != null) context.getMapUtilsZoom().zoomToDriver(dto.getName());
                    }
                    if (dto != null) setDriverVisibility(dto.getName(), true);
                }
            });
        }

    }

    private void addMapToPointLineList(String description, LineArray lineArray, ArrayList<HashMap<String, LineArray>> inputLineList) {
        HashMap<String, LineArray> tmpPointMap = new HashMap<String, LineArray>();
        tmpPointMap.put(description, lineArray);
        inputLineList.add(tmpPointMap);
    }

    public void hideDriverTrack(String driverName) {
        Map map = context.getComponent().getMap();
        if (map.getLayerByName(driverName) != null) {
            setDriverVisibility(driverName, false);
        }
    }

    public void removeDriverTrack(String driverName) {
        Map map = context.getComponent().getMap();
        activeDriversMap.remove(driverName);
        if (map.getLayerByName(driverName) != null) {
            vectorMap.remove(driverName);
            map.getLayerByName(driverName).setIsVisible(false);
            map.removeLayer(map.getLayerByName(driverName));
        }
    }

    public void showMapEntryOnTheMap(MapEntry mapEntry) {
        if (mapEntry.getPointList() != null) {
            Vector vector = context.getMapUtilsLayer().getVectorLayer(mapEntry, dsmDate);
            context.getComponent().getMap().addLayer(vector);
            vectorMap.put(mapEntry.getEntryName(), vector);
            ArrayList<Vector> vectorArrayList = new ArrayList<Vector>(vectorMap.values());
            context.getMapUtilsLayer().activateLayerSelector(vectorArrayList.toArray(new Vector[vectorArrayList.size()]));
        }
    }

    public void refreshDrivers() {

        for (DriversPathDTO driversPathDTO : mainDriversList) {
            removeDriverTrack(driversPathDTO.getName());
            if (visibleDriver != null && visibleDriver.equals(driversPathDTO.getName())) {
                ((DriverServiceAsync) GWT.create(DriverService.class)).getByName(driversPathDTO.getName(), new AsyncCallback<DriverDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(DriverDTO dto) {
                        ((MapPresenter) Website.getPresenter(Component.MAP)).getMapUtilsDriver().showDriverTrack(dto.getId(), false);
                    }
                });
            }
        }
    }

    public void setDriverVisibility(String driverName, boolean isVisible) {
        if (isVisible) {
            visibleDriver = driverName;
        } else {
            visibleDriver = null;
        }
    }

    public boolean getDriverVisibility(String driverName) {
        Map map = context.getComponent().getMap();
        if (map.getLayerByName(driverName) != null) {
            return map.getLayerByName(driverName).isVisible();
        }
        return false;
    }
}
