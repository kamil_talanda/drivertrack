package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.GlobalVars;
import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.client.UserServiceAsync;
import com.bluetill.gpstrack.client.container.View;
import com.bluetill.gpstrack.shared.Directions;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabDeselectedEvent;
import com.smartgwt.client.widgets.tab.events.TabDeselectedHandler;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

/**
 * Map view with layer tree, toolbox and map.
 */
public class MainView implements View, GlobalVars {



    private static final String SCROLL_IMAGE_URL = "TabSet/drivertrack_scroll.png";
    private static final String ABOUT_MENU = "ABOUT";
    private static final String MAP_MENU = "MAP";
    private static final String DRIVERS_MENU = "DRIVERS";
    private static final String ITEMS_MENU = "ITEMS";
    private static final String SETTINGS_MENU = "SETTINGS";
    final TabSet leftTabSet = new TabSet();
    final Tab aboutTab = new Tab(prepareTabText(ABOUT_MENU));
    final Tab mapTab = new Tab(prepareTabText(MAP_MENU));
    final Tab usersTab = new Tab(prepareTabText(DRIVERS_MENU));
    final Tab itemsTab = new Tab(prepareTabText(ITEMS_MENU));
    final Tab settingsTab = new Tab(prepareTabText(SETTINGS_MENU));
    private MainView context = this;
    private Canvas baseLayout;

    private String prepareTabText(String text) {
        return "<div class='" + text + "'></br><b style='font-size:12px; font-family: Arial, Verdana, sans-serif;'>" + text + "</b></div>";
    }

    @Override
    public Canvas getBaseLayout() {
        if (this.baseLayout == null) {
            final VLayout vLayout = new VLayout();

            leftTabSet.setTabBarPosition(Side.LEFT);
            leftTabSet.setWidth100();
            leftTabSet.setHeight100();
            leftTabSet.setTabBarThickness(100);
            leftTabSet.setShowTabPicker(false);
            leftTabSet.setPaneMargin(0);
            leftTabSet.setScrollerSrc(SCROLL_IMAGE_URL);
            leftTabSet.setUseSimpleTabs(true);

            setTab(aboutTab, TypeOfView.ABOUT_VIEW);
            setTab(mapTab, TypeOfView.MAP_VIEW);
            setTab(usersTab, TypeOfView.USER_VIEW);
            setTab(itemsTab, TypeOfView.ITEM_VIEW);
            setTab(settingsTab, TypeOfView.SETTINGS_VIEW);


            leftTabSet.addTab(aboutTab);
            leftTabSet.addTab(mapTab);
            leftTabSet.addTab(usersTab);
            leftTabSet.addTab(itemsTab);
            leftTabSet.addTab(settingsTab);

            leftTabSet.selectTab(mapTab);

            ((UserServiceAsync) GWT.create(UserService.class)).getLoggedUserDetails(new AsyncCallback<UserDTO>() {
                @Override
                public void onFailure(Throwable throwable) {
                }

                @Override
                public void onSuccess(UserDTO userDTO) {
                    if (userDTO.getUsername().equals(ADMIN_NAME)) {
                        leftTabSet.selectTab(settingsTab);
                        leftTabSet.removeTab(aboutTab);
                        leftTabSet.removeTab(mapTab);
                        leftTabSet.removeTab(usersTab);
                        leftTabSet.removeTab(itemsTab);
                    }
                }
            });

            vLayout.addMember(leftTabSet);
            this.baseLayout = vLayout;


        }

        return this.baseLayout;
    }

    private void setTab(final Tab tab, final TypeOfView typeOfView) {
        tab.addTabSelectedHandler(new TabSelectedHandler() {
            @Override
            public void onTabSelected(TabSelectedEvent tabSelectedEvent) {
                switch (typeOfView) {
                    case ABOUT_VIEW:
                        tab.setPane(new AboutView().getBaseLayout());
                        break;
                    case MAP_VIEW:
                        tab.setPane(new MapView().getBaseLayout());
                        break;
                    case USER_VIEW:
                        tab.setPane(new DriverView(context).getBaseLayout());
                        break;
                    case ITEM_VIEW:
                        tab.setPane(new ItemView(context).getBaseLayout());
                        break;
                    case SETTINGS_VIEW:
                        tab.setPane(new SettingsView().getBaseLayout());
                        break;
                    default:
                        break;
                }

            }
        });
        tab.addTabDeselectedHandler(new TabDeselectedHandler() {
            @Override
            public void onTabDeselected(TabDeselectedEvent tabDeselectedEvent) {
                tab.setPane(null);
            }
        });
    }

    public void zoomToMapPoint(Directions directions, long holderId) {
        MapView mapView = new MapView();
        mapView.zoomToPoint(directions);
        mapView.showDriverTrack(holderId);
        leftTabSet.selectTab(mapTab);
    }

    public void zoomToDriver(String driverName){
        leftTabSet.selectTab(mapTab);
        MapView mapView = new MapView();
        mapView.zoomToDriver(driverName);
    }

    public void resetItemView() {
        leftTabSet.selectTab(settingsTab);
        leftTabSet.selectTab(itemsTab);
    }

    private enum TypeOfView {
        ABOUT_VIEW, MAP_VIEW, USER_VIEW, ITEM_VIEW, SETTINGS_VIEW
    }
}
