package com.bluetill.gpstrack.client.map;

import org.gwtopenmaps.openlayers.client.Bounds;

/**
 * Spatial constants.
 */
public interface SpatialConstants {

    /**
     * EPSG:2180
     * DATABASE PROJECTION.
     */
    String EPSG_2180 = "EPSG:2180";

    /**
     * Polands aproximate bounds.
     */
    Bounds POLAND_BOUNDS = new Bounds(144907.1658, 140544.7241, 877004.0070, 910679.6817);

    /**
     * Silesia aproximate bounds.
     */
    Bounds SILESIA_BOUNDS = new Bounds(416288.00, 151911.26, 600412.00, 383551.00);
}
