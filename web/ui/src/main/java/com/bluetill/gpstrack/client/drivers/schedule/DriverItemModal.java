package com.bluetill.gpstrack.client.drivers.schedule;


import com.bluetill.gpstrack.client.GeocoderService;
import com.bluetill.gpstrack.client.GeocoderServiceAsync;
import com.bluetill.gpstrack.client.Item_ListService;
import com.bluetill.gpstrack.client.Item_ListServiceAsync;
import com.bluetill.gpstrack.client.base.AskModal;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.drivers.schedule.utils.DriverNotifier;
import com.bluetill.gpstrack.client.drivers.schedule.utils.WaitingModal;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

import java.util.Date;
import java.util.List;

public class DriverItemModal extends ModalWindow implements BaseList {

    private static final String MODAL_TITLE = "Item information";
    private static final int MODAL_WIDTH = 500;
    private static final int MODAL_HEIGHT = 300;
    private static final int MODAL_MARGIN = 20;

    private DateItem itemListDateItem = new DateItem("shift");
    private TimeItem itemListTimeItem = new TimeItem();
    private TextItem orderText = new TextItem("order");
    private TextItem destinationInputText = new TextItem("destination");
    private TextItem destinationText = new TextItem("destination_value");
    protected final TextItem approvePointTextItem = new TextItem();
    private TextItem barcodeText = new TextItem("invoice_no");
    private TextAreaItem descriptionText = new TextAreaItem("description");

    private DriverItemListModal contextModal;
    private List<ItemDTO> itemDTOs;
    private ItemDTO currentItem;

    public DriverItemModal(final DriverItemListModal contextModal, final List<ItemDTO> itemDTOs, Long itemId) {
        super();
        this.setTitle(MODAL_TITLE);
        this.setWidth(MODAL_WIDTH);
        this.setHeight(MODAL_HEIGHT);
        this.setMembersMargin(MODAL_MARGIN);

        this.itemDTOs = itemDTOs;
        this.contextModal = contextModal;

        for (ItemDTO dto : itemDTOs) {
            if (dto.getId().equals(itemId)) this.currentItem = dto;
        }

        initForm();
    }

    private void initForm() {
        DynamicForm itemListForm = new DynamicForm();
        itemListForm.setNumCols(4);
        itemListForm.setHeight100();

        Date itemListDate = new Date();
        itemListDate.setHours(8);
        itemListDate.setMinutes(0);
        itemListDate.setSeconds(0);
        if (currentItem.getItemList() != null) itemListDate = new Date(currentItem.getItemList());

        itemListDateItem.setTitle("Shift");
        itemListTimeItem.setShowTitle(false);
        orderText.setTitle("Order");
        destinationInputText.setTitle("Destination");
        destinationText.setShowTitle(false);
        approvePointTextItem.setShowTitle(false);
        barcodeText.setTitle("Invoice no");
        descriptionText.setTitle("Description");

        if(currentItem.getStatus() != 0){
            itemListDateItem.setDisabled(true);
            itemListTimeItem.setDisabled(true);
            orderText.setVisible(false);
            destinationInputText.setDisabled(true);
            destinationText.setVisible(false);

        }

        itemListDateItem.setDefaultValue(itemListDate);
        itemListTimeItem.setDefaultValue(itemListDate);

        if (currentItem.getItemListOrder() != null) orderText.setValue(currentItem.getItemListOrder());

        destinationText.setShowTitle(false);
        destinationText.setDisabled(true);

        destinationInputText.setValue(currentItem.getDestination());
        destinationInputText.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent changedEvent) {
                resolveDestination();
            }
        });


        approvePointTextItem.setVisible(false);

        barcodeText.setValue(currentItem.getBarcode());
        barcodeText.setDisabled(true);

        descriptionText.setValue(currentItem.getDescription());

        itemListForm.setFields(
                itemListDateItem, itemListTimeItem,
                orderText, new SpacerItem(),
                destinationInputText, destinationText,
                approvePointTextItem, new SpacerItem(),
                descriptionText, new SpacerItem(),
                barcodeText, new SpacerItem()
        );


        addItem(itemListForm);
        addItem(new DriverItemMenu(DriverItemModal.this) {
            @Override
            protected void saveAction() {

                final WaitingModal modal = new WaitingModal();
                modal.show();


                Date date = itemListDateItem.getValueAsDate();
                date.setHours(((Date) itemListTimeItem.getValue()).getHours());
                date.setMinutes(((Date) itemListTimeItem.getValue()).getMinutes());
                date.setSeconds(((Date) itemListTimeItem.getValue()).getSeconds());

                final long oldItemListKey = currentItem.getItemList() != null ? currentItem.getItemList() : 0;
                final long newItemListKey = date != null ? (date.getTime()/60000)* 60000 : oldItemListKey;
                final long oldListOrder = currentItem.getItemListOrder() != null ? currentItem.getItemListOrder() : 999;
                final long newListOrder = orderText.getValueAsString() != null ? Long.parseLong(orderText.getValueAsString()) : 999;

                if(!currentItem.getDestination().equals(destinationText.getValueAsString()) || !currentItem.getApprovePoint().equals(approvePointTextItem.getValueAsString()) || oldItemListKey != newItemListKey || oldListOrder != newListOrder){
                    currentItem.setDestination(destinationText.getValueAsString());
                    currentItem.setApprovePoint(approvePointTextItem.getValueAsString());

                    if(date.getTime() != currentItem.getItemList()){
                        new AskModal("Change Date", "Do you want to change the time of all the items in this list?") {
                            @Override
                            protected void yesAction() {
                                changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, true, modal);
                            }

                            @Override
                            protected void noAction() {
                                changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, false, modal);
                            }

                            @Override
                            protected void closeAction() {
                                modal.destroy();
                            }
                        }.show();
                    } else {
                        changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, false, modal);
                    }
                    new DriverNotifier(currentItem.getHolderId()).notifyChanges();
                }
            }
        });
        resolveDestination();
    }

    private void changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, boolean changeAllTime, final WaitingModal modal){
        ((Item_ListServiceAsync) GWT.create(Item_ListService.class)).changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, changeAllTime, new AsyncCallback<ItemDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ItemDTO itemDTO) {
                DriverItemModal.this.destroy();
                contextModal.reload();
                modal.hide();
            }
        });
    }

    private void resolveDestination(){
        ((GeocoderServiceAsync) GWT.create(GeocoderService.class)).geocodeAddress(destinationInputText.getValueAsString(), new AsyncCallback<String[]>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(String[] locationDetails) {
                if(locationDetails != null && locationDetails.length > 1){
                    approvePointTextItem.setValue(locationDetails[0]);
                    destinationText.setValue(locationDetails[1]);
                }

            }
        });
    }
}