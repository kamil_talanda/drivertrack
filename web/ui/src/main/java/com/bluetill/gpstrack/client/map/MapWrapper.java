package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.footer.FooterPresenter;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.events.ResizedEvent;
import com.smartgwt.client.widgets.events.ResizedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.MapWidget;

/**
 * Smart gwt openlayers wrapper.
 */
public class MapWrapper extends HLayout {

    private final MapWidget map;

    public MapWrapper(final MapWidget map) {
        this.map = map;
        this.map.setVisible(true);
        this.setBorder("0");
        setBackgroundColor("black");
        setOverflow(Overflow.VISIBLE);
        setPadding(0);
        setMargin(0);
        setMembersMargin(0);
        addResizedHandler(new ResizedHandler() {
            @Override
            public void onResized(final ResizedEvent resizedEvent) {
                //map.setWidth(MapWrapper.this.getWidthAsString());
                map.setHeight(MapWrapper.this.getHeightAsString());
            }
        });

        this.setJsMap(map.getMap().getJSObject());
        this.addMember(map);
    }

    @Override
    public Integer getHeight() {
        final int footerHeight = ((FooterPresenter) Website.getPresenter(Component.FOOTER)).getComponent().getOffsetHeight();
        return Window.getClientHeight() - footerHeight - 12;
    }

    /**
     * Retrieves the map component.
     *
     * @return map component.
     */
    public Map getMap() {
        return this.map.getMap();
    }

    /**
     * Debuggin purposes.
     */
    private native void setJsMap(final JavaScriptObject object) /*-{
        $wnd.mapWidget = object;
    }-*/;
}