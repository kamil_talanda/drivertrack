package com.bluetill.gpstrack.client.footer;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;

import java.util.Date;

/**
 * Website's footer.
 */
public class Footer extends HLayout {

    private static final Footer10nBundle BUNDLE = GWT.create(Footer10nBundle.class);

    private static final String CSS = "footer";
    private static final String CSS_CONTENT = "footerContent";
    private static final String WIDTH = "100%";

    // Footer content widget.
    private final Label content = new Label();

    /**
     * Creates footer component instance.
     */
    public Footer() {
        // set style
        this.setAlign(Alignment.CENTER);
        this.setWidth(WIDTH);
        this.setStyleName(CSS);

        this.content.setWrap(false);
        this.content.setContents(createText());
        this.content.setStyleName(CSS_CONTENT);
        this.addMember(this.content);

        final Date date = new Date();
        date.setTime(0);
    }

    private static String createText() {
        String text = new String();
        text += BUNDLE.appName();
        text += BUNDLE.version();
        text += BUNDLE.separator();
        text += BUNDLE.copyright();
        return text;
    }
}
