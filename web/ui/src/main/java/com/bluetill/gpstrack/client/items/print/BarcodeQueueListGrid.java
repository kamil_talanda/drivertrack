package com.bluetill.gpstrack.client.items.print;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.validator.MaskValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/05/14
 * Time: 10:00

 */
public class BarcodeQueueListGrid extends ListGrid implements ItemVars, BaseList {

    public static ArrayList<ItemDTO> barcodeQueue = new ArrayList<ItemDTO>();
    private final BarcodeQueueListGrid instance = this;

    private final ListGridField qtyField = new ListGridField(ItemRecord.QTY, BUNDLE.qtyColumnName(), VERY_SMALL_FIELD_WIDTH);
    private final ListGridField barcodeField = new ListGridField(ItemRecord.BARCODE, BUNDLE.barcodeColumnName());
    private final ListGridField removeField = setUpButtonField(REMOVE_IMAGE_URL, REMOVE);

    public BarcodeQueueListGrid() {

        qtyField.setAlign(Alignment.CENTER);
        MaskValidator validator = new MaskValidator();
        validator.setMask("[0-9.]");
        qtyField.setValidators(validator);
        qtyField.setValidateOnChange(true);

        removeField.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(RecordClickEvent recordClickEvent) {
                instance.removeData(recordClickEvent.getRecord());
                barcodeQueue.remove(instance.getRecordIndex(recordClickEvent.getRecord()));
                BarcodeQueueListGrid.reloadBarcodeQueueNumberList();
            }
        });


        this.setWidth100();
        this.setHeight100();
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setBorder(TABLE_BORDER);
        this.setAutoFetchData(true);
        this.setLeaveScrollbarGap(false);

        this.setDataSource(new BarcodeQueueDataSource(barcodeQueue));
//        this.setCanEdit(true);

        this.setFields(
                qtyField,
                barcodeField,
                removeField
        );
    }

    private ListGridField setUpButtonField(String iconURL, String fieldName) {
        ListGridField listGridField = new ListGridField(fieldName, BUTTON_SIZE);
        listGridField.setType(ListGridFieldType.ICON);
        listGridField.setCellIcon(iconURL);
        listGridField.setCanEdit(false);
        listGridField.setCanFilter(true);
        listGridField.setFilterEditorType(new SpacerItem());
        listGridField.setCanGroupBy(false);
        listGridField.setCanSort(false);
        return listGridField;
    }

    public static void reloadBarcodeQueueNumberList(){
        ItemList.getItemListFooter().reloadBarcodeQueueNumber();
    }

    public static int getTotalBarcodeNumberInQueue() {
        int result = 0;
        for (ItemDTO itemDTO : BarcodeQueueListGrid.barcodeQueue) {
            result += (int) (long) itemDTO.getQty();
        }
        return result;
    }
}
