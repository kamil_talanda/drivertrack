package com.bluetill.gpstrack.client.container;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;

import com.google.gwt.user.client.Window;
import com.smartgwt.client.widgets.Canvas;

/**
 * Main view presenter.
 */
public class ContainerPresenter extends Presenter<Container> {
    
    // Currently displayed widget instance.
    private ViewType currentViewType;
    
    /**
     * Initializes container.
     */
    public ContainerPresenter() {
        super(new Container(), Component.CONTAINER);
    }
    
    /**
     * Sets the current view a removes the previous one.
     * @param viewType - view type that should be displayed.
     */
    public void setCurrentView(final ViewType viewType) {
        if (this.currentViewType != null) {
            if (this.currentViewType.equals(viewType)) {
                return;
            } else if (ViewType.MAP.equals(viewType)) {
                Window.Location.reload();
            }
        }
        
        final Canvas layout = viewType.getLayout();
        
        if (this.currentViewType != null) {
            this.getComponent().removeMember(this.currentViewType.getLayout());
        }
        this.currentViewType = viewType;
        
        layout.setWidth100();
        layout.setHeight100();
        this.getComponent().addMember(layout);
    }
    
    /**
     * Returns the current view type.
     * @return the current view type.
     */
    public ViewType getCurrentView() {
        return this.currentViewType;
    }
}
