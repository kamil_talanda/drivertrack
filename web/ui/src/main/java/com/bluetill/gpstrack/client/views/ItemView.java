package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.container.View;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.bluetill.gpstrack.client.items.ItemUIUtils.dayToMilliSeconds;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/12/13
 * Time: 12:24

 */
public class ItemView implements View, ItemVars {

    private static final int MAIN_MARGIN = 20;

    final VLayout layout = new VLayout();
    final MainView context;

    public static Date startFilterDate = new Date(0);
    public static Date endFilterDate = new Date((new Date()).getTime()+dayToMilliSeconds(INIT_TIME_OFFSET));

    public static void initFilterDates(){
        startFilterDate = new Date((new Date()).getTime()-dayToMilliSeconds(INIT_TIME_OFFSET));
        endFilterDate = new Date((new Date()).getTime()+dayToMilliSeconds(INIT_TIME_OFFSET));
    }

    public static Criteria criteria = new Criteria();

    ItemView(MainView context){
        this.context = context;
        layout.setMargin(MAIN_MARGIN);
    }

    @Override
    public Canvas getBaseLayout() {
        final ItemsPresenter layerTreePresenter_new = (ItemsPresenter) Website.getPresenter(Component.TASKS);
        ((ItemsServiceAsync) GWT.create(ItemsService.class)).getAll(startFilterDate, endFilterDate, new AsyncCallback<List<ItemDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(final List<ItemDTO> itemDTOs) {
                ((DriverServiceAsync) GWT.create(DriverService.class)).getAll(new AsyncCallback<ArrayList<DriverDTO>>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(ArrayList<DriverDTO> driverDTOs) {
                        driverDTOs.add(new DriverDTO(-1l, 0l, BUNDLE.unassigned(), ""));
                        layout.addMember(layerTreePresenter_new.getItemsList(itemDTOs, driverDTOs));
                        layout.addMember(layerTreePresenter_new.getFooter());
                        layerTreePresenter_new.passContext(context);
                    }
                });

                }
            });
        return layout;
    }
}
