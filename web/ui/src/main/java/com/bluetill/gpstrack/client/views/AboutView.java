package com.bluetill.gpstrack.client.views;

import com.bluetill.gpstrack.client.about.About10nBundle;
import com.bluetill.gpstrack.client.container.View;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/12/13
 * Time: 12:01

 */
public class AboutView implements View {
    private static final About10nBundle BUNDLE = GWT.create(About10nBundle.class);

    private static final String MOBILE_MANUAL_URL = "drivertrack_online_manual/Drivertrack_for_Android__v1.31_.html";

    private static final String BLUETILL_LOGO =  "bluetill_logo.jpg";
    private static final int LOGO_WIDTH = 435;
    private static final int LOGO_HEIGHT = 100;
    private static final int MAIN_MARGIN = 30;
    private static final int LOGO_DESCRIPTION_DISTANCE = 10;
    private static final int DESCRIPTION_MOBILE_DISTANCE = 30;
    private static final int MANUAL_BUTTON_WIDTH = 170;

    private static final String DESCRIPTION_CLASS_CSS = "aboutDescriptionText";

    private final Label spacer = new Label("");

    @Override
    public Canvas getBaseLayout() {
        final VLayout layout = new VLayout();
        layout.setWidth100();
        layout.setHeight100();
        layout.setMargin(MAIN_MARGIN);

        Img bluetillLogo = new Img(BLUETILL_LOGO);
        bluetillLogo.setWidth(LOGO_WIDTH);
        bluetillLogo.setHeight(LOGO_HEIGHT);
        bluetillLogo.setLayoutAlign(Alignment.CENTER);
        layout.addMember(bluetillLogo);

        spacer.setHeight(LOGO_DESCRIPTION_DISTANCE);
        layout.addMember(spacer);

        Label description = new Label(BUNDLE.description());
        description.setStyleName(DESCRIPTION_CLASS_CSS);
        layout.addMember(description);

        spacer.setHeight(DESCRIPTION_MOBILE_DISTANCE);
        layout.addMember(spacer);

        Label mobileApp = new Label(BUNDLE.googlePlayLink());
        mobileApp.setStyleName(DESCRIPTION_CLASS_CSS);
        layout.addMember(mobileApp);

        Button mobileManualButton = new Button(BUNDLE.mobileManualButton());
        mobileManualButton.setWidth(MANUAL_BUTTON_WIDTH);
        mobileManualButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                openNewWindow(BUNDLE.mobileManualTitle(), GWT.getHostPageBaseURL() + MOBILE_MANUAL_URL);
            }
        });

        layout.addMember(mobileManualButton);
        return layout;
    }

    public static void openNewWindow(String name, String url) {
        com.google.gwt.user.client.Window.open(url, name.replace(" ", "_"),
                "menubar=no," +
                        "location=false," +
                        "resizable=yes," +
                        "scrollbars=yes," +
                        "status=no," +
                        "dependent=true");
    }
}
