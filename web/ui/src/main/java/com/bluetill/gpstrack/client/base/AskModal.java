package com.bluetill.gpstrack.client.base;

import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 22/12/14
 * Time: 07:57
 * To change this template use File | Settings | File Templates.
 */
public abstract class AskModal extends ModalWindow {
    private static final int MODAL_WIDTH = 400;
    private static final int MODAL_HEIGHT = 120;
    private static final int MODAL_MARGIN = 20;

    protected HLayout menuLayout = new HLayout();
    protected Button noButton = new Button("NO");
    protected Button yesButton = new Button("YES");
    protected final Label spacerHorizontal = new Label();
    protected final Label spacerVertical = new Label();
    protected Label content = new Label();

    public AskModal(String title, String contents) {
        this.setTitle(title);
        this.setWidth(MODAL_WIDTH);
        this.setHeight(MODAL_HEIGHT);
        this.setMembersMargin(MODAL_MARGIN);

        spacerHorizontal.setWidth("100%");
        spacerVertical.setHeight100();

        content.setContents(contents);
        content.setMargin(10);
        content.setTop(1);
        content.setHeight(30);

        this.setTop(1);

        this.addItem(content);
        this.addItem(spacerVertical);

        this.addCloseClickHandler(new CloseClickHandler() {
            @Override
            public void onCloseClick(CloseClickEvent closeClickEvent) {
                closeAction();
                AskModal.this.destroy();
            }
        });

        menuLayout.addMember(noButton);
        menuLayout.addMember(spacerHorizontal);
        menuLayout.addMember(yesButton);

        noButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                noAction();
                AskModal.this.destroy();
            }
        });

        yesButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                yesAction();
                AskModal.this.destroy();
            }
        });

        this.addItem(menuLayout);

    }

    protected abstract void yesAction();
    protected abstract void noAction();
    protected abstract void closeAction();
}
