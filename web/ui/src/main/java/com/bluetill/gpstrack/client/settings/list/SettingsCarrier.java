package com.bluetill.gpstrack.client.settings.list;

import com.bluetill.gpstrack.client.CarriersService;
import com.bluetill.gpstrack.client.CarriersServiceAsync;
import com.bluetill.gpstrack.shared.dto.CarriersDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 10:32

 */
public class SettingsCarrier extends SettingsList {

    public SettingsCarrier() {
        super();
        setAddButtonTitle(BUNDLE.addNewButtonCarrierTitle());
    }

    @Override
    protected void loadData() {
        ((CarriersServiceAsync) GWT.create(CarriersService.class)).getAll(new AsyncCallback<ArrayList<CarriersDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ArrayList<CarriersDTO> results) {
                ArrayList<SettingsRecord> tenantsList = new ArrayList<SettingsRecord>();
                for (CarriersDTO dto : results) {
                    tenantsList.add(new SettingsRecord(dto.getName()));
                }
                SettingsRecord[] tenantsArray = new SettingsRecord[tenantsList.size()];
                tenantsArray = tenantsList.toArray(tenantsArray);
                listGrid.setData(tenantsArray);
            }
        });
    }

    @Override
    protected void addNewRecordToDB(String name) {
        CarriersDTO dto = new CarriersDTO();
        dto.setName(name);
        ((CarriersServiceAsync) GWT.create(CarriersService.class)).set(dto, new AsyncCallback<CarriersDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(CarriersDTO result) {
            }
        });
    }

    @Override
    protected void removeRecordFromDB(String name) {
        ((CarriersServiceAsync) GWT.create(CarriersService.class)).deleteByName(name, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(Boolean aBoolean) {
            }
        });
    }

}
