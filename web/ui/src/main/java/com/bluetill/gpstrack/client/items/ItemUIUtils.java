package com.bluetill.gpstrack.client.items;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.items.print.BarcodeQueueListGrid;
import com.bluetill.gpstrack.shared.dto.CarriersDTO;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.bluetill.gpstrack.shared.dto.TenantsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.layout.VLayout;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 21/01/14
 * Time: 13:47

 */
public class ItemUIUtils implements ItemVars {

    private static LinkedHashMap<String, String> statusMap = createStatusSelectValueMap();
    private static LinkedHashMap<String, String> driversMap = new LinkedHashMap<String, String>();

    public static LinkedHashMap<String, String> getStatusMap() {
        statusMap = createStatusSelectValueMap();
        return statusMap;
    }

    public static void setCarriersMap(final SelectItem carriersList) {
        ((CarriersServiceAsync) GWT.create(CarriersService.class)).getAll(new AsyncCallback<ArrayList<CarriersDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ArrayList<CarriersDTO> result) {
                LinkedHashMap<String, String> carriersMap = new LinkedHashMap<String, String>();

                for (CarriersDTO dto : result) {
                    carriersMap.put(dto.getName(), dto.getName());
                }
                carriersList.setValueMap(carriersMap);
            }
        });
    }

    public static void setTenantMap(final SelectItem tenantsList) {
        ((TenantsServiceAsync) GWT.create(TenantsService.class)).getAll(new AsyncCallback<ArrayList<TenantsDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(ArrayList<TenantsDTO> result) {
                LinkedHashMap<String, String> tenantMap = new LinkedHashMap<String, String>();

                for (TenantsDTO dto : result) {
                    tenantMap.put(dto.getName(), dto.getName());
                }
                tenantsList.setValueMap(tenantMap);
            }
        });
    }

    public static LinkedHashMap<String, String> getDriversMap() {
        initDriversMap();
        return driversMap;
    }

    public static void setDriversMap(final SelectItem driversList) {
        ((MapServiceAsync) GWT.create(MapService.class)).getAll(new AsyncCallback<List<DriversPathDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<DriversPathDTO> result) {
                LinkedHashMap<String, String> driversMap = new LinkedHashMap<String, String>();
                driversMap.put("-1", NO_DRIVER);
                for (DriversPathDTO driversPathDTO : result) {
                    driversMap.put(Long.toString(driversPathDTO.getId()), driversPathDTO.getName());
                }
                driversList.setValueMap(driversMap);
            }
        });
    }

    public static long dayToMilliSeconds(long day) {
        return day * 24 * 60 * 60 * 1000;
    }

    public static LinkedHashMap<String, String> createStatusSelectValueMap() {
        LinkedHashMap<String, String> statusMap = new LinkedHashMap<String, String>();
        statusMap.put("0", BUNDLE.statusInProgress());
        statusMap.put("1", BUNDLE.statusDone());
        return statusMap;
    }

    public static String generateBarcode(String companySymbol, int number) {
        final StringBuffer stringBuffer = new StringBuffer();
        //stringBuffer.append(companySymbol);
        stringBuffer.append((DateTimeFormat.getFormat(BARCODE_DATE_FORMAT)).format(new Date()));
        stringBuffer.append(getItemsNumber3DigitString(number));
        //stringBuffer.append(String.valueOf(getChecksum(stringBuffer)));
        return stringBuffer.toString();
    }

    public static Img getBarcodeImage(String barcodeBase64, int barcodeWidth, int barcodeHeight) {
        Img barcodeImagePrint = new Img();
        barcodeImagePrint.setSrc(IMAGE_FORMAT + barcodeBase64);
        barcodeImagePrint.setWidth(barcodeWidth);
        barcodeImagePrint.setHeight(barcodeHeight);
        return barcodeImagePrint;
    }

    public static Long getStatusLong(String statusString) {
        for (String key : ItemUIUtils.statusMap.keySet()) {
            if (ItemUIUtils.statusMap.get(key).equals(statusString)) {
                return Long.parseLong(key);

            }
        }
        return null;
    }

    public static void initDriversMap() {
        ((MapServiceAsync) GWT.create(MapService.class)).getAll(new AsyncCallback<List<DriversPathDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<DriversPathDTO> result) {

                driversMap.put(Long.toString(EMPTY_HOLDER_ID), BUNDLE.unassigned());
                for (DriversPathDTO driversPathDTO : result) {
                    driversMap.put(Long.toString(driversPathDTO.getId()), driversPathDTO.getName());
                }
            }
        });
    }

    public static VLayout generateBarcodePrintSet(HashMap<String, Img> barcodeMap) {
        VLayout printContainer = new VLayout();
        Label smallSeparator = new Label();
        String[] barcodeArray = barcodeMap.keySet().toArray(new String[barcodeMap.keySet().size()]);
        Arrays.sort(barcodeArray);
        int j = 0;
        for (String text : barcodeArray) {
            addBarcodeToPrintContainer(printContainer, barcodeMap.get(text), text, barcodeMap.size() - 1, j);
            j++;
        }
        return printContainer;
    }

    public static VLayout generateBarcodePrintSet(HashMap<String, Img> barcodeMap, int repeat) {
        VLayout printContainer = new VLayout();
        Label smallSeparator = new Label();
        for (int j = 0; j < repeat; j++) {
            for (String text : barcodeMap.keySet()) {
                addBarcodeToPrintContainer(printContainer, barcodeMap.get(text), text, repeat, j);
            }
        }
        return printContainer;
    }

    public static void printBarcodeList(final ArrayList<ItemDTO> itemDTOArrayList){
        final VLayout printLayout = new VLayout();
        int itemCounter = 0;
        for (final ItemDTO itemDTO : itemDTOArrayList) {
            itemCounter++;
            final int finalItemCounter = itemCounter;
            ItemBarcode itemBarcode = new ItemBarcode(itemDTO.getBarcode()) {
                @Override
                public void barcodeTextAction() {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                @Override
                public void barcodeImageAction() {
                    addBarcodeToPrintContainerQueue(printLayout, barcodeImage, barcodeText, (int) (long) itemDTO.getQty(), finalItemCounter);
                    if (finalItemCounter == itemDTOArrayList.size())
                        Canvas.printComponents(new Object[]{printLayout});
                }
            };
            itemBarcode.generateFullBarcode(1, PRINT_BARCODE_WIDTH, PRINT_BARCODE_HEIGHT);
        }

    }

    public static void addBarcodeToPrintContainer(VLayout printContainer, Img barcodeImage, String text, int barcodeNumber, int j) {
        Label smallSeparator = new Label();
        if (j != 0) printContainer.addMember(smallSeparator);
//                printContainer.addMember(smallSeparator);
        printContainer.addMember(barcodeImage);
        printContainer.addMember(getBarcodeLabelPrint(text));
        if (j != barcodeNumber - 1) {
            HTMLPane pageBreak = new HTMLPane();
            pageBreak.setContents("<div style='page-break-after:always'></div>");
            printContainer.addMember(pageBreak);
        }
    }

    public static void addBarcodeToPrintContainerQueue(VLayout printContainer, Img barcodeImage, String text, int repeat, int j) {

        for (int i = 0; i < repeat; i++) {
            Label smallSeparator = new Label();
            if (i != 0 || j != 1) printContainer.addMember(smallSeparator);
            printContainer.addMember(barcodeImage);
            printContainer.addMember(getBarcodeLabelPrint(text));

            if (i != repeat - 1 || j != BarcodeQueueListGrid.barcodeQueue.size()) {
                HTMLPane pageBreak = new HTMLPane();
                pageBreak.setContents("<div style='page-break-after:always'></div>");
                printContainer.addMember(pageBreak);
            }
        }

    }

    private static Label getBarcodeLabelPrint(String barcodeText) {
        Label barcodeLabel = new Label();
        barcodeLabel.setContents(barcodeText);
        barcodeLabel.setStyleName("barcodeText");
        barcodeLabel.setHeight(5);
        return barcodeLabel;
    }

    private static String getItemsNumber3DigitString(int itemsNumber) {
        String curItemsNumber = String.valueOf((itemsNumber + 1) % 1000);
        switch (curItemsNumber.length()) {
            case 0:
                curItemsNumber = "000";
                break;
            case 1:
                curItemsNumber = "00" + curItemsNumber;
                break;
            case 2:
                curItemsNumber = "0" + curItemsNumber;
                break;
            default:
                break;
        }
        return curItemsNumber;
    }

    private static int getChecksum(StringBuffer stringBuffer) {
        //checksum - (sum of all the chars in ascii) * 7 % 10
        int sum = 0;
        for (char curChar : stringBuffer.toString().toCharArray()) {
            sum += curChar;
        }
        return (sum * 7) % 10;
    }

    public static StringBuilder exportCSV(List<ItemDTO> items) {
        ArrayList<ArrayList<String>> dataToExport = new ArrayList<ArrayList<String>>();
        dataToExport.add(getItemFields());
        for (ItemDTO itemDTO : items) {
            dataToExport.add(getItemDetails(itemDTO));
        }

        StringBuilder stringBuilder = new StringBuilder(); // csv data in here
        for (ArrayList<String> currentData : dataToExport) {
            for (String field : currentData) {
                stringBuilder.append(field);
                stringBuilder.append(", ");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1); // remove last
            // ","
            stringBuilder.append(START_NEW_LINE);
        }

        return stringBuilder;
    }

    public static ArrayList<String> getItemFields() {
        ArrayList<String> itemFieldNames = new ArrayList<String>();
        itemFieldNames.add(BUNDLE.barcodeTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.carrierTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.tenantTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.qtyTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.receiverTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.destinationTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.addDateTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.approveDateTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.holderTitle().replace(" ", "_"));
        itemFieldNames.add(BUNDLE.statusTitle().replace(" ", "_"));
        return itemFieldNames;
    }

    private static ArrayList<String> getItemDetails(ItemDTO itemDTO) {
        ArrayList<String> itemFieldNames = new ArrayList<String>();
        if (itemDTO.getBarcode() != null) itemFieldNames.add(itemDTO.getBarcode().replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getCarrier() != null) itemFieldNames.add(itemDTO.getCarrier().replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getTenant() != null) itemFieldNames.add(itemDTO.getTenant().replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getQty() != null) itemFieldNames.add(Long.toString(itemDTO.getQty()).replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getName() != null) itemFieldNames.add(itemDTO.getName().replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getDestination() != null) itemFieldNames.add(itemDTO.getDestination().replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getAddTime() != null)
            itemFieldNames.add(DateTimeFormat.getFormat("dd/MM/yyy").format(new Timestamp(itemDTO.getAddTime())).replace(" ", "_"));
        else itemFieldNames.add("");
        if (itemDTO.getApproveTime() != null)
            itemFieldNames.add(DateTimeFormat.getFormat("dd/MM/yyy").format(new Timestamp(itemDTO.getApproveTime())).replace(" ", "_"));
        else itemFieldNames.add("");
        //TODO holder id to name
        if (itemDTO.getHolderId() != null) itemFieldNames.add(Long.toString(itemDTO.getHolderId()));
        else itemFieldNames.add("");
        if (itemDTO.getStatus() == STATUS_IN_PROGRESS) itemFieldNames.add(BUNDLE.statusInProgress().replace(" ", "_"));
        else if (itemDTO.getStatus() == STATUS_DONE) itemFieldNames.add(BUNDLE.statusDone().replace(" ", "_"));
        else itemFieldNames.add("");
        return itemFieldNames;
    }


}
