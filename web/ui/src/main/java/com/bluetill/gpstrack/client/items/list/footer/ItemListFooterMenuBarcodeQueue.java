package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.bluetill.gpstrack.client.items.print.BarcodeQueueListGrid;
import com.bluetill.gpstrack.client.items.print.BarcodeQueueWindow;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/01/14
 * Time: 09:51

 */
public class ItemListFooterMenuBarcodeQueue extends ToolStrip implements ItemVars {

    private static final int INPUT_NUMBER_OF_BARCODE_WIDTH = 30;
    private final ToolStripButton showBarcodeQueueWindowButton = new ToolStripButton();
    private final ToolStripButton printBarcodeQueueButton = new ToolStripButton();
    private ItemList context;

    public ItemListFooterMenuBarcodeQueue(final ItemList context) {
        this.context = context;

        showBarcodeQueueWindowButton.setTitle(BUNDLE.showBarcodeQueue());
        this.addShowBarcodeQueueWindowButtonClickHandler();
        this.addButton(showBarcodeQueueWindowButton);

        reloadBarcodeQueueNumber();
        this.addPrintBarcodeQueueClickHandler();
        this.addButton(printBarcodeQueueButton);
    }

    public void reloadBarcodeQueueNumber() {
        printBarcodeQueueButton.setTitle(BUNDLE.printBarcodeQueue() + "(" + BarcodeQueueListGrid.getTotalBarcodeNumberInQueue() + ")");
    }

    private int getBarcodeQueueNumber() {
        int result = 0;
        for (ItemDTO itemDTO : BarcodeQueueListGrid.barcodeQueue) {
            result += itemDTO.getQty();
        }
        return result;
    }

    private void addPrintBarcodeQueueClickHandler() {
        printBarcodeQueueButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ItemUIUtils.printBarcodeList(BarcodeQueueListGrid.barcodeQueue);
            }
        });
    }



    private void addShowBarcodeQueueWindowButtonClickHandler() {
        showBarcodeQueueWindowButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                new BarcodeQueueWindow();
            }
        });
    }


}
