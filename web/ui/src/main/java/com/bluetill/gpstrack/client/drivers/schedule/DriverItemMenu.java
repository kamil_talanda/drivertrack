package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.drivers.DriverVars;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:40

 */
public abstract class DriverItemMenu extends HLayout implements DriverVars {

    private DriverItemModal context;

    protected final Button saveButton = new Button();
    protected final Button closeButton = new Button();
    protected final Label spacer = new Label();

    public DriverItemMenu(final DriverItemModal context){
        this.context = context;

        spacer.setWidth("100%");

        this.addMember(saveButton);
        this.addMember(spacer);
        this.addMember(closeButton);

        closeButton.setTitle(BUNDLE.closeButtonText());
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                context.destroy();
            }
        });

        saveButton.setTitle(BUNDLE.saveButtonText());
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                saveAction();
            }
        });

    }

    protected abstract void saveAction();
}
