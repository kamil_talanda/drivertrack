package com.bluetill.gpstrack.client;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/05/14
 * Time: 08:10

 */
public interface GlobalVars {
    String ADMIN_NAME = "admin";
    int MAIN_MARGIN = 20;

    String projectionType = "EPSG:4326";
    int pointZoomLevel = 14;
}
