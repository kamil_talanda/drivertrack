package com.bluetill.gpstrack.client.drivers;

import com.google.gwt.core.client.GWT;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/01/14
 * Time: 07:05

 */
public interface DriverVars {

    DriverList10nBundle BUNDLE = GWT.create(DriverList10nBundle.class);

    String START_NEW_LINE = "_START_NEW_LINE_ITEM_";

}
