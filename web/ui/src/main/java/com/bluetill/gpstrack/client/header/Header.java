package com.bluetill.gpstrack.client.header;

import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.client.UserServiceAsync;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.*;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/03/14
 * Time: 09:14

 */
public class Header extends HLayout {

    private static final String LOGOUT_PAGE_URL = "j_spring_security_logout";
    private static final int HEADER_HEIGHT = 70;
    private static final int LOGO_SIZE = 60;
    private static final int LOGOUT_HEIGHT = 30;
    private static final String LOGOUT_CLASS_CSS = "logoutButton";
    private static final String LOGOUT_OVER_CLASS_CSS = "logoutButtonOver";
    private static final String HEADER_CLASS_CSS = "titleBar";
    private static final String LOGO_IMAGE_CLASS_CSS = "logoImage";
    private static final String LOGO_TEXT_CLASS_CSS = "logoText";
    private static final String CUSTOMER_NAME_TEXT_CLASS_CSS = "customerNameText";
    private static final String LOGO_IMAGE = "icon.png";
    private static final String LOGO_TEXT = "drivertrack_logo_text.png";

    final Img logoImage = new Img(LOGO_IMAGE);
    final Img logoText = new Img(LOGO_TEXT);
    final Label logout = new Label(setUpLogoutButton(LOGOUT_CLASS_CSS));
    final Label customerName = new Label();
    final Label spacer = new Label();


    public Header() {
        this.setWidth100();
        this.setHeight(HEADER_HEIGHT);
        this.setStyleName(HEADER_CLASS_CSS);

        logoImage.setStyleName(LOGO_IMAGE_CLASS_CSS);
        logoImage.setWidth(LOGO_SIZE);
        logoImage.setHeight(LOGO_SIZE);
        logoImage.setLayoutAlign(VerticalAlignment.CENTER);
        this.addMember(logoImage);

        logoText.setStyleName(LOGO_TEXT_CLASS_CSS);
        logoText.setWidth(211);
        logoText.setHeight(45);
        logoText.setLayoutAlign(VerticalAlignment.CENTER);
        this.addMember(logoText);

        customerName.setStyleName(CUSTOMER_NAME_TEXT_CLASS_CSS);
        customerName.setLayoutAlign(VerticalAlignment.CENTER);

        ((UserServiceAsync) GWT.create(UserService.class)).getLoggedUserDetails(new AsyncCallback<UserDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(UserDTO userDTO) {
                customerName.setContents("•" + "&nbsp&nbsp&nbsp" + userDTO.getFull_name());
            }
        });
        this.addMember(customerName);

        spacer.setWidth100();
        spacer.setHeight(HEADER_HEIGHT);
        this.addMember(spacer);

        logout.setHeight(LOGOUT_HEIGHT);
        logout.setLayoutAlign(VerticalAlignment.CENTER);
        logout.setCursor(Cursor.POINTER);

        logout.addMouseOverHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent mouseOverEvent) {
                logout.setContents(setUpLogoutButton(LOGOUT_OVER_CLASS_CSS));
            }
        });
        logout.addMouseOutHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent mouseOutEvent) {
                logout.setContents(setUpLogoutButton(LOGOUT_CLASS_CSS));
            }
        });

        logout.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                Window.Location.assign(GWT.getHostPageBaseURL() + LOGOUT_PAGE_URL);
            }
        });

        this.addMember(logout);
    }

    private String setUpLogoutButton(String classCSS) {
        return "<div class='" + classCSS + "' style='font-size:25px'><b style='font-size:16px; font-family: Arial, Verdana, sans-serif;'>&nbspLOGOUT</b></div>";
    }
}
