package com.bluetill.gpstrack.client.container;

import com.bluetill.gpstrack.client.views.MainView;

import com.smartgwt.client.widgets.Canvas;

/**
 * View type enum.
 */
public enum ViewType {

    /**
     * Map view.
     */
    MAP(new MainView());
    
    private transient View view;
    
    private ViewType(final View view) {
        this.view = view;
    }
    
    /**
     * Returns base layout associated with this enum value.
     * @return base layout associated with this enum value.
     */
    public Canvas getLayout() {
        return this.view.getBaseLayout();
    }
}
