package com.bluetill.gpstrack.client.items.add;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:40

 */
public class ItemAddMenu extends HLayout implements ItemVars {

    private ItemAdd context;

    protected final Button addNewButton = new Button();
    protected final Button printButton = new Button();
    protected final Button saveButton = new Button();
    protected final Button closeButton = new Button();
    protected final Label spacer = new Label();

    public ItemAddMenu(final ItemAdd context){
        this.context = context;

        spacer.setWidth("100%");
        this.addMember(addNewButton);
        this.addMember(printButton);
        this.addMember(spacer);
        this.addMember(saveButton);
        this.addMember(closeButton);

        addNewButton.setTitle(BUNDLE.addItemButtonText());
        addNewButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent clickEvent) {
                Record record = new Record();
                record.setAttribute(ItemRecord.QTY, 1);
                context.getItemListGrid().startEditingNew(record);
            }
        });

        printButton.setTitle(BUNDLE.printButtonText());
        printButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                printBarcode(getItemQty());
            }
        });

        saveButton.setTitle(BUNDLE.saveButtonText());
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                final int itemNo = context.getDetails().size();
                final int itemQty = getItemQty();

                boolean isLast = false;
                int i = 0;
                for (ItemDTO itemDTO : context.getDetails()) {
                    i++;
                    if (i == itemNo) isLast = true;
                    saveRecordToDB(new ItemRecord(itemDTO), isLast);
                }
                SC.ask(BUNDLE.enteredText() + " " + i + BUNDLE.newDeliveryItemText() + " " + context.barcodeLocal + ". " + BUNDLE.printBarcodeQuestion(), new BooleanCallback() {
                    @Override
                    public void execute(Boolean result) {
                        if (result) {
                            printBarcode(itemQty);
                        }
                    }
                });
                context.destroy();

            }

        });

        closeButton.setTitle(BUNDLE.closeButtonText());
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                context.destroy();
            }
        });
    }

    private void printBarcode(int itemQty){
        if (itemQty >0) {
            HashMap<String, Img> barcodeMap = new HashMap<String, Img>();
            barcodeMap.put(context.barcodeLocal, context.barcodeImageLocal);
            Canvas.printComponents(new Object[]{ItemUIUtils.generateBarcodePrintSet(barcodeMap, itemQty)});
        } else {
            SC.say(BUNDLE.emptyListAlertText());
        }
    }

    private int getItemQty(){
        long itemQty = 0;
        for(ItemDTO itemDTO : context.getDetails()){
            itemQty = itemQty + itemDTO.getQty();
        }
        return (int) itemQty;
    }

    private void saveRecordToDB(final ItemRecord itemRecord, final boolean isLast) {
        ((DriverServiceAsync) GWT.create(DriverService.class)).getByName(itemRecord.getAttribute(HOLDER_NAME), new AsyncCallback<DriverDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(DriverDTO result) {
                if(result != null) itemRecord.setAttribute(HOLDER_ID, (long) result.getId());
                else itemRecord.setAttribute(HOLDER_ID, -1l);
                ((ItemsServiceAsync) com.google.gwt.core.client.GWT.create(ItemsService.class)).set(itemRecord.convertToItemDTO(), new AsyncCallback<ItemDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(ItemDTO itemDTO) {
                        if (isLast) {
                            final ItemsPresenter itemsPresenter = (ItemsPresenter) Website.getPresenter(Component.TASKS);
                            itemsPresenter.resetItemView();
                        }

                    }
                });

            }
        });
    }
}
