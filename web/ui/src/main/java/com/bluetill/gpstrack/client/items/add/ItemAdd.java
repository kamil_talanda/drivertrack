package com.bluetill.gpstrack.client.items.add;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.items.ItemBarcode;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/02/14
 * Time: 13:14

 */
public class ItemAdd extends ModalWindow implements BaseList, ItemVars {

    private final DynamicForm mainForm = new DynamicForm();
    private final SelectItem carrierSelectItem = new SelectItem();
    private final SelectItem tenantSelectItem = new SelectItem();
    private final SelectItem driverSelectItem = new SelectItem();
    private final ItemAddListGrid itemListGrid = new ItemAddListGrid();
    private final ItemAddMenu menuForm = new ItemAddMenu(this);
    public String barcodeLocal;
    public Img barcodeImageLocal;

    public ItemAdd() {
        super();

        new ItemBarcode() {
            @Override
            public void barcodeTextAction() {
            }

            @Override
            public void barcodeImageAction() {
                barcodeLocal = barcodeText;
                barcodeImageLocal = barcodeImage;
            }
        }.generateFullBarcode(0, PRINT_BARCODE_WIDTH, PRINT_BARCODE_HEIGHT);

        this.setTitle(BUNDLE.newDeliveryItemTitle());
        this.setWidth(WINDOW_WIDTH);
        this.setHeight(WINDOW_HEIGHT_WITHOUT_SIGNATURE);
        this.setMembersMargin(DEFAULT_MEMBERS_MARGIN);
        this.initFields();
        this.addItem(mainForm);
        this.addItem(itemListGrid);
        this.addItem(menuForm);

        this.show();
    }

    public ItemAddListGrid getItemListGrid() {
        return itemListGrid;
    }

    private void initFields() {
        carrierSelectItem.setTitle(BUNDLE.carrierTitle());
        ItemUIUtils.setCarriersMap(carrierSelectItem);
        carrierSelectItem.setValue(NO_CARRIER);

        tenantSelectItem.setTitle(BUNDLE.tenantTitle());
        ItemUIUtils.setTenantMap(tenantSelectItem);
        tenantSelectItem.setValue(NO_TENANT);

        driverSelectItem.setTitle(BUNDLE.holderTitle());
        ItemUIUtils.setDriversMap(driverSelectItem);
        driverSelectItem.setValue(NO_DRIVER);
        mainForm.setNumCols(2);
        mainForm.setFields(

                carrierSelectItem,
                tenantSelectItem,
                driverSelectItem

        );
    }

    public ArrayList<ItemDTO> getDetails() {
        ArrayList<ItemDTO> itemDTOs = new ArrayList<ItemDTO>();

        for (Record record : itemListGrid.getRecords()) {
            ItemDTO itemDTO = null;
            itemDTO = new ItemDTO();
            itemDTO.setBarcode(barcodeLocal);
            itemDTO.setCarrier(carrierSelectItem.getValueAsString());
            itemDTO.setTenant(tenantSelectItem.getValueAsString());
            itemDTO.setQty(record.getAttribute(ItemRecord.QTY) != null ? Long.parseLong(record.getAttribute(ItemRecord.QTY)) : 1);
            itemDTO.setName(record.getAttribute(ItemRecord.NAME));
            itemDTO.setDestination(record.getAttribute(ItemRecord.DESTINATION));
//            itemDTO.setDescription(descriptionInputItem.getValueAsString());
            itemDTO.setAddTime(new Date().getTime());
            itemDTO.setHolderName(ItemUIUtils.getDriversMap().get(driverSelectItem.getValueAsString()));
            itemDTO.setStatus((long) 0);
            itemDTOs.add(itemDTO);
        }

        return itemDTOs;
    }
}