package com.bluetill.gpstrack.client.items.list;

import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.client.views.ItemView;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.RelativeDate;
import com.smartgwt.client.util.DateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 22/01/14
 * Time: 09:33

 */
public class ItemListFilter implements ItemVars {

    public static void processFilter(Criteria criteria) {
        ItemView.criteria = criteria;
//        ItemView.initFilterDates();
//        if (criteria.getJsObj() != null) {
//            Map map = criteria.getValues();
//
//            ArrayList<Map> criteriaArray = (ArrayList<Map>) map.get("criteria");
//            if (criteriaArray != null) {
//                if (criteriaArray.size() > 1) {
//                    processTimeFilter(criteriaArray);
//                } else {
//                    Map criteriaMap = criteriaArray.get(0);
//                    processTimeFilter((ArrayList<Map>) criteriaMap.get("criteria"));
//                }
//            }
//        }
    }

    private static void processTimeFilter(ArrayList<Map> criteriaArray) {
        Object from = null;
        Object to = null;
        for (Map criteriaMap : criteriaArray) {
            if (criteriaMap.get("fieldName").equals(ADD_TIME)) {
                if (criteriaMap.get("operator").equals("greaterOrEqual")) {
                    from = criteriaMap.get("value");
                } else {
                    to = criteriaMap.get("value");
                }
            }
        }
        if (from != null || to != null) {
            refreshView(getDateFromObject(from), getDateFromObject(to));
        }

    }

    private static void refreshView(Date fromDate, Date toDate) {
        if (!ItemView.endFilterDate.equals(toDate) || !ItemView.startFilterDate.equals(fromDate)) {
            if (fromDate != null) ItemView.startFilterDate = fromDate;
            if (toDate != null) ItemView.endFilterDate = toDate;
            ((ItemsPresenter) Website.getPresenter(Component.TASKS)).resetItemView();
        }
    }

    private static Date getDateFromObject(Object dateObject) {
        if (dateObject != null) {
            if (dateObject.getClass() == Date.class)
                return (Date) dateObject;
            else {
                return DateUtil.getAbsoluteDate(new RelativeDate((String) ((Map) dateObject).get("value")));
            }
        } else
            return null;
    }
}
