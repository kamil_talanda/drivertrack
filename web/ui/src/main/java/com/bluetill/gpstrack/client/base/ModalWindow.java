package com.bluetill.gpstrack.client.base;

import com.smartgwt.client.widgets.Window;

/**
 * Created with IntelliJ IDEA.
 * User: krzysztof.janota
 * Date: 03.12.13
 * Time: 11:12

 */
public class ModalWindow extends Window {

    public ModalWindow() {
        super();

        this.setShowMinimizeButton(false);
        this.setKeepInParentRect(true);
        this.setCanDragResize(false);
        this.setCanDrag(false);
        this.setAutoCenter(true);
        this.setIsModal(true);
        this.setShowModalMask(true);
        this.centerInPage();
    }
}
