package com.bluetill.gpstrack.client.footer;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;

/**
 * Footer Presenter.
 */
public class FooterPresenter extends Presenter<Footer> {
    
    /**
     * Creates a presenter associated with a new Footer class instance.
     */
    public FooterPresenter() {
        super(new Footer(), Component.FOOTER);        
    }
}
