package com.bluetill.gpstrack.client.settings;

import com.bluetill.gpstrack.client.GlobalVars;
import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.client.UserServiceAsync;
import com.bluetill.gpstrack.client.settings.admin.UserList;
import com.bluetill.gpstrack.client.settings.others.SettingsOfficeAddress;
import com.bluetill.gpstrack.client.settings.others.SettingsShift;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 08:44

 */
public class SettingsMain extends VLayout implements GlobalVars {

    private static final int MAIN_MARGIN = 20;
    private final Label spacer = new Label();

    public SettingsMain() {
        spacer.setHeight(10);
        this.setMargin(MAIN_MARGIN);
//        this.addMember(new SettingsCompanySymbol());
//        final SettingsEmailSignature emailSignature = new SettingsEmailSignature();
//        emailSignature.setHeight(70);
//        this.addMember(emailSignature);
        final SettingsOfficeAddress settingsOfficeAddress = new SettingsOfficeAddress();
        settingsOfficeAddress.setHeight(50);
        this.addMember(settingsOfficeAddress);
        final SettingsShift settingsShift = new SettingsShift();
        settingsShift.setHeight(50);
        this.addMember(settingsShift);
//        final SettingsTenant settingsTenant = new SettingsTenant();
//        this.addMember(settingsTenant);
//        this.addMember(spacer);
//        final SettingsCarrier settingsCarrier = new SettingsCarrier();
//        this.addMember(settingsCarrier);

        ((UserServiceAsync) GWT.create(UserService.class)).getLoggedUserDetails(new AsyncCallback<UserDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(UserDTO userDTO) {
                if (userDTO.getUsername().equals(ADMIN_NAME)) {
//                    SettingsMain.this.removeMember(emailSignature);
//                    SettingsMain.this.removeMember(settingsTenant);
//                    SettingsMain.this.removeMember(spacer);
//                    SettingsMain.this.removeMember(settingsCarrier);
                    SettingsMain.this.removeMember(settingsOfficeAddress);
                    SettingsMain.this.removeMember(settingsShift);

                    SettingsMain.this.addMember(new UserList());
                }
            }
        });


    }
}
