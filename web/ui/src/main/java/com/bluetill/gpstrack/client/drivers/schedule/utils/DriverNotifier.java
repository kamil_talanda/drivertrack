package com.bluetill.gpstrack.client.drivers.schedule.utils;

import com.bluetill.gpstrack.client.LegendService;
import com.bluetill.gpstrack.client.LegendServiceAsync;
import com.bluetill.gpstrack.shared.DriverData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 18/12/14
 * Time: 08:49

 */
public class DriverNotifier {

    Long driverId;

    public DriverNotifier(Long driverId) {
        this.driverId = driverId;
    }

    public void notifyChanges(){
        ((LegendServiceAsync) GWT.create(LegendService.class)).setMessage(driverId, DriverData.SCHEDULE_CHANGE_NOTIFICATION, new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }
}
