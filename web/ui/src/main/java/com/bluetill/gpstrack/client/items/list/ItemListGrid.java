package com.bluetill.gpstrack.client.items.list;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.views.ItemView;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 08/01/14
 * Time: 06:32

 */
public class ItemListGrid extends ListGrid implements BaseList, ItemVars {

    public static String groupBy = null;
    private ItemListHandlers itemListHandlers = new ItemListHandlers(this);
    private ListGridField barcodeField = new ListGridField(ItemRecord.BARCODE, BUNDLE.barcodeColumnName(), MEDIUM_FIELD_WIDTH);
    private ListGridField carrierField = new ListGridField(ItemRecord.CARRIER, BUNDLE.carrierColumnName(), SMALL_FIELD_WIDTH);
    private ListGridField tenantField = new ListGridField(ItemRecord.TENANT, BUNDLE.tenantColumnName(), SMALL_FIELD_WIDTH);
//    private ListGridField qtyField = new ListGridField(ItemRecord.QTY, BUNDLE.qtyColumnName(), VERY_SMALL_FIELD_WIDTH);
    private ListGridField nameField = new ListGridField(ItemRecord.NAME, BUNDLE.receiverNameColumnName());
    private ListGridField destinationField = new ListGridField(ItemRecord.DESTINATION, BUNDLE.destinationColumnName());
    private ListGridField addTimeField = new ListGridField(ItemRecord.ADD_TIME, BUNDLE.addedDateColumnName(), SMALL_FIELD_WIDTH);
    private ListGridField approvedTimeField = new ListGridField(ItemRecord.APPROVE_TIME, BUNDLE.approvedDateColumnName(), SMALL_FIELD_WIDTH);
    private ListGridField holderField = new ListGridField(ItemRecord.HOLDER_NAME, BUNDLE.driverColumnName(), SMALL_FIELD_WIDTH);
    private ListGridField statusField = new ListGridField(ItemRecord.STATUS, BUNDLE.statusColumnName(), SMALL_FIELD_WIDTH);
    private ListGridField editField = setUpButtonField(EDIT_IMAGE_URL, EDIT);
    private ListGridField removeField = setUpButtonField(REMOVE_IMAGE_URL, REMOVE);

    private FormItem addTimeFormItem = new FormItem();
    private FormItem approvedTimeFormItem = new FormItem();


    public ItemListGrid(List<ItemDTO> items, String[] driverArray) {

        if (groupBy != null) this.setGroupByField(groupBy);
        this.setShowGroupSummary(true);
        this.setWidth100();
        this.setHeight100();
        this.setLayoutAlign(Alignment.CENTER);
        this.setShowAllRecords(true);
        this.setShowRecordComponents(true);
        this.setShowRecordComponentsByCell(true);
        this.setBorder(TABLE_BORDER);
        this.setDataSource(new ItemListDataSource(items, driverArray));
        this.setAutoFetchData(true);
        this.setShowFilterEditor(true);
        this.setFilterOnKeypress(true);
        this.setInitialCriteria(ItemView.criteria);
        this.setFilterEditorCriteria(ItemView.criteria);
        this.setCriteria(ItemView.criteria);
        this.setSummaryRowCriteria(ItemView.criteria);
//        this.setCanEdit(true);
        this.setCanReorderRecords(true);
        this.setCanAcceptDroppedRecords(true);
        this.setDragDataAction(DragDataAction.MOVE);
        this.setLeaveScrollbarGap(false);


        this.addRecordDropHandler(itemListHandlers.getRecordDropHandler());
        this.addEditCompleteHandler(itemListHandlers.getEditCompleteHandler());
        this.addFilterEditorSubmitHandler(itemListHandlers.getFilterEditorSubmitHandler());
        this.addTimeFormItem.addChangedHandler(itemListHandlers.getDateFilterChangeHandler(ADD_TIME));
        this.approvedTimeFormItem.addChangedHandler(itemListHandlers.getDateFilterChangeHandler(APPROVE_TIME));

        this.setGridFields();

    }

    public ItemListGrid() {
    }

    private void setGridFields() {

        barcodeField.setCanEdit(false);
        carrierField.setCanEdit(false);
        tenantField.setCanEdit(false);
//        qtyField.setAlign(Alignment.CENTER);
//        qtyField.setValidateOnChange(true);
//        qtyField.setCanEdit(false);
        nameField.setCanEdit(false);
        destinationField.setCanEdit(false);

        addTimeField.setAlign(Alignment.CENTER);
        addTimeField.setCanEdit(false);
        addTimeField.setFilterEditorProperties(this.addTimeFormItem);

        approvedTimeField.setAlign(Alignment.CENTER);
        approvedTimeField.setCanEdit(false);
        approvedTimeField.setFilterEditorProperties(this.approvedTimeFormItem);

        holderField.setAlign(Alignment.CENTER);
        statusField.setAlign(Alignment.CENTER);
        editField.addRecordClickHandler(itemListHandlers.getEditItemHandler());
        removeField.addRecordClickHandler(itemListHandlers.getRemoveRecordClickHandler());

        this.setFields(
                barcodeField,
//                carrierField,
//                tenantField,
//                qtyField,
//                nameField,
                destinationField,
                addTimeField,
                approvedTimeField,
                holderField,
                statusField,
                editField
//                removeField
        );
    }

    private ListGridField setUpButtonField(String iconURL, String fieldName) {
        ListGridField listGridField = new ListGridField(fieldName, BUTTON_SIZE);
        listGridField.setType(ListGridFieldType.ICON);
        listGridField.setCellIcon(iconURL);
        listGridField.setCanEdit(false);
        listGridField.setCanFilter(true);
        listGridField.setFilterEditorType(new SpacerItem());
        listGridField.setCanGroupBy(false);
        listGridField.setCanSort(false);
        return listGridField;
    }
}
