package com.bluetill.gpstrack.client.base;

import java.util.HashMap;
import java.util.Map;

public class StringConvert {

    private static final Map<Character, Character> polishLetters = new HashMap<Character, Character>();
    static {
        polishLetters.put('ą', 'a');
        polishLetters.put('ą', 'a');
        polishLetters.put('ę', 'a');
        polishLetters.put('ó', 'o');
        polishLetters.put('ś', 's');
        polishLetters.put('ł', 'l');
        polishLetters.put('ż', 'z');
        polishLetters.put('ź', 'z');
        polishLetters.put('ć', 'c');
        polishLetters.put('ń', 'n');
    }

    private static boolean isNormalLetter(int chVal) {
        return (chVal >= 65 && chVal <= 90) || (chVal >= 97 && chVal <= 122);
    }

    public static String changeLetters(String org) {
        StringBuilder sb = new StringBuilder();
        for (char ch: org.toCharArray()) {
            if (Character.isLetter(ch)) {
                sb.append(isNormalLetter(ch)? ch : polishLetters.get(ch));
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}
