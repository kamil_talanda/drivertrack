package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.items.ItemVars;
import com.smartgwt.client.types.SelectionType;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/01/14
 * Time: 09:47

 */
public class ItemListFooterMenuAdd extends ToolStrip implements ItemVars{

    private final ToolStripButton lockBarcodeButton = new ToolStripButton();
    private final ToolStripButton generateBarcodeButton = new ToolStripButton();
    private final ToolStripButton addToListButton = new ToolStripButton();

    public ItemListFooterMenuAdd(){
        lockBarcodeButton.setActionType(SelectionType.CHECKBOX);
        lockBarcodeButton.setTitle(BUNDLE.lockBarcodeText());
        lockBarcodeButton.setSelected(false);
//        this.addButton(lockBarcodeButton);

        generateBarcodeButton.setActionType(SelectionType.BUTTON);
        generateBarcodeButton.setTitle(BUNDLE.generateBarcodeText());
//        this.addButton(generateBarcodeButton);

        addToListButton.setActionType(SelectionType.BUTTON);
        addToListButton.setTitle(BUNDLE.addListButtonText());
//        this.addButton(addToListButton);
    }

    public void addNewItemButtonListener(ClickHandler handler){
        addToListButton.addClickHandler(handler);
    }

    public void addGenerateBarcodeButton(ClickHandler handler){
        generateBarcodeButton.addClickHandler(handler);
    }

    public boolean isBarcodeLocked(){
        return lockBarcodeButton.getSelected();
    }
}
