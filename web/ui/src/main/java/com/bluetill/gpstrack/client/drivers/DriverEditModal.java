package com.bluetill.gpstrack.client.drivers;


import com.bluetill.gpstrack.client.DriverService;
import com.bluetill.gpstrack.client.DriverServiceAsync;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class DriverEditModal extends ModalWindow implements BaseList {

    private static final String MODAL_TITLE = "Edit driver";
    private static final int MODAL_WIDTH = 500;
    private static final int MODAL_HEIGHT = 150;
    private static final int MODAL_MARGIN = 20;
    protected final Label spacerHorizontal = new Label();
    protected HLayout menuLayout = new HLayout();
    protected Button cancelButton = new Button("CANCEL");
    protected Button saveButton = new Button("SAVE");
    private DriverListGrid contextListGrid;
    private TextItem nameText = new TextItem("name");
    private TextItem bmIdText = new TextItem("bm_id");


    public DriverEditModal(final DriverListGrid contextListGrid, final String id, final String name) {
        super();
        this.contextListGrid = contextListGrid;
        this.setTitle(MODAL_TITLE);
        this.setWidth(MODAL_WIDTH);
        this.setHeight(MODAL_HEIGHT);
        this.setMembersMargin(MODAL_MARGIN);

        bmIdText.setTitle("BizManager id");
        nameText.setTitle("Name");

        ((DriverServiceAsync) GWT.create(DriverService.class)).get(Long.parseLong(id), new AsyncCallback<DriverDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(DriverDTO driverDTO) {
                bmIdText.setValue(driverDTO.getBm_id());
                nameText.setValue(driverDTO.getName());
            }

        });

        spacerHorizontal.setWidth100();

        DynamicForm driverForm = new DynamicForm();
        driverForm.setNumCols(2);
        driverForm.setHeight100();

        driverForm.setFields(bmIdText, nameText);

        addItem(driverForm);

        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                destroy();
            }
        });

        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ((DriverServiceAsync) GWT.create(DriverService.class)).get(Long.parseLong(id), new AsyncCallback<DriverDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(DriverDTO driverDTO) {
                        driverDTO.setBm_id(Long.parseLong(bmIdText.getValueAsString()));
                        driverDTO.setName(nameText.getValueAsString());
                        ((DriverServiceAsync) GWT.create(DriverService.class)).set(driverDTO, new AsyncCallback<DriverDTO>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                            }

                            @Override
                            public void onSuccess(DriverDTO driverDTO) {
                                contextListGrid.reload();
                                destroy();
                            }
                        });
                    }
                });
            }
        });

        menuLayout.addMember(cancelButton);
        menuLayout.addMember(spacerHorizontal);
        menuLayout.addMember(saveButton);
        addItem(menuLayout);
    }

}