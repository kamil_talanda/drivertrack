package com.bluetill.gpstrack.client.items.details;

import com.bluetill.gpstrack.client.GeocoderService;
import com.bluetill.gpstrack.client.GeocoderServiceAsync;
import com.bluetill.gpstrack.client.MapService;
import com.bluetill.gpstrack.client.MapServiceAsync;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 06/01/14
 * Time: 14:10

 */
public abstract class ItemDetailsLayout extends VLayout implements ItemVars {

    protected static final DynamicForm mainForm = new DynamicForm();
    protected static final DynamicForm timeForm = new DynamicForm();
    protected static final DynamicForm signatureForm = new DynamicForm();
    protected final TextItem receiverTextItem = new TextItem();
    protected final SelectItem statusSelectItem = new SelectItem();
    protected final TextItem destinationInputTextItem = new TextItem();
    protected final TextItem destinationTextItem = new TextItem();
    protected final TextItem approvePointTextItem = new TextItem();
    protected final SelectItem driverSelectItem = new SelectItem();
    protected final TextItem barcodeTextItem = new TextItem();
    protected final TextAreaItem descriptionTextAreaItem = new TextAreaItem();
    protected final TextItem addTimeItem = new TextItem();
    protected final TextItem approveTimeItem = new TextItem();
    protected ItemDetails context;
    protected CanvasItem signatureItem = new CanvasItem();
    protected Img signatureImg = new Img();
    protected CanvasItem barcodeImgCanvas = new CanvasItem();
    protected SpacerItem spacerItem = new SpacerItem();
    protected Img barcodeImg;

    public ItemDetailsLayout(ItemDetails context, Record record) {
        this.context = context;
        this.initFields();
        fillUp(record);

        destinationTextItem.setShowTitle(false);

        mainForm.setNumCols(4);

        Label warning = new Label();
        warning.setContents("This window is just for view. To edit the item go to DRIVERS->SCHEDULE.");
        warning.setHeight(20);
        super.addMember(warning);

        mainForm.setFields(
//                receiverTextItem,
//                carrierSelectItem,
//
//                tenantSelectItem,
                destinationInputTextItem,
                spacerItem,
                destinationTextItem,

                barcodeTextItem,
                statusSelectItem,

                driverSelectItem,
                spacerItem,
                spacerItem,

                descriptionTextAreaItem,
                barcodeImgCanvas,

                approvePointTextItem);
        super.addMember(mainForm);

        timeForm.setNumCols(3);
        timeForm.setFields(
                addTimeItem,
                approveTimeItem
        );
        super.addMember(timeForm);

        signatureForm.setNumCols(2);
        signatureItem.setCanvas(signatureImg);
        signatureForm.setFields(
                signatureItem
        );
        super.addMember(signatureForm);
    }

    public abstract void fillUp(Record record);

    protected void resolveDestination(){
        ((GeocoderServiceAsync) GWT.create(GeocoderService.class)).geocodeAddress(destinationInputTextItem.getValueAsString(), new AsyncCallback<String[]>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(String[] locationDetails) {
                if(locationDetails != null && locationDetails.length > 1){
                    approvePointTextItem.setValue(locationDetails[0]);
                    destinationTextItem.setValue(locationDetails[1]);
                }
            }
        });
    }


    private void initFields() {

        destinationInputTextItem.disable();
        destinationTextItem.disable();
        barcodeTextItem.disable();
        statusSelectItem.disable();
        driverSelectItem.disable();
        descriptionTextAreaItem.disable();
        addTimeItem.disable();
        approveTimeItem.disable();

        receiverTextItem.setTitle(BUNDLE.receiverTitle());
        statusSelectItem.setTitle(BUNDLE.statusTitle());
        statusSelectItem.setValueMap(ItemUIUtils.getStatusMap());
        statusSelectItem.setDefaultToFirstOption(true);

        destinationInputTextItem.setTitle(BUNDLE.destinationTitle());

        approvePointTextItem.setTitle("");
        approvePointTextItem.setVisible(false);
        destinationTextItem.setTitle("");

        driverSelectItem.setTitle(BUNDLE.holderTitle());
        ((MapServiceAsync) GWT.create(MapService.class)).getAll(new AsyncCallback<List<DriversPathDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<DriversPathDTO> result) {
                context.driversMap.put(Long.toString(EMPTY_HOLDER_ID), BUNDLE.unassigned());
                for (DriversPathDTO driversPathDTO : result) {
                    context.driversMap.put(Long.toString(driversPathDTO.getId()), driversPathDTO.getName());
                }
                driverSelectItem.setValueMap(context.driversMap);
            }
        });
        driverSelectItem.setDefaultToFirstOption(true);

        barcodeTextItem.setTitle(BUNDLE.barcodeTitle());

        barcodeImgCanvas.setShowTitle(false);
        barcodeImgCanvas.setAlign(Alignment.CENTER);
        barcodeImg = new Img(IMAGE_FORMAT, DETAILS_BARCODE_WIDTH, DETAILS_BARCODE_HEIGHT);
        barcodeImg.setHeight(DETAILS_BARCODE_HEIGHT);

        barcodeImg.setMargin(20);
        barcodeImgCanvas.setCanvas(barcodeImg);
        barcodeImgCanvas.setColSpan(2);

        descriptionTextAreaItem.setTitle(BUNDLE.descriptionTitle());
        descriptionTextAreaItem.setStartRow(true);

        addTimeItem.setTitle(BUNDLE.addDateTitle());
        addTimeItem.setEndRow(false);
        addTimeItem.setStartRow(false);


        approveTimeItem.setTitle(BUNDLE.approveDateTitle());
        approveTimeItem.setEndRow(false);
        approveTimeItem.setStartRow(false);


        signatureItem.setTitle(BUNDLE.signatureTitle());


    }

    public ItemDTO getCurrentDTO() {
        ItemDTO item = new ItemDTO();

        if (context.currentRecord != null) {
            item.setId(context.currentRecord.getAttributeAsLong(ID));
            item.setSignatureImage(context.currentRecord.getAttributeAsString(ItemRecord.SIGNATURE));
            Date addDate = context.currentRecord.getAttributeAsDate(ItemRecord.ADD_TIME);
            Date approveTime = context.currentRecord.getAttributeAsDate(ItemRecord.APPROVE_TIME);
            if (addDate != null) {
                item.setAddTime(addDate.getTime());
            }
            if (approveTime != null) {
                item.setApproveTime(approveTime.getTime());
            }
        } else { //if item is new, set add date to current time
            item.setAddTime(new Date().getTime());
        }
        item.setName(receiverTextItem.getValueAsString());
        if (driverSelectItem.getValueAsString() != null)
            item.setHolderId(Long.parseLong(driverSelectItem.getValueAsString()));
        item.setHolderName(context.driversMap.get(driverSelectItem.getValueAsString()));
        item.setBarcode(barcodeTextItem.getValueAsString());
        item.setDescription(descriptionTextAreaItem.getValueAsString());
        item.setStatus(ItemUIUtils.getStatusLong(ItemUIUtils.getStatusMap().get(statusSelectItem.getValueAsString())));
        item.setDestination(destinationTextItem.getValueAsString());
        item.setApprovePoint(approvePointTextItem.getValueAsString());
        return item;
    }
}
