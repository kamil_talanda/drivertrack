package com.bluetill.gpstrack.client.settings.others;

import com.bluetill.gpstrack.client.SettingsService;
import com.bluetill.gpstrack.client.SettingsServiceAsync;
import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/02/14
 * Time: 09:02

 */
public class SettingsEmailSignature extends VLayout implements SettingsVars {

    private final ToolStrip emailSignatureToolStrip = new ToolStrip();
    private final TextItem emailSignatureText = new TextItem();
    private final ToolStripButton saveSignatureButton = new ToolStripButton();
    private final ToolStrip emailFromToolStrip = new ToolStrip();
    private final TextItem emailFromText = new TextItem();
    private final ToolStripButton saveFromButton = new ToolStripButton();

    public SettingsEmailSignature() {
        emailFromToolStrip.addMember(new Label("&nbsp" + BUNDLE.emailFromText() + " :"));
        emailFromText.setWidth("100%");
        emailFromText.setShowTitle(false);
        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getByName(SettingsDTO.SETTINGS_EMAIL_FROM, new AsyncCallback<SettingsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(SettingsDTO settingsDTO) {
                if (settingsDTO != null) emailFromText.setValue(settingsDTO.getValue());
            }
        });
        emailFromToolStrip.addFormItem(emailFromText);

        saveFromButton.setTitle(BUNDLE.setButtonTitle());
        saveFromButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                SettingsDTO dto = new SettingsDTO();
                dto.setName(SettingsDTO.SETTINGS_EMAIL_FROM);
                dto.setValue((String) emailFromText.getValue());
                ((SettingsServiceAsync) GWT.create(SettingsService.class)).set(dto, new AsyncCallback<SettingsDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(SettingsDTO aVoid) {
                        SC.say(BUNDLE.emailFromSetAlert());
                    }
                });
            }

            ;
        });

        emailFromToolStrip.addButton(saveFromButton);
        this.addMember(emailFromToolStrip);

        emailSignatureToolStrip.addMember(new Label("&nbsp" + BUNDLE.emailSignatureText() + " :"));
        emailSignatureText.setShowTitle(false);
        emailSignatureText.setWidth("100%");
        ((SettingsServiceAsync) GWT.create(SettingsService.class)).getByName(SettingsDTO.SETTINGS_EMAIL_SIGNATURE, new AsyncCallback<SettingsDTO>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(SettingsDTO settingsDTO) {
                if (settingsDTO != null) emailSignatureText.setValue(settingsDTO.getValue());
            }
        });
        emailSignatureToolStrip.addFormItem(emailSignatureText);
        saveSignatureButton.setTitle(BUNDLE.setButtonTitle());
        saveSignatureButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                SettingsDTO dto = new SettingsDTO();
                dto.setName(SettingsDTO.SETTINGS_EMAIL_SIGNATURE);
                dto.setValue((String) emailSignatureText.getValue());
                ((SettingsServiceAsync) GWT.create(SettingsService.class)).set(dto, new AsyncCallback<SettingsDTO>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(SettingsDTO result) {
                        SC.say(BUNDLE.emailSignatureSetAlert());
                    }
                });
            }
        });
        emailSignatureToolStrip.addButton(saveSignatureButton);
        this.addMember(emailSignatureToolStrip);


    }
}
