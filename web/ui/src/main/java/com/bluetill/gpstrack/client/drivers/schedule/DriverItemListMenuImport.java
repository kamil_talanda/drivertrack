package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.DriverService;
import com.bluetill.gpstrack.client.DriverServiceAsync;
import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.bluetill.gpstrack.client.base.AskModal;
import com.bluetill.gpstrack.client.drivers.DriverVars;
import com.bluetill.gpstrack.client.drivers.schedule.utils.DriverNotifier;
import com.bluetill.gpstrack.client.drivers.schedule.utils.WaitingModal;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import gwtupload.client.IUploadStatus;
import gwtupload.client.IUploader;
import gwtupload.client.MultiUploader;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 11/02/14
 * Time: 10:40
 */
public class DriverItemListMenuImport extends HLayout implements DriverVars {

    protected static MultiUploader uploader;
    protected Button importButton = new Button();
    protected Label spacer = new Label();
    private List<ItemDTO> itemDTOs = new ArrayList<ItemDTO>();
    private boolean uploaded = false;
    private int counter;
    private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
        public void onFinish(IUploader uploader) {
            itemDTOs = new ArrayList<ItemDTO>();
            if (uploader.getStatus() == IUploadStatus.Status.SUCCESS) {
                boolean firstLine = true;
                for (String line : uploader.getServerInfo().message.split("\n")) {
                    final String[] values = line.split(",");
                    final ItemDTO dto = new ItemDTO();
                    if (firstLine) {
                        firstLine = false;
                    } else {
                        ((DriverServiceAsync) GWT.create(DriverService.class)).getByBmId(Long.parseLong(values[2]), new AsyncCallback<DriverDTO>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                                uploaded = true;
                            }

                            @Override
                            public void onSuccess(DriverDTO driverDTO) {
                                try {
                                    dto.setBmNo(Long.parseLong(values[0].replaceAll(" ", "")));
                                    dto.setItemListOrder(Long.parseLong(values[0].replaceAll(" ", "")));

                                    DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
                                    Date date = dateFormat.parse(values[1].replaceAll(" ", ""));
                                    date.setHours(8);
                                    dto.setItemList(date.getTime());
                                    dto.setHolderId(driverDTO.getId());

                                    dto.setBarcode(values[3].replaceAll(" ", ""));
                                    dto.setDescription(values[4]);
                                    dto.setDestination(values[5]);
                                    dto.setStatus(0l);
                                    itemDTOs.add(dto);
                                    uploaded = true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        }
    };

    public DriverItemListMenuImport() {


        spacer.setWidth("100%");

        this.addMember(importButton);

        importButton.setTitle(BUNDLE.importButtonText());
        importButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (uploaded) {
                    Date today = new Date();
                    today.setHours(0);
                    today.setMinutes(0);
                    today.setSeconds(0);

                    boolean pastDate = false;
                    for (ItemDTO dto : itemDTOs) {
                        if (dto.getItemList() < today.getTime())
                            pastDate = true;
                    }

                    if (itemDTOs == null || itemDTOs.size() <= 0) {
                        SC.say("Wrong file format.");
                    } else if (pastDate) {
                        SC.say("Date of the items you are trying to upload is invalid. Check if the tasks you want to upload are not scheduled for the past date.");
                    } else {
                        counter = 0;
                        AskModal askModal = new AskModal("Import Data", "Do you want to upload item list from file and replace all previous tasks for day: " + DateTimeFormat.getFormat("dd/MM/yyyy").format(new Date(itemDTOs.get(0).getItemList()))) {
                            @Override
                            protected void yesAction() {
                                uploader.clear();
                                uploader.reset();
                                final WaitingModal modal = new WaitingModal();
                                modal.show();

                                HashMap<Long, ArrayList<ItemDTO>> driversMap = new HashMap<Long, ArrayList<ItemDTO>>();
                                for (ItemDTO dto : itemDTOs) {
                                    if (!driversMap.containsKey(dto.getHolderId()))
                                        driversMap.put(dto.getHolderId(), new ArrayList<ItemDTO>());
                                    driversMap.get(dto.getHolderId()).add(dto);
                                }

                                for (final Long driverId : driversMap.keySet()) {
                                    final ArrayList<ItemDTO> driverItemDTOs = driversMap.get(driverId);
                                    ((ItemsServiceAsync) GWT.create(ItemsService.class)).deleteByList(driverItemDTOs.get(0).getItemList(), driverId, new AsyncCallback<Void>() {
                                        @Override
                                        public void onFailure(Throwable throwable) {

                                        }

                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            final List<ItemDTO> resultList = new ArrayList<ItemDTO>();
                                            for (ItemDTO dto : driverItemDTOs) {
                                                dto.setAddTime(new Date().getTime());
                                                ((ItemsServiceAsync) GWT.create(ItemsService.class)).setFromUploadFIle(dto, new AsyncCallback<ItemDTO>() {
                                                    @Override
                                                    public void onFailure(Throwable throwable) {
                                                    }

                                                    @Override
                                                    public void onSuccess(ItemDTO dto) {
                                                        counter++;
                                                        resultList.add(dto);
                                                        if (counter >= driverItemDTOs.size()) {
                                                            new DriverItemListCalculator(driverId) {
                                                                @Override
                                                                protected void successAction() {
                                                                    modal.hide();
                                                                    new DriverNotifier(driverId).notifyChanges();
                                                                }
                                                            }.execute();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }

                            @Override
                            protected void noAction() {
                            }

                            @Override
                            protected void closeAction() {
                            }
                        };
                        askModal.setHeight(150);
                        askModal.show();
                    }

                } else {
                    SC.say(BUNDLE.importWarningNotReady());
                }
            }
        });

        setUpUploader();
        this.addMember(uploader);
    }

    private void setUpUploader() {
        if (uploader == null) {
            uploader = new MultiUploader();
            uploader.addOnCancelUploadHandler(new IUploader.OnCancelUploaderHandler() {
                @Override
                public void onCancel(IUploader widgets) {
                    itemDTOs = new ArrayList<ItemDTO>();
                    uploaded = false;
                }
            });
            uploader.addOnFinishUploadHandler(onFinishUploaderHandler);
            uploader.addOnStartUploadHandler(new IUploader.OnStartUploaderHandler() {
                @Override
                public void onStart(IUploader widgets) {
                    uploaded = false;
                }
            });
        }
    }

}
