package com.bluetill.gpstrack.client.map;

import com.bluetill.gpstrack.shared.Directions;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/01/14
 * Time: 10:53

 */
public abstract class MapUtils {
    protected static final String patternString = "\\((.*?)\\)";

    public static ArrayList<Directions> getPointListFromWKT(String wktGeometry, String description) {
        ArrayList<Directions> resultPointList = new ArrayList<Directions>();
        RegExp regExp = RegExp.compile(patternString);
        MatchResult matcher = regExp.exec(wktGeometry);
        if (matcher != null) {

            for (String curPosition : matcher.getGroup(1).split(",")) {
                String[] r = curPosition.split(" ");
                resultPointList.add(new Directions(Double.parseDouble(r[0]), Double.parseDouble(r[1]), description));
            }
        }
        return resultPointList;
    }

    public static Directions getPointFromWKT(String wktPoint, String description) {
        Directions directions = new Directions();
        for (Directions curDirections : MapUtils.getPointListFromWKT(wktPoint, description)) {
            directions = curDirections;
        }
        return directions;
    }

    protected enum MapEntryType {
        LINE, POINT
    }

    protected enum AttributeType {
        TIMESTAMP("timestamp"), ITEM("item"), DESCRIPTION("description");
        private String attributeLabel;

        AttributeType(String attributeLabel) {
            this.attributeLabel = attributeLabel;
        }

        private String getAttributeLabel() {
            return attributeLabel;
        }

    }

}
