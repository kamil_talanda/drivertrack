package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 08/01/14
 * Time: 10:05

 */
public class ItemListFooter extends VLayout implements BaseList, ItemVars {

    private ItemListFooterMenu itemListFooterMenu;

    public ItemListFooter(final ItemList context) {
        this.setWidth(FOOTER_WIDTH);
        this.setHeight(FOOTER_HEIGHT);
        this.setPadding(FOOTER_PADDING);
        this.setLayoutAlign(VerticalAlignment.BOTTOM);
        this.setLayoutRightMargin(DEFAULT_MEMBERS_MARGIN);

        itemListFooterMenu = new ItemListFooterMenu(context);

        this.addMember(itemListFooterMenu);
    }

    public void reloadBarcodeQueueNumber() {
        itemListFooterMenu.reloadBarcodeQueueNumber();
    }
}