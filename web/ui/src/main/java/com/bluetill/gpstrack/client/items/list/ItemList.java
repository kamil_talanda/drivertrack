package com.bluetill.gpstrack.client.items.list;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.footer.ItemListFooter;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.DateUtil;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;

import java.util.ArrayList;
import java.util.List;

public class ItemList extends ModalWindow implements BaseList, ItemVars {

    private static ItemListGrid itemsListGrid;
    private static ItemListFooter itemListFooter;

    public static String[] drivers;

    public static List<ItemDTO> items;

    public ItemList() {
        super();

        DateUtil.setShortDateDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
        DateUtil.setShortDatetimeDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

        itemsListGrid = new ItemListGrid();
        itemListFooter = new ItemListFooter(this);
        this.setWidth100();
        this.setHeight100();
        this.setMembersMargin(DEFAULT_MEMBERS_MARGIN);
    }

    public static ItemListGrid getItemsListGrid() {
        return itemsListGrid;
    }

    public void setItemsListGrid(ItemListGrid itemsListGrid) {
        ItemList.itemsListGrid = itemsListGrid;
    }

    public static ItemListFooter getItemListFooter() {
        return itemListFooter;
    }

    public ListGrid createTable(final List<ItemDTO> inItems, ArrayList<DriverDTO> inDrivers) {
        ArrayList<String> driverList = new ArrayList<String>();
        for(DriverDTO dto: inDrivers){
            driverList.add(dto.getName());
        }
        drivers = driverList.toArray(new String[driverList.size()]);
        items = inItems;
        itemsListGrid = new ItemListGrid(items, drivers);
        itemListFooter = new ItemListFooter(this);
        return itemsListGrid;
    }

    public ItemListFooter createFooter() {
        return itemListFooter;
    }

    public void setRecordDoubleClickHandler(RecordDoubleClickHandler handler) {
        itemsListGrid.addRecordDoubleClickHandler(handler);
    }

    public void setTableRecords(List<ItemRecord> records) {
        itemsListGrid.selectAllRecords();
        itemsListGrid.removeSelectedData();
        for (ListGridRecord record : records) {
            itemsListGrid.addData(record);
        }
    }

}