package com.bluetill.gpstrack.client.base;

import org.gwtopenmaps.openlayers.client.LonLat;

/**
 * Moduł do konwersji współrzędnych.
 */
public final class LonLatConverter {
    
    private static final int MINUTES = 60;
    private static final int SECONDS = 10;
    private static final int MAGNITUDE = 100;
    
    private LonLatConverter() {
    }
    
    /**
     * Oznacza kierunek geograficzny.
     * @param coordinate - współrzędne
     * @param isLongtitude - czy sprawdzana jest długośc, czy szerokośc
     * @return zwraca kierunek
     */
    public static String getHemisphereForCoordinate(final float coordinate, final boolean isLongtitude) {
        if (isLongtitude) {
            if (coordinate >= 0) {
                return "E";
            } else {
                return "W";
            }
        } else {
            if (coordinate >= 0) {
                return "N";
            } else {
                return "S";
            }
        }
    }

    /**
     * Formatowanie współrzędnych.
     * @param coordinate - współrzędne
     * @return sformatowany łańcuch
     */
    public static String dmsCoordinateToString(final float coordinate) {
        
        final float absCoordinate = Math.abs(coordinate);
        final float degrees = (float) Math.floor(absCoordinate);
        float minutes = (absCoordinate - degrees) * MINUTES;
        final float tempMinutes = minutes;
        minutes = (float) Math.floor(minutes);
        float seconds = (tempMinutes - minutes) * MINUTES;
        seconds = Math.round(seconds * MAGNITUDE);
        
        String zeros = "";
        if (((int) seconds) % SECONDS == 0) {
            zeros = "0";
        }
        final float secondsf = (float) ((int) seconds) / MAGNITUDE;
        
        final StringBuilder wyjscie = new StringBuilder("");
        if (((int) degrees) < SECONDS) {
            wyjscie.append(0);
        }
        wyjscie.append((int) degrees).append("°");
        if (((int) minutes) < SECONDS) {
            wyjscie.append(0);
        }
        wyjscie.append((int) minutes).append('\'');
        if (((int) seconds) < SECONDS) {
            wyjscie.append(0);
        }
        wyjscie.append(secondsf).append(zeros).append('"');
        return wyjscie.toString();
    }

    /**
     * Przygotowuje łańcuch ze współrzędnymi do wyświetlenia.
     * @param point - współrzędna do przetworzenia.
     * @return sformatowane współrzędne
     */
    public static String toDMSString(final LonLat point) {
        
        final StringBuilder builder = new StringBuilder();
        builder.append(getHemisphereForCoordinate((float) point.lon(), true));
        builder.append(" ");
        builder.append(dmsCoordinateToString((float) point.lon()));
        builder.append(", ");
        builder.append(getHemisphereForCoordinate((float) point.lat(), false));
        builder.append(" ");
        builder.append(dmsCoordinateToString((float) point.lat()));
        
        return builder.toString();
    }
}
