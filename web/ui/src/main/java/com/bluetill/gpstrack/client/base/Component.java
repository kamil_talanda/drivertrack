package com.bluetill.gpstrack.client.base;

/**
 * Componentns enum, each value should be assigned to one existing
 * <view, presenter> pair.
 */
public enum Component {
    HEADER,
    FOOTER,
    CONTAINER,
    LAYER_TREE,
    MAP,
    TASKS,
    DRIVER,
    SETTINGS
}
