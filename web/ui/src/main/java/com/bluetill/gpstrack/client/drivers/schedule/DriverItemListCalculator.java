package com.bluetill.gpstrack.client.drivers.schedule;

import com.bluetill.gpstrack.client.Item_ListService;
import com.bluetill.gpstrack.client.Item_ListServiceAsync;
import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 17/12/14
 * Time: 10:30

 */
public abstract class DriverItemListCalculator {

    private Long driverId;

    public DriverItemListCalculator(final Long driverId) {
        this.driverId = driverId;
    }

    public void execute(){
        ((ItemsServiceAsync) GWT.create(ItemsService.class)).getByDriverId(driverId, new AsyncCallback<List<ItemDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(List<ItemDTO> itemDTOs) {
                ((Item_ListServiceAsync) GWT.create(Item_ListService.class)).calculateRoute(itemDTOs, new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        successAction();
                    }
                });
            }
        });
    }

    protected abstract void successAction();
}
