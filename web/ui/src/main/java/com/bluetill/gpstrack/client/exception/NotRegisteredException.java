package com.bluetill.gpstrack.client.exception;

import com.google.gwt.core.client.GWT;

/**
 * Exception thrown for not registered ui components.
 */
@SuppressWarnings("serial")
public class NotRegisteredException extends Exception {

    // bundle conatining messages.
    private static final transient NotRegisteredExceptionL10nBundle BUNDLE
            = GWT.create(NotRegisteredExceptionL10nBundle.class);

    /**
     * Creates new exception instance.
     */
    public NotRegisteredException() {
        super(BUNDLE.message());
    }
}
