package com.bluetill.gpstrack.client.settings.list;

import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.EditCompleteEvent;
import com.smartgwt.client.widgets.grid.events.EditCompleteHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 10/02/14
 * Time: 10:26

 */
public abstract class SettingsList extends VLayout implements SettingsVars {

    protected ListGrid listGrid = new ListGrid();
    private ListGridField nameField = new ListGridField(SettingsRecord.NAME_ATTRIBUTE);
    private ListGridField removeField = setUpButtonField(REMOVE_IMAGE_URL, SettingsRecord.REMOVE_ATTRIBUTE);

    private Button addNewTenant = new Button();

    public SettingsList() {

        listGrid.setLeaveScrollbarGap(false);
        removeField.addRecordClickHandler(new RecordClickHandler() {
            @Override
            public void onRecordClick(final RecordClickEvent recordClickEvent) {
                SC.confirm(BUNDLE.removeAlertTitle(), BUNDLE.removeAlertText(), new BooleanCallback() {
                    public void execute(Boolean value) {
                        if (Boolean.TRUE.equals(value)) {
                            listGrid.removeData(recordClickEvent.getRecord());
                            removeRecordFromDB(recordClickEvent.getRecord().getAttribute(SettingsRecord.NAME_ATTRIBUTE));
                        }
                    }
                });
            }
        });
        listGrid.setFields(nameField, removeField);

        loadData();

        this.addMember(listGrid);
        this.addMember(addNewTenant);

        addNewTenant.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                listGrid.startEditingNew();
            }
        });

        listGrid.addEditCompleteHandler(new EditCompleteHandler() {
            @Override
            public void onEditComplete(EditCompleteEvent editCompleteEvent) {

                if(editCompleteEvent.getNewValues().size()>0){
                    for(Object name : editCompleteEvent.getNewValues().values()){
                        boolean isInList = false;
                        boolean isInDB = false;
                        for (ListGridRecord tenantRecord : listGrid.getRecords()) {
                            if (tenantRecord.getAttributeAsString(SettingsRecord.NAME_ATTRIBUTE).equals(name)) {
                                if(isInList){
                                    isInDB = true;
                                    listGrid.removeData(tenantRecord);
                                    SC.say(BUNDLE.alreadyExistsAlertText());
                                }
                                isInList = true;
                            }
                        }
                        if(!isInDB){
                            addNewRecordToDB((String) name);
                        }
                    }
                }
                listGrid.setCanEdit(false);
                cleanList();
            }
        });
    }

    private void cleanList(){
        for (ListGridRecord tenantRecord : listGrid.getRecords()) {
            if (tenantRecord.getAttributeAsString(SettingsRecord.NAME_ATTRIBUTE) == null) {
                listGrid.removeData(tenantRecord);
            }
        }
    }

    protected void setAddButtonTitle(String listName){
        addNewTenant.setTitle(BUNDLE.addNewButtonBaseTitle() + " " + listName);
    }

    private ListGridField setUpButtonField(String iconURL, String fieldName) {
        ListGridField listGridField = new ListGridField(fieldName, BUTTON_SIZE);
        listGridField.setType(ListGridFieldType.ICON);
        listGridField.setCellIcon(iconURL);
        listGridField.setCanEdit(false);
        listGridField.setCanFilter(true);
        listGridField.setFilterEditorType(new SpacerItem());
        listGridField.setCanGroupBy(false);
        listGridField.setCanSort(false);
        return listGridField;
    }

    protected class SettingsRecord extends ListGridRecord implements SettingsVars {
        public SettingsRecord(String name) {
            this.setAttribute(NAME_ATTRIBUTE, name);
        }
    }

    protected abstract void loadData();
    protected abstract void addNewRecordToDB(String name);
    protected abstract void removeRecordFromDB(String name);
}
