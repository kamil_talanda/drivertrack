package com.bluetill.gpstrack.client.settings.admin;

import com.bluetill.gpstrack.client.GlobalVars;
import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.client.UserServiceAsync;
import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.settings.SettingsVars;
import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/05/14
 * Time: 08:24

 */
public class UserList extends VLayout implements BaseList, GlobalVars, SettingsVars {

    private UserListGrid userListGrid = new UserListGrid(new ArrayList<UserDTO>());
    private Button newUserButton = new Button(BUNDLE.addNewUserButtonTitle());

    public UserList() {
        this.setMargin(MAIN_MARGIN);

        ((UserServiceAsync) GWT.create(UserService.class)).getAll(new AsyncCallback<List<UserDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
            }

            @Override
            public void onSuccess(final List<UserDTO> userDTOs) {

                reloadUserList(userDTOs);
                newUserButton.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent clickEvent) {
                        SC.askforValue(BUNDLE.addNewUserDialogUserNameTitle(), "", new ValueCallback() {
                            @Override
                            public void execute(final String username) {
                                if (username != null && username.length() > 0) {
                                    SC.askforValue(BUNDLE.addNewUserDialogPasswordTitle(), "", new ValueCallback() {
                                        @Override
                                        public void execute(final String password) {
                                            if (password != null && password.length() > 0) {
                                                final UserDTO userDTO = new UserDTO(username, md5Message(password), true, "");
                                                ((UserServiceAsync) GWT.create(UserService.class)).set(userDTO, new AsyncCallback<UserDTO>() {
                                                    @Override
                                                    public void onFailure(Throwable throwable) {
                                                    }

                                                    @Override
                                                    public void onSuccess(UserDTO result) {
                                                        userDTOs.add(result);
                                                        reloadUserList(userDTOs);
                                                    }
                                                });

                                            } else {
                                                SC.say(BUNDLE.errorMessage());
                                            }
                                        }
                                    });
                                } else {
                                    SC.say(BUNDLE.errorMessage());
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    public void reloadUserList(List<UserDTO> userDTOs) {
        if (contains(userListGrid)) removeMember(userListGrid);
        if (contains(newUserButton)) removeMember(newUserButton);
        userListGrid = new UserListGrid(userDTOs);
        addMember(userListGrid);
        addMember(newUserButton);
    }

    private String md5Message(String message) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(message.getBytes());
            byte byteData[] = messageDigest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return message;
    }
}
