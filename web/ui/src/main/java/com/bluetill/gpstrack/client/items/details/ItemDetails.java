package com.bluetill.gpstrack.client.items.details;


import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.base.ModalWindow;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;

import java.util.LinkedHashMap;

public class ItemDetails extends ModalWindow implements BaseList, ItemVars {

    public Record currentRecord;
    public LinkedHashMap<String, String> driversMap = new LinkedHashMap<String, String>();
    public ItemDetailsLayout itemDetailsLayout = new ItemDetailsLayout(this, null) {
        @Override
        public void fillUp(Record record) {
        }
    };
    public ItemDetailsFooter itemDetailsFooter;

    public ItemDetails(boolean fromList) {
        super();
        this.setWidth(WINDOW_WIDTH);
        this.setHeight(WINDOW_HEIGHT_WITHOUT_SIGNATURE);
        this.setMembersMargin(DEFAULT_MEMBERS_MARGIN);
        this.addItem(itemDetailsLayout);
        this.itemDetailsFooter = new ItemDetailsFooter(this, fromList);
        this.addItem(itemDetailsFooter);
    }

    public void setCurrentRecord(Record currentRecord) {
        this.currentRecord = currentRecord;
    }

    public void showWindow(Record record) {
        if (record != null) itemDetailsLayout = new ItemDetailsLayoutEdit(this, record);
        itemDetailsFooter.reloadQueueTitle();
        this.show();
    }

    public ItemDTO getCurrentDTO() {
        return itemDetailsLayout.getCurrentDTO();
    }

    public Label getBarcodeLabel() {
        return itemDetailsFooter.barcodeLabel;
    }

    public void setBarcodeImage(Img barcodeImage) {
        itemDetailsFooter.barcodeImage = barcodeImage;
    }


}