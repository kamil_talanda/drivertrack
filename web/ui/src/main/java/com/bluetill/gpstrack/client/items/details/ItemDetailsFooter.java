package com.bluetill.gpstrack.client.items.details;

import com.bluetill.gpstrack.client.*;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.ItemsPresenter;
import com.bluetill.gpstrack.client.items.print.BarcodeQueueListGrid;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.*;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 06/01/14
 * Time: 16:23

 */
public class ItemDetailsFooter extends HLayout implements ItemVars {

    private static final String FOOTER_HEIGHT = "35px";
    private static final String FOOTER_WIDTH = "100%";
    private static final int FOOTER_PADDING = 5;
    private static long emptyHolderId = -1;
    public final Label barcodeLabel = new Label();
    private final Button saveButton = new Button();
    private final Button closeButton = new Button();
    private final Button addToQueueButton = new Button();
    private final Button printButton = new Button();
    private final Button emailButton = new Button();
    private final Label separator = new Label();
    public Img barcodeImage;
    private ItemDetails context;
    private boolean fromList = false;

    public ItemDetailsFooter(ItemDetails context, boolean fromList) {
        this.context = context;

        this.setWidth(FOOTER_WIDTH);
        this.setHeight(FOOTER_HEIGHT);
        this.setPadding(FOOTER_PADDING);
        this.setDefaultLayoutAlign(VerticalAlignment.BOTTOM);
        this.setLayoutRightMargin(DEFAULT_MEMBERS_MARGIN);

        this.fromList = fromList;

        initFields();

//        this.addMember(this.saveButton);
//        this.addMember(this.addToQueueButton);
//        this.addMember(this.printButton);
        this.addMember(separator);
//        this.addMember(this.emailButton);
        this.addMember(this.closeButton);

        setSaveButtonClickHandler();
        setPrintButtonClickHandler();
        setAddToQueueButtonClickHandler();
        setEmailButtonClickHandler();
    }

    public void reloadQueueTitle() {
        if (fromList) {
            this.addToQueueButton.setTitle("TO QUEUE (" + getBarcodeNumberInQueue() + ")");
            BarcodeQueueListGrid.reloadBarcodeQueueNumberList();
        }
    }

    public void initFields() {
        this.saveButton.setLayoutAlign(Alignment.LEFT);
        this.saveButton.setLayoutAlign(VerticalAlignment.CENTER);
        this.saveButton.setTitle(BUNDLE.saveButtonText());

        this.printButton.setLayoutAlign(Alignment.LEFT);
        this.printButton.setLayoutAlign(VerticalAlignment.CENTER);
        this.printButton.setTitle(BUNDLE.printButtonText());

        this.addToQueueButton.setLayoutAlign(Alignment.LEFT);
        this.addToQueueButton.setLayoutAlign(Alignment.CENTER);
        reloadQueueTitle();

        separator.setWidth("*");
        separator.setLayoutAlign(VerticalAlignment.CENTER);

        this.emailButton.setLayoutAlign(Alignment.RIGHT);
        this.emailButton.setLayoutAlign(VerticalAlignment.CENTER);
        this.emailButton.setTitle(BUNDLE.emailButtonText());

        this.closeButton.setLayoutAlign(Alignment.RIGHT);
        this.closeButton.setLayoutAlign(VerticalAlignment.CENTER);
        this.closeButton.setTitle(BUNDLE.closeButtonText());
        this.closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ((ItemsPresenter) Website.getPresenter(Component.TASKS)).closeWindow();
            }
        });
    }

    private void setSaveButtonClickHandler() {
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                ItemDTO item = context.getCurrentDTO();
                ((ItemsPresenter) Website.getPresenter(Component.TASKS)).saveData(item);
                long lastHolderId = emptyHolderId;
                if (item.getHolderId() != null) {
                    lastHolderId = item.getHolderId();
                }
                if (item.getHolderId() != emptyHolderId && item.getHolderId() != lastHolderId) {
                    ((LegendServiceAsync) GWT.create(LegendService.class)).setMessage(
                            item.getHolderId(), BUNDLE.newItemMessage_1() + " " + item.getId() + " " + BUNDLE.newItemMessage_2(), new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                        }

                        @Override
                        public void onSuccess(Void aVoid) {
                        }

                    });
                }
            }
        });
    }

    private void setPrintButtonClickHandler() {
        printButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                Img barcodeImagePrint = barcodeImage;
                barcodeImagePrint.setWidth(new Double(PRINT_BARCODE_WIDTH).intValue());
                barcodeImagePrint.setHeight(new Double(PRINT_BARCODE_HEIGHT * 0.5).intValue());

                HashMap<String, Img> barcodeMap = new HashMap<String, Img>();
                barcodeMap.put(barcodeLabel.getContents(), barcodeImagePrint);
                Canvas.printComponents(new Object[]{ItemUIUtils.generateBarcodePrintSet(barcodeMap)});
            }
        });
    }

    private void setAddToQueueButtonClickHandler() {
        addToQueueButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

                ItemDTO itemToDelete = null;
                ItemDTO itemToUpload = null;
                for (ItemDTO itemDTO : BarcodeQueueListGrid.barcodeQueue) {
                    if (itemDTO.getId() == context.getCurrentDTO().getId()) {
                        itemToDelete = new ItemDTO(itemDTO);
                        itemToUpload = new ItemDTO(itemDTO);
                        itemToUpload.setQty(itemToUpload.getQty() + 1);
                    }
                }
                if (itemToDelete != null) BarcodeQueueListGrid.barcodeQueue.remove(itemToDelete);
                if (itemToUpload != null) BarcodeQueueListGrid.barcodeQueue.add(itemToUpload);
                else BarcodeQueueListGrid.barcodeQueue.add(context.getCurrentDTO());

                reloadQueueTitle();
            }
        });
    }

    private void setEmailButtonClickHandler() {
        emailButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                final ItemDTO item = context.getCurrentDTO();
                String toAddress = null;

                if (item.getName() != null) {
                    String[] receiverArray = item.getName().split(" ");
                    for (String receiverString : receiverArray) {
                        if (receiverString.matches(EMAIL_REGEX)) toAddress = receiverString;
                    }
                }

                if (toAddress != null) {
                    sendEmail(item, toAddress);
                } else {
                    final Dialog dialogProperties = new Dialog();
                    dialogProperties.setWidth(200);
                    SC.askforValue(BUNDLE.customerEmailTitle(), BUNDLE.customerEmailText(), "", new ValueCallback() {
                        @Override
                        public void execute(String value) {
                            sendEmail(item, value);

                        }
                    }, dialogProperties);
                }
            }
        });
    }

    private void sendEmail(ItemDTO item, String toAddress) {

        if (toAddress != null && toAddress.matches(EMAIL_REGEX)) {
            ((ItemsServiceAsync) GWT.create(ItemsService.class)).sendEmail(item, toAddress, new AsyncCallback<Boolean>() {
                @Override
                public void onFailure(Throwable throwable) {

                }

                @Override
                public void onSuccess(Boolean aBoolean) {
                    SC.say(BUNDLE.successText());
                }
            });
        } else {
            SC.say(BUNDLE.wrongEmailText());
        }
    }

    public Label getBarcodeLabelPrint(String barcodeText) {
        Label barcodeLabel = new Label();
        barcodeLabel.setContents(barcodeText);
        barcodeLabel.setStyleName("barcodeText");
        return barcodeLabel;
    }

    private int getBarcodeNumberInQueue() {
        ItemDTO item = context.getCurrentDTO();
        for (ItemDTO itemDTO : BarcodeQueueListGrid.barcodeQueue) {
            if (item.getId() == itemDTO.getId()) return (int) (long) itemDTO.getQty();
        }
        return 0;
    }

}
