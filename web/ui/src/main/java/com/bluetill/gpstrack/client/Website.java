package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;
import com.bluetill.gpstrack.client.base.PresenterFactory;
import com.bluetill.gpstrack.client.container.Container;
import com.bluetill.gpstrack.client.container.ContainerPresenter;
import com.bluetill.gpstrack.client.container.ViewType;
import com.bluetill.gpstrack.client.exception.NotRegisteredException;
import com.bluetill.gpstrack.client.footer.Footer;
import com.bluetill.gpstrack.client.header.Header;
import com.google.gwt.user.client.Cookies;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;
import org.gwtopenmaps.openlayers.client.OpenLayers;

import java.util.Date;

/**
 * Main portal layout.
 */
public class Website extends VLayout {



    // Skin name
    public static final String DEFAULT_SKIN_NAME = "drivertrackTheme";
    public static final String ALTERNATIVE_SKIN_NAME = "Simplicity";
    public static final Float DEFAULT_BASE_OPACITY = 1.0f;
    public static final int HEADER_SIZE = 70;
    // Constants.
    private static final ViewType DEFAULT_VIEW = ViewType.MAP;
    // Presenter FACTORY for creating and retrieving instances.
    private static final PresenterFactory FACTORY = new PresenterFactory();
    // Skin name
    private static String skinName = DEFAULT_SKIN_NAME;

    /**
     * Creates basic website layout, initializes presenter type map.
     */
    public Website() {
        super();

        this.setWidth100();
        this.setHeight100();
        this.setOverflow(Overflow.HIDDEN);

        Website.loadSkin(null, null);

        OpenLayers.setDotPerInch(25.4 / 0.28);
        OpenLayers.setImageReloadAttempts(5);

        final Header header = (Header) getPresenter(Component.HEADER).getComponent();
        header.setHeight(HEADER_SIZE);
        addMember(header);

        final Container container = (Container) getPresenter(Component.CONTAINER).getComponent();
        container.setHeight100();
        container.setOverflow(Overflow.HIDDEN);
        addMember(container);

        final Footer footer = (Footer) getPresenter(Component.FOOTER).getComponent();
        footer.setHeight(Website.isAlternativeSkinEnabled() ? "30px" : "20px");
        addMember(footer);

        loadView(DEFAULT_VIEW);
    }

    // Accessing presenter instances

    /**
     * Retrieves presenter instance from factory.
     *
     * @param component - the presenters binded component identifier.
     * @return presenter instance.
     */
    public static Presenter<?> getPresenter(final Component component) {
        Presenter<?> result = null;
        try {
            result = FACTORY.getPresenter(component);
        } catch (NotRegisteredException e) {
            SC.warn(e.getMessage());
        }
        return result;
    }

    public static Presenter<?> resetPresenter(final Component component) {
        Presenter<?> result = null;
        try {
            result = FACTORY.resetPresenter(component);
        } catch (NotRegisteredException e) {
            SC.warn(e.getMessage());
        }
        return result;
    }

    /**
     * Loads skin based on cookies.
     */
    public static void loadSkin(final String name, final Float opacity) {
        skinName = name;
        Float baseOpacity = opacity;

        if (skinName == null) {
            skinName = Cookies.getCookie("skin");
            if (skinName == null) {
                skinName = DEFAULT_SKIN_NAME;
            }
        }

        if (baseOpacity == null) {
            final String cookieBaseOpacity = Cookies.getCookie("baseOpacity");
            if (cookieBaseOpacity == null) {
                baseOpacity = DEFAULT_BASE_OPACITY;
            } else {
                baseOpacity = Float.valueOf(cookieBaseOpacity);
            }
        }

        final Date date = new Date();
        long time = date.getTime();
        time += 31556952000L;
        date.setTime(time);
        Cookies.setCookie("skin", skinName, date);
        Cookies.setCookie("baseOpacity", baseOpacity.toString(), date);
    }

    /**
     * Gets the skin name.
     *
     * @return skin name.
     */
    public static String getSkinName() {
        return skinName;
    }

    public static boolean isAlternativeSkinEnabled() {
        return ALTERNATIVE_SKIN_NAME.equals(getSkinName());
    }

    /**
     * Loads view based on identifier.
     *
     * @param type - view type.
     */
    private void loadView(final ViewType type) {
        ((ContainerPresenter) getPresenter(Component.CONTAINER)).setCurrentView(type);
    }
}
