package com.bluetill.gpstrack.client.footer;

import com.google.gwt.i18n.client.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 12/12/13
 * Time: 11:17

 */
public interface Footer10nBundle extends Constants {
    String appName();
    String version();
    String separator();
    String copyright();
}
