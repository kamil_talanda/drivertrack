package com.bluetill.gpstrack.client.drivers;

import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;
import com.bluetill.gpstrack.client.views.MainView;
import com.bluetill.gpstrack.shared.Directions;
import com.google.gwt.core.shared.GWT;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 09/01/14
 * Time: 09:28

 */
public class DriverListPresenter extends Presenter<DriverList> {
    private static final DriverList10nBundle BUNDLE = GWT.create(DriverList10nBundle.class);

    private MainView context;

    public DriverListPresenter() {
        super(new DriverList(), Component.DRIVER);
    }

    public void passContext(MainView context){
        this.context = context;
    }

    public void zoomToPoint(Directions directions, long holderId){
        this.context.zoomToMapPoint(directions, holderId);
    }

    public void zoomToDriver(String driverName){
        this.context.zoomToDriver(driverName);
    }

}
