package com.bluetill.gpstrack.client.items.list.footer;

import com.bluetill.gpstrack.client.base.BaseList;
import com.bluetill.gpstrack.client.items.ItemVars;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 23/01/14
 * Time: 09:01

 */
public class ItemListFooterMenu extends HLayout implements BaseList, ItemVars {

    private final ItemListFooterMenuAdd barcodeToolStrip = new ItemListFooterMenuAdd();
    private final ItemListFooterMenuBarcodeQueue barcodeQueueStrip;
    private final ItemListFooterMenuList listToolStrip;

    private Label bigSeparator = new Label();
    private Label smallSeparator = new Label();

    public ItemListFooterMenu(ItemList context) {

        listToolStrip = new ItemListFooterMenuList(context);
        barcodeQueueStrip = new ItemListFooterMenuBarcodeQueue(context);

        bigSeparator.setWidth100();
        smallSeparator.setWidth(SMALL_SEPARATOR);

        //this.addMember(barcodeToolStrip);

//        this.addMember(barcodeQueueStrip);
        this.addMember(bigSeparator);
        this.addMember(listToolStrip);

    }

    public ItemListFooterMenuAdd getBarcodeToolStrip() {
        return barcodeToolStrip;
    }

    public ItemListFooterMenuList getListToolStrip() {
        return listToolStrip;
    }

    public void reloadBarcodeQueueNumber(){
        barcodeQueueStrip.reloadBarcodeQueueNumber();
    }
}
