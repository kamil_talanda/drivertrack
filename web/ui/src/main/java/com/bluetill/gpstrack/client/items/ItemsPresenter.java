package com.bluetill.gpstrack.client.items;

import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.client.ItemsServiceAsync;
import com.bluetill.gpstrack.client.Website;
import com.bluetill.gpstrack.client.base.Component;
import com.bluetill.gpstrack.client.base.Presenter;
import com.bluetill.gpstrack.client.items.details.ItemDetails;
import com.bluetill.gpstrack.client.items.list.ItemList;
import com.bluetill.gpstrack.client.items.list.footer.ItemListFooter;
import com.bluetill.gpstrack.client.views.MainView;
import com.bluetill.gpstrack.shared.Directions;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;

import java.util.ArrayList;
import java.util.List;

public class ItemsPresenter extends Presenter<ItemList> {

    private MainView context;
    private ItemDetails itemDetailsWindow;

    public ItemsPresenter() {
        super(new ItemList(), Component.TASKS);
        itemDetailsWindow = new ItemDetails(true);
        setRecordDoubleClickHandler();
    }

    public ListGrid getItemsList(List<ItemDTO> items, ArrayList<DriverDTO> drivers) {
        List<ItemRecord> records = new ArrayList<ItemRecord>();
        for (ItemDTO item : items) {
            records.add(new ItemRecord(
                    item.getId(),
                    item.getBarcode(),
                    item.getCarrier(),
                    item.getTenant(),
                    item.getQty(),
                    item.getHolderId(),
                    item.getName(),
                    item.getDestination(),
                    item.getAddTime(),
                    item.getApproveTime(),
                    item.getDeliveryTimePlanned(),
                    item.getApprovePoint(),
                    item.getSignatureImage(),
                    ItemUIUtils.getStatusMap().get(item.getStatus()),
                    item.getDescription(),
                    item.getHolderName(),
                    item.getItemList(),
                    item.getItemListOrder(),
                    item.getBmNo()
                    ));
        }
        this.getComponent().setTableRecords(records);
        return this.getComponent().createTable(items, drivers);
    }

    public ItemListFooter getFooter(){
        return this.getComponent().createFooter();
    }

    public void passContext(MainView context){
        this.context = context;
    }

    public void resetItemView(){
        this.context.resetItemView();
    }

    public void zoomToPoint(Directions directions, long holderId){
        this.context.zoomToMapPoint(directions, holderId);
    }

    public void saveData(final ItemDTO item) {
        ((ItemsServiceAsync) GWT.create(ItemsService.class)).set(item, new AsyncCallback<ItemDTO>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ItemDTO itemDTO) {
                final ItemsPresenter itemsPresenter = (ItemsPresenter) Website.getPresenter(Component.TASKS);
                itemsPresenter.resetItemView();
                itemsPresenter.itemDetailsWindow.hide();
            }
        });
    }


    private void setRecordDoubleClickHandler() {
        this.getComponent().setRecordDoubleClickHandler(new RecordDoubleClickHandler() {
            @Override
            public void onRecordDoubleClick(RecordDoubleClickEvent recordDoubleClickEvent) {
                itemDetailsWindow.showWindow(recordDoubleClickEvent.getRecord());
            }
        });
    }

    public void closeWindow(){
        itemDetailsWindow.hide();
    }

    public ItemDetails getItemDetailsWindow() {
        return itemDetailsWindow;
    }

}
