package com.bluetill.gpstrack.client.items.details;

import com.bluetill.gpstrack.client.items.ItemBarcode;
import com.bluetill.gpstrack.client.items.ItemRecord;
import com.bluetill.gpstrack.client.items.ItemUIUtils;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Img;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 07/01/14
 * Time: 07:10

 */
public class ItemDetailsLayoutEdit extends ItemDetailsLayout {

    public static final Integer SIGNATURE_IMG_WIDTH = 400;
    private static final String WINDOW_HEIGHT_WITHOUT_SIGNATURE = "360px";
    private static final String WINDOW_HEIGHT_WITH_SIGNATURE = "480px";
    private static final Integer SIGNATURE_IMG_HEIGHT = 80;
    private static final String SHOW_TIME_FORMAT = "HH:mm dd/MM/yyyy";

    public ItemDetailsLayoutEdit(ItemDetails context, Record record) {
        super(context, record);
    }

    @Override
    public void fillUp(final Record record) {
        timeForm.hide();
        signatureForm.hide();
        context.setCurrentRecord(record);
        context.setHeight(WINDOW_HEIGHT_WITHOUT_SIGNATURE);
        context.setTitle(BUNDLE.windowTitleDetails());

        receiverTextItem.setValue(record.getAttribute(ItemRecord.NAME));


        approvePointTextItem.setValue(record.getAttribute(ItemRecord.APPROVE_POINT));

        statusSelectItem.setValue(record.getAttribute(STATUS)!=null ? ItemUIUtils.getStatusLong(record.getAttribute(STATUS)).toString():null);
        destinationInputTextItem.setValue(record.getAttribute(DESTINATION));
        for (String driverId : context.driversMap.keySet()) {
            if (context.driversMap.get(driverId).equals(record.getAttribute(HOLDER_NAME))) {
                driverSelectItem.setValue(driverId);
                break;
            }
        }

        timeForm.show();

        addTimeItem.setValue(parseTime(record.getAttributeAsDate(ADD_TIME)));
        approveTimeItem.setValue(parseTime(record.getAttributeAsDate(APPROVE_TIME)));


        if (record.getAttribute(SIGNATURE) != null) {
            signatureForm.show();
            context.setHeight(WINDOW_HEIGHT_WITH_SIGNATURE);
            createSignature(record.getAttribute(SIGNATURE));
        }

        barcodeTextItem.setValue(record.getAttribute(BARCODE));
        barcodeTextItem.disable();
        descriptionTextAreaItem.setValue(record.getAttribute(DESCRIPTION));

        new ItemBarcode(record.getAttribute(BARCODE)) {

            @Override
            public void barcodeTextAction() {
            }

            @Override
            public void barcodeImageAction() {
                barcodeImg.setSrc(barcodeImage.getSrc());
                context.setBarcodeImage(barcodeImage);
                context.getBarcodeLabel().setContents(barcodeText);
            }
        }.generateBarcodeImageBase64(DETAILS_BARCODE_WIDTH, DETAILS_BARCODE_HEIGHT);

        resolveDestination();

    }

    private Img createSignature(String signatureImageBase64) {

        signatureImg = new Img(IMAGE_FORMAT, SIGNATURE_IMG_WIDTH, SIGNATURE_IMG_HEIGHT);
        signatureImg.setImageWidth(SIGNATURE_IMG_WIDTH);
        signatureImg.setImageHeight(SIGNATURE_IMG_HEIGHT);
        signatureImg.setAlign(Alignment.CENTER);
        signatureImg.setHeight(SIGNATURE_IMG_HEIGHT);
        signatureImg.setWidth(SIGNATURE_IMG_WIDTH);
        signatureImg.setSrc(IMAGE_FORMAT + signatureImageBase64);

        return signatureImg;
    }

    private String parseTime(Date date) {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat(SHOW_TIME_FORMAT);
        if (date != null) return dateTimeFormat.format(date);
        else return null;
    }

}
