package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:43

 */

@RemoteServiceRelativePath("driverService")
public interface DriverService  extends RemoteService {

    DriverDTO get(Long driverId);
    DriverDTO getByName(String driverName);

    ArrayList<DriverDTO> getAll();

    boolean deleteByName(String name);

    DriverDTO set(DriverDTO dto);

    DriverDTO getByBmId(Long bm_id);
}
