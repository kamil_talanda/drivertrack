package com.bluetill.gpstrack.server.restws;

import com.bluetill.gpstrack.DriverVars;
import com.bluetill.gpstrack.DriversPathVars;
import com.bluetill.gpstrack.UserVars;
import com.bluetill.gpstrack.server.DriverPathServiceLogic;
import com.bluetill.gpstrack.server.DriverServiceLogic;
import com.bluetill.gpstrack.server.ItemsServiceLogic;
import com.bluetill.gpstrack.shared.dto.DriverDTO;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author mprajs
 */
@Controller
public class UserController implements RestVars, UserVars, DriverVars, DriversPathVars {

    private static final String JSON_DRIVER_NAME = "driver_name";
    private static final String CONSUMES = "application/json;charset=UTF-8";
    @Autowired
    DriverServiceLogic driverLogic;
    @Autowired
    DriverPathServiceLogic driverPathLogic;
    @Autowired
    ItemsServiceLogic itemsServiceLogic;

    @RequestMapping(method = RequestMethod.POST, value = "/" + USER_TABLE + "_signIn", consumes = CONSUMES)
    @ResponseBody
    public String getDriver(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(ANDROID_ID);
        DriverDTO dto = driverLogic.getByName(driverName, username);

        if (driverName != null && androidId != null && username != null && dto != null) {
            if (driverLogic.checkAndroidId(driverName, androidId, username)) {
                return Long.toString(dto.getId());
            } else if (driverLogic.isUserEmpty(driverName, username)) {
                dto.setDeviceId(androidId);
                return Long.toString(driverLogic.set(dto, username).getId());
            }
        }
        return Integer.toString(-1);
    }

}