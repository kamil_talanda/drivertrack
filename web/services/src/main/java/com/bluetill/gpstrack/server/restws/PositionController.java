package com.bluetill.gpstrack.server.restws;

import com.bluetill.gpstrack.DriversPathVars;
import com.bluetill.gpstrack.ItemsEntityVars;
import com.bluetill.gpstrack.server.DriverPathServiceLogic;
import com.bluetill.gpstrack.server.DriverServiceLogic;
import com.bluetill.gpstrack.server.ItemsServiceLogic;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gson.Gson;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author mprajs
 */
@Controller
public class PositionController implements RestVars, ItemsEntityVars, DriversPathVars {

    private static final String ITEM_DELIVERED_ALERT = "Item already delivered";
    private static final String NO_DRIVER_ALERT = "No driver assigned yet";
    private static final String NO_GPS_ALERT = "Item 'In Transit' but GPS location is not available yet.";
    private static final String WRONG_ID_ALERT = "Wrong item id";
    private static final String DELIVERED_ALERT = "Delivered";
    private static final long STATUS_DONE = 1;
    private static final long STATUS_IN_PROGRESS = 0;
    private static final long HOLDER_UNASSIGNED = -1;
    private static final String POSITION = "position";
    private static final String JSON_DRIVER_NAME = "driver_name";
    private static final String JSON_DRIVER_ID = "driver_id";
    private static final String JSON_ANDROID_ID = "android_id";
    private static final String JSON_LINE = "line";
    private static final String CONSUMES = "application/json;charset=UTF-8";
    @Autowired
    DriverServiceLogic driverLogic;
    @Autowired
    DriverPathServiceLogic driverPathLogic;
    @Autowired
    ItemsServiceLogic itemsServiceLogic;

    @Deprecated
    @RequestMapping(method = RequestMethod.POST, value = "/" + POSITION + "_syncDriver", consumes = CONSUMES)
    @ResponseBody
    public String syncDriver(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        Long driverId = ((Integer) jsonObject.get(JSON_DRIVER_ID)).longValue();
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        String line = (String) jsonObject.get(JSON_LINE);

        if (driverId != -1 && androidId != null) {
            if (driverLogic.get(driverId, username) != null && driverLogic.checkAndroidId(driverLogic.get(driverId, username).getName(), androidId, username)) {
                if (line != null && line.length() > 0) {
                    driverPathLogic.saveLine(driverId, line, username);
                }
                return driverPathLogic.syncDriver(driverId);
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + POSITION + "_getItemLocation", consumes = CONSUMES)
    @ResponseBody
    public String getItemLocation(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String barcode = (String) jsonObject.get(ITEM_BARCODE);

        for (ItemDTO itemDTO : itemsServiceLogic.getByBarcode(barcode)) {
            if (itemDTO.getStatus() == 0) {
                itemDTO.setSignatureImage(null);
                itemDTO.setBmNo(null);
                String result = "{\"item\": " + new Gson().toJson(itemDTO);
                if (itemDTO.getStatus().equals(STATUS_DONE) || itemDTO.getHolderId() == HOLDER_UNASSIGNED)
                    return result + "}";
                else {
                    Integer numberOfOpenBefore = itemsServiceLogic.getNumberOfOpenBefore(itemDTO);

                    String lastKnownPosition = driverPathLogic.getLastDriverPosition(itemDTO.getHolderId());
                    if (lastKnownPosition != null) result += ", \"position\":\"" + lastKnownPosition + "\"";
                    if (numberOfOpenBefore != null) result += ", \"before\":" + numberOfOpenBefore;
                    result += "}";
                    return result;
                }
            } else {
                return DELIVERED_ALERT;
            }
        }
        return WRONG_ID_ALERT;

    }

}