package com.bluetill.gpstrack.shared;

import java.io.Serializable;

/**
 * Service layer exception.
 */
@SuppressWarnings("serial")
public class ServiceException extends Exception implements Serializable {

    public ServiceException() { }

    /**
     * Creates a new service exception
     * @param message - exception message.
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Creates a new service exception
     * @param e - exception.
     */
    public ServiceException(Exception e) {
        super(e);
        this.setStackTrace(e.getStackTrace());
        e.printStackTrace();
    }
}
