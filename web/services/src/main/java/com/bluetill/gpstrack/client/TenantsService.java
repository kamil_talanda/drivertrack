package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.TenantsDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:43

 */

@RemoteServiceRelativePath("tenantsService")
public interface TenantsService extends RemoteService {

    ArrayList<TenantsDTO> getAll();

    TenantsDTO set(TenantsDTO dto);

    boolean deleteByName(String name);
}
