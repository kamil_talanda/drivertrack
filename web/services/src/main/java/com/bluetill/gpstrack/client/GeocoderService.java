package com.bluetill.gpstrack.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:48

 */
@RemoteServiceRelativePath("geocoderService")
public interface GeocoderService extends RemoteService {
    String[] geocodeAddress(String address);

    Long getDrivingTimeInSeconds(String origin, String destination);
}
