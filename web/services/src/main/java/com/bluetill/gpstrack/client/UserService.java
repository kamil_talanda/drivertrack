package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:48

 */
@RemoteServiceRelativePath("userService")
public interface UserService extends RemoteService {

    List<UserDTO> getAll();

    UserDTO get(String username);

    UserDTO set(UserDTO userDTO);

    boolean deleteByName(String username);

    UserDTO getLoggedUserDetails();
}
