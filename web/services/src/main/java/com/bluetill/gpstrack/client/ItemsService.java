package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Date;
import java.util.List;

@RemoteServiceRelativePath("itemsService")
public interface ItemsService extends RemoteService {

    ItemDTO set(ItemDTO itemDTO);

    void deleteById(Long itemId);

    List<ItemDTO> getByDriverId(Long id);

    List<ItemDTO> getUncommittedItems();

    List<ItemDTO> getAll();

    List<ItemDTO> getByBarcode(String barcode);

    List<ItemDTO> getAll(Date startFilterDate, Date endFilterDate);

    String getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight);

    int getItemsNumber();

    int getMaxItemId();

    String getApprovePointDirections(String wktGeometry);

    boolean sendEmail(ItemDTO item, String emailAddress);

    List<ItemDTO> getInProgressByDriverId(Long id);

    ItemDTO setFromUploadFIle(ItemDTO dto);

    void deleteByList(Long list, Long holderId);
}
