package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface Item_ListServiceAsync {
    void changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, boolean changeAllTime, AsyncCallback<ItemDTO> async);

    void calculateRoute(List<ItemDTO> itemDTOs, AsyncCallback<Void> async);
}
