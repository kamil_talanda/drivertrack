package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.LegendEntry;
import com.bluetill.gpstrack.shared.ServiceException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("LegendService")
public interface LegendService extends RemoteService {

    List<LegendEntry> getLegend() throws ServiceException;
    void setMessage(Long driverId, String msg);
}
