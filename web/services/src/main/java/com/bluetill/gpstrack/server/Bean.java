package com.bluetill.gpstrack.server;

import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Class for retrieving beans from context.
 */
public final class Bean {

    /**
     * Gets bean from given context.
     *
     * @param <T>   generic type parameter of the bean.
     * @param ctx   servlet context.
     * @param clazz - type of the bean.
     * @return spring bean.
     */
    public static <T> T get(ServletContext ctx, Class<T> clazz) {
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(ctx);
        return context.getBean(clazz);
    }
}
