package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.DriverService;
import com.bluetill.gpstrack.shared.dto.DriverDTO;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:51

 */
public class DriverServiceImpl extends BaseService <DriverServiceLogic> implements DriverService {
    @Override
    public ArrayList<DriverDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public DriverDTO get(Long driverId) {
        return logic().get(driverId);
    }

    @Override
    public DriverDTO set(DriverDTO dto) {
        return logic().set(dto);
    }

    @Override
    public DriverDTO getByName(String driverName) {
        return logic().getByName(driverName);
    }

    @Override
    public DriverDTO getByBmId(Long bm_id) {
        return logic().getByBmId(bm_id);
    }

    @Override
    public boolean deleteByName(String name){
        return logic().deleteByName(name);
    }

}
