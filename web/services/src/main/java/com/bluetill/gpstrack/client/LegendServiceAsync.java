package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.LegendEntry;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface LegendServiceAsync {

    void getLegend(AsyncCallback<List<LegendEntry>> async);

    void setMessage(Long driverId, String msg, AsyncCallback<Void> async);
}
