package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.CarriersService;
import com.bluetill.gpstrack.shared.dto.CarriersDTO;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:51

 */
public class CarriersServiceImpl extends BaseService <CarriersServiceLogic> implements CarriersService {


    @Override
    public ArrayList<CarriersDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public CarriersDTO set(CarriersDTO dto) {
        return logic().set(dto);
    }

    @Override
    public boolean deleteByName(String name) {
        return logic().deleteByName(name);
    }
}
