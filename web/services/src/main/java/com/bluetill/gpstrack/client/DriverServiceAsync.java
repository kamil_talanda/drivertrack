package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.DriverDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:43

 */
public interface DriverServiceAsync {


    void get(Long driverId, AsyncCallback<DriverDTO> async);

    void getByName(String driverName, AsyncCallback<DriverDTO> async);

    void getAll(AsyncCallback<ArrayList<DriverDTO>> async);

    void deleteByName(String name, AsyncCallback<Boolean> async);

    void set(DriverDTO dto, AsyncCallback<DriverDTO> async);

    void getByBmId(Long bm_id, AsyncCallback<DriverDTO> async);
}
