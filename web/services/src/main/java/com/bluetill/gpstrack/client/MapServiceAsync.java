package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 09:24

 */
public interface MapServiceAsync {

    void getAll(AsyncCallback<List<DriversPathDTO>> async);

    void getLastDriverPosition(long driverId, AsyncCallback<String> async);

    void getDriverTrack(String projection, Long driverId, Long day, AsyncCallback<DriversPathDTO> async);
}
