package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.TenantsService;
import com.bluetill.gpstrack.shared.dto.TenantsDTO;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:51

 */
public class TenantsServiceImpl extends BaseService <TenantsServiceLogic> implements TenantsService {

    @Override
    public ArrayList<TenantsDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public TenantsDTO set(TenantsDTO dto) {
        return logic().set(dto);
    }

    @Override
    public boolean deleteByName(String name) {
        return logic().deleteByName(name);
    }
}
