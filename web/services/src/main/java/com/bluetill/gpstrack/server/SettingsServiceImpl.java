package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.SettingsService;
import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:42
 */
public class SettingsServiceImpl extends BaseService<SettingsServiceLogic> implements SettingsService {

    @Override
    public SettingsDTO getByName(String name) {
        return logic().getByName(name);
    }

    @Override
    public SettingsDTO set(SettingsDTO dto) {
        return logic().set(dto);
    }

    @Override
    public String getUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        return name;
    }
}
