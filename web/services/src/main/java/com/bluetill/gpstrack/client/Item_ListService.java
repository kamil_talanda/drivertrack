package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("item_ListService")
public interface Item_ListService extends RemoteService {
    ItemDTO changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, boolean changeAllTime);

    void calculateRoute(List<ItemDTO> itemDTOs);
}
