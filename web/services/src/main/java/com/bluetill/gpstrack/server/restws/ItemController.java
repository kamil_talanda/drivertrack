package com.bluetill.gpstrack.server.restws;

import com.bluetill.gpstrack.DriverVars;
import com.bluetill.gpstrack.ItemsEntityVars;
import com.bluetill.gpstrack.server.DriverPathServiceLogic;
import com.bluetill.gpstrack.server.DriverServiceLogic;
import com.bluetill.gpstrack.server.ItemsServiceLogic;
import com.bluetill.gpstrack.server.UserServiceLogic;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gson.Gson;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 23/12/13
 * Time: 12:26

 */
@Controller
public class ItemController implements ItemsEntityVars, DriverVars, RestVars {


    @Autowired
    DriverServiceLogic driverLogic;
    @Autowired
    DriverPathServiceLogic driverPathLogic;
    @Autowired
    ItemsServiceLogic itemLogic;
    @Autowired
    UserServiceLogic userServiceLogic;

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_getAll", consumes = CONSUMES)
    @ResponseBody
    public String getAll(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);

        if (userServiceLogic.checkPassword(username, password))
            return new Gson().toJson(itemLogic.getAll(username));
        else return createJSONMessage(REST_VERIFICATION_FAIL);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_get", consumes = CONSUMES)
    @ResponseBody
    public String get(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);

        Long itemId = (Long) jsonObject.get(ID);
        if (userServiceLogic.checkPassword(username, password))
            return new Gson().toJson(itemLogic.get(itemId, username));
        else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_getByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String getByBarcode(@RequestBody String input) {

        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);
        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        String barcode = (String) jsonObject.get(ITEM_BARCODE);

        if (userServiceLogic.checkPassword(username, password))
            return new Gson().toJson(itemLogic.getByBarcode(barcode, username));

        else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_getByDriverId", consumes = CONSUMES)
    @ResponseBody
    public String getByDriverId(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);

        if (userServiceLogic.checkPassword(username, password))
            return new Gson().toJson(itemLogic.getByDriverId(driverId, username));
        else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_approve", consumes = CONSUMES)
    @ResponseBody
    public String approve(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        ArrayList<Long> itemIdList = getListFromJSON((String) jsonObject.get(ID));
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);
        String signatureImage = (String) jsonObject.get(ITEM_SIGNATURE_IMAGE);
        String approvePoint = (String) jsonObject.get(ITEM_APPROVE_POINT);

        if (userServiceLogic.checkPassword(username, password)) {
            for (Long itemId : itemIdList) {
                itemLogic.approve(itemId, driverId, signatureImage, approvePoint, username);
            }
            return createJSONMessage(REST_OK_MESSAGE);
        } else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_approveByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String approveByBarcode(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        String barcode = (String) jsonObject.get(ITEM_BARCODE);
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);
        String signatureImage = (String) jsonObject.get(ITEM_SIGNATURE_IMAGE);
        String approvePoint = (String) jsonObject.get(ITEM_APPROVE_POINT);

        if (userServiceLogic.checkPassword(username, password)) {
            itemLogic.approveByBarcode(barcode, driverId, signatureImage, approvePoint, username);
            return createJSONMessage(REST_OK_MESSAGE);
        } else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_addDescription", consumes = CONSUMES)
    @ResponseBody
    public String addDescription(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        Long itemId = (Long) jsonObject.get(ID);
        String description = (String) jsonObject.get(ITEM_DESCRIPTION);

        if (userServiceLogic.checkPassword(username, password)) {
            ItemDTO dto = itemLogic.get(itemId, username);
            dto.setDescription(dto.getDescription() + description);
            return new Gson().toJson(itemLogic.set(dto, username));
        } else return createJSONMessage(REST_VERIFICATION_FAIL);


    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_setHolder", consumes = CONSUMES)
    @ResponseBody
    public String setHolder(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);
        Long itemId = (Long) jsonObject.get(ID);

        if (userServiceLogic.checkPassword(username, password)) {
            ItemDTO dto = itemLogic.get(itemId, username);
            dto.setHolderId(driverId);
            return new Gson().toJson(itemLogic.set(dto, username));
        } else return createJSONMessage(REST_VERIFICATION_FAIL);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_setHolderByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String setHolderByBarcode(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);
        String barcode = (String) jsonObject.get(ITEM_BARCODE);

        if (userServiceLogic.checkPassword(username, password)) {
            itemLogic.setHolderByBarcode(barcode,
                    driverId, username);
            return createJSONMessage(REST_OK_MESSAGE);
        } else return createJSONMessage(REST_VERIFICATION_FAIL);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/" + ITEMS_TABLE + "_resetHolder", consumes = CONSUMES)
    @ResponseBody
    public String resetHolder(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);
        String username = (String) jsonObject.get(USERNAME);
        String password = (String) jsonObject.get(PASSWORD);
        Long driverId = (Long) jsonObject.get(ITEM_HOLDER_ID);
        Long itemId = (Long) jsonObject.get(ID);

        if (userServiceLogic.checkPassword(username, password)) {
            ItemDTO itemDTO = itemLogic.get(itemId, username);
            itemDTO.setHolderId(-1l);
            return new Gson().toJson(itemLogic.set(itemDTO, username));
        } else return createJSONMessage(REST_VERIFICATION_FAIL);

    }

    private ArrayList<Long> getListFromJSON(String jsonArray) {
        ArrayList<Long> result = new ArrayList<Long>();
        String cleanInput = jsonArray.substring(1, jsonArray.length() - 1);
        for (String inputItemId : cleanInput.split(", ")) {
            result.add(Long.parseLong(inputItemId));
        }
        return result;
    }

    private String createJSONMessage(String errorMessage) {
        return new Gson().toJson(errorMessage);
    }
}
