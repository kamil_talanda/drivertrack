package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.LegendService;
import com.bluetill.gpstrack.shared.DrawIcon;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.bluetill.gpstrack.shared.LegendEntry;
import com.bluetill.gpstrack.shared.ServiceException;

import java.util.ArrayList;
import java.util.List;

public class LegendServiceImpl extends BaseService<DriverPathServiceLogic> implements LegendService {


    @Override
    public List<LegendEntry> getLegend() throws ServiceException {
        ArrayList<LegendEntry> legend = new ArrayList<>();
        for(DriversPathDTO driversPathDTO : logic().getAll()){
            DrawIcon icon = new DrawIcon(driversPathDTO.getStyle().color);
            legend.add(new LegendEntry((int)(long)driversPathDTO.getId(), driversPathDTO.getName(), icon.base64bytes));
        }
        return legend;
    }

    @Override
    public void setMessage(Long driverId, String msg) {
        DriverPathServiceLogic.setMessage(driverId,msg);
    }
}
