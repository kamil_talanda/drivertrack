package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.CarriersDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 05/12/13
 * Time: 19:43

 */

@RemoteServiceRelativePath("carriersService")
public interface CarriersService extends RemoteService {

    ArrayList<CarriersDTO> getAll();

    CarriersDTO set(CarriersDTO dto);

    boolean deleteByName(String name);
}
