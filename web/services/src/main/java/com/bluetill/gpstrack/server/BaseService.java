package com.bluetill.gpstrack.server;

import java.lang.reflect.ParameterizedType;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Base class for all services;
 */
@SuppressWarnings("serial")
public abstract class BaseService<T> extends RemoteServiceServlet {

    private T logic;
    private Class<T> logicType;

    /**
     * Constructs the BaseService object and uses reflection to infer the generic type
     * logic type parameter.
     */
    @SuppressWarnings("unchecked")
    public BaseService() {
        super();
        logicType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    /**
     * Retrieves a lazily initialized logic bean class.
     *
     * @return logic type based on generic parameter.
     */
    protected T logic() {
        if (logic == null) {
            logic = Bean.get(getServletContext(), logicType);
        }
        return logic;
    }
}
