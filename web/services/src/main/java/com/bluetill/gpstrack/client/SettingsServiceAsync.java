package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:42

 */
public interface SettingsServiceAsync {

    void getUserName(AsyncCallback<String> async);

    void getByName(String name, AsyncCallback<SettingsDTO> async);

    void set(SettingsDTO dto, AsyncCallback<SettingsDTO> async);
}
