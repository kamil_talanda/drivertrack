package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Date;
import java.util.List;

public interface ItemsServiceAsync {

    void getItemsNumber(AsyncCallback<Integer> async);

    void getApprovePointDirections(String wktGeometry, AsyncCallback<String> async);

    void getMaxItemId(AsyncCallback<Integer> async);

    void sendEmail(ItemDTO item, String emailAddress, AsyncCallback<Boolean> async);

    void set(ItemDTO itemDTO, AsyncCallback<ItemDTO> async);

    void deleteById(Long itemId, AsyncCallback<Void> async);

    void getByDriverId(Long id, AsyncCallback<List<ItemDTO>> async);

    void getUncommittedItems(AsyncCallback<List<ItemDTO>> async);

    void getAll(AsyncCallback<List<ItemDTO>> async);

    void getByBarcode(String barcode, AsyncCallback<List<ItemDTO>> async);

    void getAll(Date startFilterDate, Date endFilterDate, AsyncCallback<List<ItemDTO>> async);

    void getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight, AsyncCallback<String> async);

    void getInProgressByDriverId(Long id, AsyncCallback<List<ItemDTO>> async);

    void setFromUploadFIle(ItemDTO dto, AsyncCallback<ItemDTO> async);

    void deleteByList(Long list, Long holderId, AsyncCallback<Void> async);
}
