package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.MapService;
import com.bluetill.gpstrack.shared.dto.DriversPathDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 09:24

 */
public class MapServiceImpl extends BaseService<DriverPathServiceLogic> implements MapService {

    @Override
    public List<DriversPathDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public DriversPathDTO getDriverTrack(String projection, Long driverId, Long day) {
        return logic().getDriverTrack(driverId, day);
    }

    @Override
    public String getLastDriverPosition(long driverId) {
        return logic().getLastDriverPosition(driverId);
    }

}