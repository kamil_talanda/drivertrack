package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.DriversPathDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 12.07.13
 * Time: 09:24

 */
@RemoteServiceRelativePath("MapService")
public interface MapService extends RemoteService {


    List<DriversPathDTO> getAll();


    String getLastDriverPosition(long driverId);

    DriversPathDTO getDriverTrack(String projection, Long driverId, Long day);
}
