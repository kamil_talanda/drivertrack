package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.TenantsDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

public interface TenantsServiceAsync {

    void getAll(AsyncCallback<ArrayList<TenantsDTO>> async);

    void set(TenantsDTO dto, AsyncCallback<TenantsDTO> async);

    void deleteByName(String name, AsyncCallback<Boolean> async);
}
