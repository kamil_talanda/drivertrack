package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.GeocoderService;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:47

 */
public class GeocoderServiceImpl extends BaseService <GeocoderServiceLogic> implements GeocoderService {
    @Override
    public String[] geocodeAddress(String address) {
        return logic().geocodeAddress(address);
    }

    @Override
    public Long getDrivingTimeInSeconds(String origin, String destination) {
        return logic().getDrivingTimeInSeconds(origin, destination);
    }

}
