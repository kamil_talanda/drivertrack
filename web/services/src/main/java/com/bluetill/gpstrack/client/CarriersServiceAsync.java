package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.CarriersDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

public interface CarriersServiceAsync {

    void getAll(AsyncCallback<ArrayList<CarriersDTO>> async);

    void set(CarriersDTO dto, AsyncCallback<CarriersDTO> async);

    void deleteByName(String name, AsyncCallback<Boolean> async);
}
