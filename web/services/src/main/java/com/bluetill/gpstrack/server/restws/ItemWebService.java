package com.bluetill.gpstrack.server.restws;

import com.bluetill.gpstrack.server.DriverPathServiceLogic;
import com.bluetill.gpstrack.server.DriverServiceLogic;
import com.bluetill.gpstrack.server.Item_ListServiceLogic;
import com.bluetill.gpstrack.server.ItemsServiceLogic;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import com.google.gson.Gson;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 23/12/13
 * Time: 12:26

 */
@Controller
public class ItemWebService {

    private static final String ITEM_DELIVERED_ALERT = "Item already delivered";
    private static final String NO_DRIVER_ALERT = "No driver assigned yet";
    private static final String NO_GPS_ALERT = "Item 'In Transit' but GPS location is not available yet.";
    private static final String WRONG_ID_ALERT = "Wrong item id";
    //Web service query statics
    private static final String JSON_DRIVER_NAME = "driver_name";
    private static final String JSON_ANDROID_ID = "android_id";
    private static final String JSON_ITEM_ID = "item_id";
    private static final String JSON_ITEM_BARCODE = "barcode";
    private static final String JSON_DESCRIPTION = "description";
    private static final String JSON_SIGNATURE_IMAGE = "signature_image";
    private static final String JSON_POINT = "point";
    private static final String CONSUMES = "application/json;charset=UTF-8";
    private static final long STATUS_DONE = 1;
    private static final long STATUS_IN_PROGRESS = 0;
    private static final long HOLDER_UNASSIGNED = -1;

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @Autowired
    DriverServiceLogic driverLogic;
    @Autowired
    DriverPathServiceLogic driverPathLogic;
    @Autowired
    ItemsServiceLogic itemLogic;
    @Autowired
    Item_ListServiceLogic item_listServiceLogic;

    @RequestMapping(method = RequestMethod.POST, value = "/getItem", consumes = CONSUMES)
    @ResponseBody
    public String getItem(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        Long itemId = ((Integer) jsonObject.get(JSON_ITEM_ID)).longValue();

        if (driverName != null && androidId != null && itemId != null) {
            if (driverLogic.checkAndroidId(driverName, androidId, username)) {
                JSONArray tmpJSONArray = new JSONArray();
                tmpJSONArray.add(itemLogic.get(itemId, username));
                return tmpJSONArray.toString();
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getItemByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String getItemByBarcode(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        String barcode = (String) jsonObject.get(JSON_ITEM_BARCODE);

        if (driverName != null && androidId != null && barcode != null) {
            if (driverLogic.checkAndroidId(driverName, androidId, username)) {
                JSONArray tmpJSONArray = new JSONArray();
                tmpJSONArray.add(itemLogic.getByBarcode(barcode, username));
                return tmpJSONArray.toString();
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getItemList", consumes = CONSUMES)
    @ResponseBody
    public String getItemList(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);

        if (driverName != null && androidId != null) {
            if (driverLogic.checkAndroidId(driverName, androidId, username)) {
                JSONArray tmpJSONArray = new JSONArray();
                tmpJSONArray.add(itemLogic.getByDriverIdToday(driverLogic.getByName(driverName, username).getId(), username));
                return tmpJSONArray.toString();
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/approveItem", consumes = CONSUMES)
    @ResponseBody
    public String approveItem(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        ArrayList<Long> itemIdList = getListFromJSON((String) jsonObject.get(JSON_ITEM_ID));
        String signatureImage = (String) jsonObject.get(JSON_SIGNATURE_IMAGE);
        String approvePoint = (String) jsonObject.get(JSON_POINT);

        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            for (Long itemId : itemIdList) {
                itemLogic.approve(itemId, driverLogic.getByName(driverName, username).getId(), signatureImage, approvePoint, username);
                item_listServiceLogic.calculateRoute(itemLogic.getByDriverId(driverLogic.getByName(driverName, username).getId(), username),username);
            }
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/approveItemByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String approveItemByBarcode(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        String barcode = (String) jsonObject.get(JSON_ITEM_BARCODE);
        String signatureImage = (String) jsonObject.get(JSON_SIGNATURE_IMAGE);
        String approvePoint = (String) jsonObject.get(JSON_POINT);

        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            itemLogic.approveByBarcode(barcode,
                    driverLogic.getByName(driverName, username).getId(),
                    signatureImage, approvePoint, username);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/addDescription", consumes = CONSUMES)
    @ResponseBody
    public String addDescription(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        Long itemId = ((Integer) jsonObject.get(JSON_ITEM_ID)).longValue();
        String description = (String) jsonObject.get(JSON_DESCRIPTION);

        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            ItemDTO dto = itemLogic.get((long) itemId, username);
            dto.setDescription(dto.getDescription() + description);
            return new Gson().toJson(itemLogic.set(dto, username));
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/setHolder", consumes = CONSUMES)
    @ResponseBody
    public String setHolder(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        Long itemId = ((Integer) jsonObject.get(JSON_ITEM_ID)).longValue();


        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            ItemDTO dto = itemLogic.get(itemId, username);
            dto.setHolderId(driverLogic.getByName(driverName, username).getId());
            new Gson().toJson(itemLogic.set(dto, username));
        }

        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/setHolderByBarcode", consumes = CONSUMES)
    @ResponseBody
    public String setHolderByBarcode(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        String barcode = (String) jsonObject.get(JSON_ITEM_BARCODE);

        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            itemLogic.setHolderByBarcode(barcode,
                    driverLogic.getByName(driverName).getId(), username);
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/resetHolder", consumes = CONSUMES)
    @ResponseBody
    public String resetHolder(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String username = (String) jsonObject.get(USERNAME);
        String driverName = (String) jsonObject.get(JSON_DRIVER_NAME);
        String androidId = (String) jsonObject.get(JSON_ANDROID_ID);
        Long itemId = ((Integer) jsonObject.get(JSON_ITEM_ID)).longValue();

        if (driverLogic.checkAndroidId(driverName, androidId, username)) {
            ItemDTO itemDTO = itemLogic.get(itemId, username);
            itemDTO.setHolderId(-1l);
            return new Gson().toJson(itemLogic.set(itemDTO, username));
        }
        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getItemLocation", consumes = CONSUMES)
    @ResponseBody
    public String getItemLocation(@RequestBody String input) {
        JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(input);

        String barcode = (String) jsonObject.get(JSON_ITEM_BARCODE);

        for (ItemDTO itemDTO : itemLogic.getByBarcode(barcode)) {
            if (itemDTO.getStatus().equals(STATUS_DONE)) return ITEM_DELIVERED_ALERT;
            else if (itemDTO.getHolderId() == HOLDER_UNASSIGNED) return NO_DRIVER_ALERT;
            else {
                String lastKnownPosition = driverPathLogic.getLastDriverPosition(itemDTO.getHolderId());
                if (lastKnownPosition != null) return lastKnownPosition;
                else return NO_GPS_ALERT;
            }
        }
        return WRONG_ID_ALERT;

    }

    private ArrayList<Long> getListFromJSON(String jsonArray) {
        ArrayList<Long> result = new ArrayList<Long>();
        String cleanInput = jsonArray.substring(1, jsonArray.length() - 1);
        for (String inputItemId : cleanInput.split(", ")) {
            result.add(Long.parseLong(inputItemId));
        }
        return result;
    }

    private String createJSONMessage(String errorMessage) {
        return new Gson().toJson(errorMessage);
    }
}
