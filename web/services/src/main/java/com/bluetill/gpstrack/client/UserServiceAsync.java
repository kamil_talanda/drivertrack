package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.UserDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface UserServiceAsync {

    void getAll(AsyncCallback<List<UserDTO>> async);

    void get(String username, AsyncCallback<UserDTO> async);

    void set(UserDTO userDTO, AsyncCallback<UserDTO> async);

    void deleteByName(String username, AsyncCallback<Boolean> async);

    void getLoggedUserDetails(AsyncCallback<UserDTO> async);
}
