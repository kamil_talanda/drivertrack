package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.ItemsService;
import com.bluetill.gpstrack.shared.dto.ItemDTO;

import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class ItemsServiceImpl extends BaseService<ItemsServiceLogic> implements ItemsService {

    @Override
    public ItemDTO set(ItemDTO itemDTO) {
        return logic().set(itemDTO);
    }

    @Override
    public void deleteById(Long itemId) {
        logic().deleteById(itemId);
    }

    @Override
    public void deleteByList(Long list, Long holderId) {
        logic().deleteByList(list, holderId);
    }

    @Override
    public List<ItemDTO> getByDriverId(Long id) {
        return logic().getByDriverId(id);
    }

    @Override
    public List<ItemDTO> getInProgressByDriverId(Long id) {
        return logic().getInProgressByDriverId(id);
    }

    @Override
    public List<ItemDTO> getUncommittedItems() {
        return logic().getUncommittedItems();
    }

    @Override
    public List<ItemDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public List<ItemDTO> getByBarcode(String barcode) {
        return logic().getByBarcode(barcode);
    }

    @Override
    public ItemDTO setFromUploadFIle(ItemDTO dto) {
        return logic().setFromUploadFIle(dto);
    }

    @Override
    public List<ItemDTO> getAll(Date startFilterDate, Date endFilterDate) {
        return logic().getAll(startFilterDate, endFilterDate);
    }

    @Override
    public String getBarcodeImage(String itemId, int barcodeWidth, int barcodeHeight) {
        return logic().getBarcodeImage(itemId, barcodeWidth, barcodeHeight);
    }

    @Override
    public int getItemsNumber() {
        return logic().getItemsNumber();
    }

    @Override
    public int getMaxItemId() {
        return logic().getMaxItemId();
    }

    @Override
    public String getApprovePointDirections(String wktGeometry) {
        return logic().getApprovePointDirections(wktGeometry);
    }

    @Override
    public boolean sendEmail(ItemDTO item, String emailAddress) {
        return logic().sendEmail(item, emailAddress);
    }

}
