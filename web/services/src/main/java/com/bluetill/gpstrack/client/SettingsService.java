package com.bluetill.gpstrack.client;

import com.bluetill.gpstrack.shared.dto.SettingsDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 24/12/13
 * Time: 15:42

 */
@RemoteServiceRelativePath("SettingsService")
public interface SettingsService extends RemoteService {


    String getUserName();

    SettingsDTO getByName(String name);

    SettingsDTO set(SettingsDTO dto);
}
