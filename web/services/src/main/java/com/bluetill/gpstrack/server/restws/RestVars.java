package com.bluetill.gpstrack.server.restws;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 04/09/14
 * Time: 06:02

 */
public interface RestVars {

    String CONSUMES = "application/json;charset=UTF-8";
    String PRODUCES = "application/json;charset=UTF-8";

    String REST_OK_MESSAGE = "OK";
    String REST_FAIL_MESSAGE = "FAIL";
    String REST_VERIFICATION_FAIL = "VERIFICATION_FAIL";
}
