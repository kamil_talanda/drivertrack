package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.Item_ListService;
import com.bluetill.gpstrack.shared.dto.ItemDTO;
import java.util.List;

public class Items_ListServiceImpl extends BaseService<Item_ListServiceLogic> implements Item_ListService {

    @Override
    public ItemDTO changeOrder(List<ItemDTO> itemDTOs, ItemDTO currentItem, long oldItemListKey, long newItemListKey, long oldListOrder, long newListOrder, boolean changeAllTime) {
        return logic().changeOrder(itemDTOs, currentItem, oldItemListKey, newItemListKey, oldListOrder, newListOrder, changeAllTime);
    }

    @Override
    public void calculateRoute(List<ItemDTO> itemDTOs){
        logic().calculateRoute(itemDTOs);
    }


}
