package com.bluetill.gpstrack.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GeocoderServiceAsync {

    void getDrivingTimeInSeconds(String origin, String destination, AsyncCallback<Long> async);

    void geocodeAddress(String address, AsyncCallback<String[]> async);
}
