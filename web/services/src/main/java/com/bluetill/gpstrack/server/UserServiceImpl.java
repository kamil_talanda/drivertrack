package com.bluetill.gpstrack.server;

import com.bluetill.gpstrack.client.UserService;
import com.bluetill.gpstrack.shared.dto.UserDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/05/14
 * Time: 08:47

 */
public class UserServiceImpl extends BaseService <UserServiceLogic> implements UserService {
    @Override
    public List<UserDTO> getAll() {
        return logic().getAll();
    }

    @Override
    public UserDTO get(String username) {
        return logic().getByName(username);
    }

    @Override
    public UserDTO set(UserDTO userDTO) {
        return logic().set(userDTO);
    }

    @Override
    public boolean deleteByName(String username) {
        return logic().deleteByName(username);
    }

    @Override
    public UserDTO getLoggedUserDetails() {
        return logic().getLoggedUserDetails();
    }
}
